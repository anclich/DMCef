// ----------------------------------------------------------------
// Copyright (c)  
// All rights reserved.
// 
// File name:	DMWebApp.h
// File mark:   
// File summary:IDMWebApp的实现
// Author:		guoyouhuang
// Edition:     1.0
// Create date: 2017-9-8
// ----------------------------------------------------------------
#pragma once
#include "IDMWebApp.h"
#include "MainUIRunner.h"

class DMWebApp : public IDMWebApp
{
public:
	DMWebApp();
	~DMWebApp();

public:
	bool InitializeWeb(bool bOsrRender = true,LPCWSTR lpszWebDir = NULL,IDMExternalHandler* pExternalHandler = NULL);
	void UnInitializeWeb();
	void PostQuitMsg();
	IDMExternalHandler* GetExternalHandler();
	bool SetCookieForUrl(LPCWSTR lpszUrl, LPCWSTR lpszCookie,LPCWSTR lpszDoMain,bool bSync = false);

private:
	bool AddDirToEnvirPath(LPCWSTR lpszWebDir);
	void InitCefSetting(CefSettings& Settings);
	void InitExtraInfo(CefRefPtr<CefListValue>& ExtraInfo);
	bool SetCefCookieForUrl(std::wstring strUrl,std::wstring strCookie,std::wstring strDoMain,bool bSync = false,WaitableEvent*Wait = NULL);

public:// 此处不能保留LIBCef.dll的任意变量,因为是延迟加载
	MainUIRunner*							m_pMainUIRunner;			
	IDMExternalHandler*						m_pExternalHandler;				
	bool                                    m_bInit;
	bool									m_bOsrRender;
};