// ----------------------------------------------------------------
// Copyright (c)  
// All rights reserved.
// 
// File name:	DUIWeb.h
// File mark:   
// File summary:离屏模式CEF
// Author:		guoyouhuang
// Edition:     1.0
// Create date: 2017-9-4
// ----------------------------------------------------------------
#pragma once
#include "CefClientHandler.h"

namespace DMAttr
{
	/// <summary>
	///		<see cref="DM::DUIWeb"/>的xml属性定义
	/// </summary>
	class DUIWebAttr
	{
	public:
		static wchar_t* STRING_url;                                    ///< 指定Web的默认加载URL,示例:url="www.baidu.com"
		static wchar_t* bool_bshowcontext;                             ///< 是否显示或隐藏右键菜单,默认显示,示例:bshowcontext="0"
		static wchar_t* ACCEL_refreshkey;                              ///< 刷新快捷捷,示例:refreshkey="ctrl+f5"
	};
	DMAttrValueInit(DUIWebAttr,STRING_url)DMAttrValueInit(DUIWebAttr,bool_bshowcontext)DMAttrValueInit(DUIWebAttr,ACCEL_refreshkey)
}

class DUIWeb : public DUIWindow
			  ,public CefClientHandler::Delegate
			  ,public CefOsrClientHandler::OsrDelegate
			  ,public OsrDragEvents
			  ,public IDMMessageFilter
			  ,public IDUIWeb 
{
	DMDECLARE_CLASS_NAME(DUIWeb, L"cef",DMREG_Window)
public:
	DUIWeb();
	~DUIWeb();

public:
	//---------------------------------------------------
	// Function Des: IDUIWeb对外接口
	//---------------------------------------------------
	int GetWebType(){return DMWEBTYPE_OSRWEBKIT;}
	HRESULT SetEvtHandler(IDMWebEvent* pEventHandler);
	HRESULT OpenUrl(LPCWSTR pszURL,int iFlags = 0,LPCWSTR pszTargetFrameName = NULL,LPCWSTR pszHeaders = NULL, LPCSTR pszPostData = NULL,int iPostDataLen = 0);
	HRESULT GetUrl(LPWSTR pszUrl, int nMaxLen);
	HWND GetOleWindow();
	bool IsBusy();
	HRESULT Stop();
	HRESULT Quit();
	HRESULT Refresh();
	HRESULT Refresh2(UINT32 nLevel);
	HRESULT GoBack();
	HRESULT GoForward();
	HRESULT ExecuteScript(LPCWSTR pszScript);
	HRESULT ExecuteScriptFuntion(LPCWSTR pszFun, const DM::CArray<LPCWSTR>& vecParams, LPWSTR strResult = NULL,int nMaxLen = -1);
	HRESULT SetContextMenuShow(bool bShow);
	HRESULT WebSetAttribute(LPCWSTR pszAttribute,LPCWSTR pszValue,bool bLoadXml);

public:
	//---------------------------------------------------
	// Function Des: Delegate实现
	//---------------------------------------------------
	virtual bool OnProcessMessageReceived(CefRefPtr<CefBrowser> browser, CefProcessId source_process, CefRefPtr<CefProcessMessage> message);
	virtual void OnBeforeContextMenu(CefRefPtr<CefBrowser> browser, CefRefPtr<CefFrame> frame, CefRefPtr<CefContextMenuParams> params, CefRefPtr<CefMenuModel> model);
	virtual bool OnPreKeyEvent(CefRefPtr<CefBrowser> browser,const CefKeyEvent& event,CefEventHandle os_event,bool* is_keyboard_shortcut);
	virtual bool OnBeforePopup(CefRefPtr<CefBrowser> browser, CefRefPtr<CefFrame> frame, const CefString& target_url, const CefString& target_frame_name, CefLifeSpanHandler::WindowOpenDisposition target_disposition, bool user_gesture, const CefPopupFeatures& popupFeatures, CefWindowInfo& windowInfo, CefRefPtr<CefClient>& client, CefBrowserSettings& settings, bool* no_javascript_access);
	virtual void OnAfterCreated(CefRefPtr<CefBrowser> browser);
	virtual void OnAddressChange(CefRefPtr<CefBrowser> browser, CefRefPtr<CefFrame> frame,const CefString& url);
	virtual void OnTitleChange(CefRefPtr<CefBrowser> browser,const CefString& title);
	virtual void OnLoadEnd(CefRefPtr<CefBrowser> browser,CefRefPtr<CefFrame> frame,int httpStatusCode);
	virtual void OnFullscreenModeChange(CefRefPtr<CefBrowser> browser,bool fullscreen);

	//---------------------------------------------------
	// Function Des: OsrDelegate实现
	//---------------------------------------------------
	virtual bool GetViewRect(CefRefPtr<CefBrowser> browser,CefRect& rect);
	virtual void OnPaint(CefRefPtr<CefBrowser> browser,PaintElementType type,const RectList& dirtyRects,const void* buffer,int width,int height);
	virtual void OnPopupShow(CefRefPtr<CefBrowser> browser, bool show);
	virtual void OnPopupSize(CefRefPtr<CefBrowser> browser, const CefRect& rect);
	virtual void OnCursorChange(CefRefPtr<CefBrowser> browser, CefCursorHandle cursor, CursorType type,	const CefCursorInfo& custom_cursor_info);
	virtual bool StartDragging(CefRefPtr<CefBrowser> browser,CefRefPtr<CefDragData> drag_data,CefRenderHandler::DragOperationsMask allowed_ops,int x, int y);
	virtual void UpdateDragCursor(CefRefPtr<CefBrowser> browser,CefRenderHandler::DragOperation operation);

	//---------------------------------------------------
	// Function Des: OsrDragEvents实现
	//---------------------------------------------------
	virtual CefBrowserHost::DragOperationsMask OnDragEnter(CefRefPtr<CefDragData> drag_data,CefMouseEvent ev,CefBrowserHost::DragOperationsMask effect);
	virtual CefBrowserHost::DragOperationsMask OnDragOver(CefMouseEvent ev,CefBrowserHost::DragOperationsMask effect);
	virtual void OnDragLeave();
	virtual CefBrowserHost::DragOperationsMask OnDrop(CefMouseEvent ev,CefBrowserHost::DragOperationsMask effect);

	//---------------------------------------------------
	// Function Des: IDMMessageFilter实现
	//---------------------------------------------------
	virtual DMCode DV_OnSetCursor(const CPoint &pt);
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	LRESULT SendMouseMoveEvent(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
	LRESULT SendButtonDownEvent(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
	LRESULT SendButtonUpEvent(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
	LRESULT SendButtonDoubleDownEvent(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
	LRESULT SendKeyEvent(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
	LRESULT SendCaptureLostEvent(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
	LRESULT SendMouseWheelEvent(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
	LRESULT SendMouseLeaveEvent(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);

private:	
	HRESULT LoadUrl(std::wstring strUrl);
	bool InitOsrCanvas(PaintElementType type,int iWidth,int iHei);
	void OsrOnPaint(PaintElementType type,const RectList& dirtyRects,int width,int height);
	bool IsCaptured();
	CefRefPtr<CefBrowser> GetBrowser();										
	CefRefPtr<CefBrowserHost> GetBrowserHost();
	void NewWindow3(const CefString& target_url,VARIANT_BOOL *pbCancel,WaitableEvent*Wait = NULL);
	void BeforeNavigate2(CefRefPtr<CefFrame> frame,const CefString& strUrl,int iNavType,const CefString& strPostData,const CefString& strHeaderData,VARIANT_BOOL *pbCancel,WaitableEvent*Wait = NULL);
	bool JsCallbackMessage(CefRefPtr<CefBrowser>& browser, CefProcessId& source_process, CefRefPtr<CefProcessMessage>& message);
	bool BeforeNavigateMessage(CefRefPtr<CefBrowser>& browser, CefProcessId& source_process, CefRefPtr<CefProcessMessage>& message);

public:
	DM_BEGIN_MSG_MAP()
		MSG_WM_CREATE(OnCreate)
		MSG_WM_DESTROY(OnDestroy)
		MSG_WM_SIZE(OnSize)
		DM_MSG_WM_PAINT(DM_OnPaint)
		MSG_WM_SHOWWINDOW(OnShowWindow)
		DM_MSG_WM_SETFOCUS(DM_OnSetFocus)
		DM_MSG_WM_KILLFOCUS(DM_OnKillFocus)
	DM_END_MSG_MAP()
	int  OnCreate(LPVOID);
	void OnDestroy();
	void OnSize(UINT nType, CSize size);
	void DM_OnPaint(IDMCanvas* pCanvas);
	void OnShowWindow(BOOL bShow, UINT nStatus);
	void DM_OnSetFocus();
	void DM_OnKillFocus();

public:
	DM_BEGIN_ATTRIBUTES()
		DM_CUSTOM_ATTRIBUTE(DMAttr::DUIWebAttr::STRING_url,OnAttributeUrl)
		DM_bool_ATTRIBUTE(DMAttr::DUIWebAttr::bool_bshowcontext,m_bShowContext,DM_ECODE_OK)
		DM_CUSTOM_ATTRIBUTE(DMAttr::DUIWebAttr::ACCEL_refreshkey, OnAttributeRefreshKey)
	DM_END_ATTRIBUTES()

public:
	DMCode OnAttributeUrl(LPCWSTR pszValue, bool bLoadXml);
	DMCode OnAttributeRefreshKey(LPCWSTR pszValue, bool bLoadXml);

public:
	CStringW										m_strUrl;
	CefRefPtr<CefOsrClientHandler>					m_pWebHandler;
	IDMWebEvent*                                    m_pEventHandler;
	bool											m_bShowContext;
	DWORD											m_Refreshkey;

	//--------------------------------------------------------------------
	DMBufT<byte>                                    m_pOrgBuf;				
	int                                             m_iOrgSize;
	DMBufT<byte>                                    m_pOrgPopBuf;
	int                                             m_iOrgPopSize;

	DMSmartPtrT<IDMCanvas>							m_pOsrCanvas;			///< 保存cef离屏渲染的数据
	DMSmartPtrT<IDMCanvas>							m_pOsrPopCanvas;		///< 保存cef的popup窗口的离屏渲染数据
	CefRect                                         m_rcOsrPop;				///< 当网页的组合框一类的控件弹出时，记录弹出的位置
	DMLock                                          m_OsrLock;
	
	//--------------------------------------------------------------------
	CComPtr<DropTargetWin>							m_pDropTarget;
	CefRenderHandler::DragOperation					m_pCurDropOp;
	
public: // DUIWeb的生命周期不再由DMRefNum控制，转由IMPLEMENT_REFCOUNTING控制，默认引用计数为1,在OnFinalRelease中递减
	virtual void OnFinalRelease();
	IMPLEMENT_REFCOUNTING(DUIWeb)						
};
