// ----------------------------------------------------------------
// Copyright (c)  
// All rights reserved.
// 
// File name:	MainUIRunner.h 
// File mark:   
// File summary:实现多线程异步转发消息到DM主UI线程
// Author:		guoyouhuang
// Edition:     1.0
// Create date: 2017-9-4
// ----------------------------------------------------------------
#pragma once

/// <summary>
///		消息窗口
/// </summary>
class MainUIRunner : public DMCWnd,
					 public IMainUIRunner
{
public:
	MainUIRunner();
	~MainUIRunner();

public:
	virtual void PostTask(CefRefPtr<CefTask> task);	
	virtual void PostTask(const base::Closure& closure);
	virtual bool RunTaskOnMainUI() const;			

public:
	bool Run();

public:
	BEGIN_MSG_MAPT(MainUIRunner)
		MESSAGE_HANDLER_EX(m_MsgId, OnHandlerEvent)
	END_MSG_MAP()
public:
	LRESULT OnHandlerEvent(UINT uMsg, WPARAM wParam, LPARAM lParam);

public:
	const UINT							m_MsgId;
	DWORD								m_dwThreadId;
	DMLock                              m_Lock;
};