#pragma once
#define WIN32_LEAN_AND_MEAN             // 从 Windows 头中排除极少使用的资料
#include <windows.h>
#include <tchar.h> 
#include <comdef.h>

// DM
#include "DmMainOutput.h"
#include "IDUIWeb.h"

// lib
#ifdef _DEBUG
#pragma comment(lib,"DmMain_d.lib")
#pragma comment(lib,"libcef.lib")
#pragma comment(lib,"libcef_dll_wrapper_d.lib")
#pragma comment(lib,"cef_module_d.lib")
#else
#pragma comment(lib,"DmMain.lib")
#pragma comment(lib,"libcef.lib")
#pragma comment(lib,"libcef_dll_wrapper.lib")
#pragma comment(lib,"cef_module.lib")
#endif

using namespace DM;

//
#include "CefModHelper.h"
#include "CefManager.h"
#include "CefOsrDragDrop.h"
#include "MainUIRunner.h"
#include "DMWebApp.h"
#if 0
#include "vld.h"
#endif

extern IDMWebPtr g_pDMWebApp;