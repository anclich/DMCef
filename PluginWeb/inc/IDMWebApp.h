// ----------------------------------------------------------------
// Copyright (c)  
// All rights reserved.
// 
// File name:	IDMWebApp.h
// File mark:   
// File summary:CEF对外接口
// Author:		guoyouhuang
// Edition:     1.0
// Create date: 2017-9-8
// ----------------------------------------------------------------
#pragma once
#include "IDUIWeb.h"

#ifdef PluginWeb_EXPORTS
#define WEB_EXPORT __declspec(dllexport)
#else
#define WEB_EXPORT __declspec(dllimport)
#endif // PluginWeb_EXPORTS

/// <summary>
///		JS调用c++接口
/// </summary>
class IDMExternalHandler
{
public:
	virtual void GetAllFunNames(DM::CArray<CStringW>& NameArray) = 0;	///< 获取外部对象所有的方法名称
	virtual void CallFunction(DUIWND hDUIWnd, CStringW strName, DM::CArray<CStringW>&strArgs, CStringW& strResult) = 0;
};

/// <summary>
///	 唯一全局接口
/// </summary>
class IDMWebApp : public DMBase
{
public:
	/// -------------------------------------------------
	/// @brief	InitializeWeb				初始化CEF加载
	/// @param	bOsrRender					离屏模式
	/// @param	lpszWebDir					CEF文件夹所在目录,NULL表示为exe同目录
	virtual bool InitializeWeb(bool bOsrRender = true,LPCWSTR lpszWebDir = NULL,IDMExternalHandler* pExternalHandler = NULL) = 0; 
	virtual void UnInitializeWeb() = 0;
	virtual void PostQuitMsg() = 0;
	virtual IDMExternalHandler* GetExternalHandler() = 0;

	/// -------------------------------------
	/// @brief	SetCookieForUrl 
	/// @param	lpszUrl						要设置的url,记得带上http之类的前缀
	/// @param	lpszCookie					cookie串,以逗号分隔
	/// @param	lpszDoMain					为空或NULL时使用url的域,否则使用自定义的域
	/// @param	bSync						同步内部默认会等待3S
	virtual bool SetCookieForUrl(LPCWSTR lpszUrl, LPCWSTR lpszCookie,LPCWSTR lpszDoMain=NULL,bool bSync = false) = 0;
};
typedef IDMWebApp* IDMWebPtr;
EXTERN_C WEB_EXPORT void DMWebCreate(IDMWebPtr* ppObj);	///< 全局接口,请使用DMSmartPtrT获取,Release释放



