// ----------------------------------------------------------------
// Copyright (c)  
// All rights reserved.
// 
// File name:	DUINativeWeb.h 
// File mark:   
// File summary:原生模式CEF
// Author:		guoyouhuang
// Edition:     1.0
// Create date: 2017-9-4
// ----------------------------------------------------------------
#pragma once
#include "CefClientHandler.h"

namespace DMAttr
{
	/// <summary>
	///		<see cref="DM::DUINativeWeb"/>的xml属性定义
	/// </summary>
	class DUINativeWebAttr
	{
	public:
		static wchar_t* STRING_url;                                    ///< 指定Web的默认加载URL,示例:url="www.baidu.com"
		static wchar_t* bool_bshowcontext;                             ///< 是否显示或隐藏右键菜单,默认显示,示例:bshowcontext="0"
		static wchar_t* ACCEL_refreshkey;                              ///< 刷新快捷捷,示例:refreshkey="ctrl+f5"
	};
	DMAttrValueInit(DUINativeWebAttr,STRING_url)DMAttrValueInit(DUINativeWebAttr,bool_bshowcontext)DMAttrValueInit(DUINativeWebAttr,ACCEL_refreshkey)
}

class DUINativeWeb : public DUIWindow
					,public CefClientHandler::Delegate
					,public IDUIWeb 
{
	DMDECLARE_CLASS_NAME(DUINativeWeb, L"cef",DMREG_Window)
public:
	DUINativeWeb();
	~DUINativeWeb();

public:
	//---------------------------------------------------
	// Function Des: IDUIWeb对外接口
	//---------------------------------------------------
	int GetWebType(){return DMWEBTYPE_WEBKIT;}
	HRESULT SetEvtHandler(IDMWebEvent* pEventHandler);
	HRESULT OpenUrl(LPCWSTR pszURL,int iFlags = 0,LPCWSTR pszTargetFrameName = NULL,LPCWSTR pszHeaders = NULL, LPCSTR pszPostData = NULL,int iPostDataLen = 0);
	HRESULT GetUrl(LPWSTR pszUrl, int nMaxLen);
	HWND GetOleWindow();
	bool IsBusy();
	HRESULT Stop();
	HRESULT Quit();
	HRESULT Refresh();
	HRESULT Refresh2(UINT32 nLevel);
	HRESULT GoBack();
	HRESULT GoForward();
	HRESULT ExecuteScript(LPCWSTR pszScript);
	HRESULT ExecuteScriptFuntion(LPCWSTR pszFun, const DM::CArray<LPCWSTR>& vecParams, LPWSTR strResult = NULL,int nMaxLen = -1);
    HRESULT SetContextMenuShow(bool bShow);
	HRESULT WebSetAttribute(LPCWSTR pszAttribute,LPCWSTR pszValue,bool bLoadXml);

public:
	//---------------------------------------------------
	// Function Des: Delegate实现
	//---------------------------------------------------
	virtual bool OnProcessMessageReceived(CefRefPtr<CefBrowser> browser, CefProcessId source_process, CefRefPtr<CefProcessMessage> message);
	virtual void OnBeforeContextMenu(CefRefPtr<CefBrowser> browser, CefRefPtr<CefFrame> frame, CefRefPtr<CefContextMenuParams> params, CefRefPtr<CefMenuModel> model);
	virtual bool OnPreKeyEvent(CefRefPtr<CefBrowser> browser,const CefKeyEvent& event,CefEventHandle os_event,bool* is_keyboard_shortcut);
	virtual bool OnBeforePopup(CefRefPtr<CefBrowser> browser, CefRefPtr<CefFrame> frame, const CefString& target_url, const CefString& target_frame_name, CefLifeSpanHandler::WindowOpenDisposition target_disposition, bool user_gesture, const CefPopupFeatures& popupFeatures, CefWindowInfo& windowInfo, CefRefPtr<CefClient>& client, CefBrowserSettings& settings, bool* no_javascript_access);
	virtual void OnAfterCreated(CefRefPtr<CefBrowser> browser);
	virtual void OnAddressChange(CefRefPtr<CefBrowser> browser, CefRefPtr<CefFrame> frame,const CefString& url);
	virtual void OnTitleChange(CefRefPtr<CefBrowser> browser,const CefString& title);
	virtual void OnLoadEnd(CefRefPtr<CefBrowser> browser,CefRefPtr<CefFrame> frame,int httpStatusCode);
	virtual void OnFullscreenModeChange(CefRefPtr<CefBrowser> browser,bool fullscreen);

private:	
	HRESULT LoadUrl(std::wstring strUrl);
	void UpdateWebRect(); 
	bool CurrentlyOnMainUI();
	CefRefPtr<CefBrowser> GetBrowser();										
	CefRefPtr<CefBrowserHost> GetBrowserHost();
	void NewWindow3(const CefString& target_url,VARIANT_BOOL *pbCancel,WaitableEvent*Wait = NULL);
	void BeforeNavigate2(CefRefPtr<CefFrame> frame,const CefString& strUrl,int iNavType,const CefString& strPostData,const CefString& strHeaderData,VARIANT_BOOL *pbCancel,WaitableEvent*Wait = NULL);
	bool JsCallbackMessage(CefRefPtr<CefBrowser>& browser, CefProcessId& source_process, CefRefPtr<CefProcessMessage>& message);
	bool BeforeNavigateMessage(CefRefPtr<CefBrowser>& browser, CefProcessId& source_process, CefRefPtr<CefProcessMessage>& message);

public:
	DM_BEGIN_MSG_MAP()
		MSG_WM_CREATE(OnCreate)
		MSG_WM_DESTROY(OnDestroy)
		MSG_WM_SIZE(OnSize)
		MSG_WM_SHOWWINDOW(OnShowWindow)
	DM_END_MSG_MAP()
	int  OnCreate(LPVOID);
	void OnDestroy();
	void OnSize(UINT nType, CSize size);
	void OnShowWindow(BOOL bShow, UINT nStatus);

public:
	DM_BEGIN_ATTRIBUTES()
		DM_CUSTOM_ATTRIBUTE(DMAttr::DUINativeWebAttr::STRING_url,OnAttributeUrl)
		DM_bool_ATTRIBUTE(DMAttr::DUINativeWebAttr::bool_bshowcontext,m_bShowContext,DM_ECODE_OK)
		DM_CUSTOM_ATTRIBUTE(DMAttr::DUINativeWebAttr::ACCEL_refreshkey, OnAttributeRefreshKey)
	DM_END_ATTRIBUTES()

public:
	DMCode OnAttributeUrl(LPCWSTR pszValue, bool bLoadXml);
	DMCode OnAttributeRefreshKey(LPCWSTR pszValue, bool bLoadXml);

public:
	CStringW										m_strUrl;
	CefRefPtr<CefClientHandler>						m_pWebHandler;
	IDMWebEvent*                                    m_pEventHandler;
	bool											m_bShowContext;
	DWORD											m_Refreshkey;

public: // DUINativeWeb的生命周期不再由DMRefNum控制，转由IMPLEMENT_REFCOUNTING控制，默认引用计数为1,在OnFinalRelease中递减
	virtual void OnFinalRelease();
	IMPLEMENT_REFCOUNTING(DUINativeWeb)	
};


