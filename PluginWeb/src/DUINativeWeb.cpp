#include "PluginWebAfx.h"
#include "DUINativeWeb.h"
#include "include/base/cef_bind.h"
#include "include/base/cef_bind_helpers.h"
#include <exdisp.h>

DUINativeWeb::DUINativeWeb()
{
	m_pWebHandler = NULL;
	m_pEventHandler = NULL;
	m_bShowContext = true;
	m_Refreshkey = DUIAccel::TranslateAccelKey(L"f5");
	DUINativeWeb::AddRef();
}

DUINativeWeb::~DUINativeWeb()
{

}

//---------------------------------------------------
// Function Des: IDUIWeb对外接口
//---------------------------------------------------
#pragma region IDUIWeb
HRESULT DUINativeWeb::SetEvtHandler(IDMWebEvent* pEventHandler)
{
	m_pEventHandler = pEventHandler;	
	return S_OK;
}

HRESULT DUINativeWeb::OpenUrl(LPCWSTR pszURL,int iFlags /*= 0*/,LPCWSTR pszTargetFrameName /*= NULL*/,LPCWSTR pszHeaders /*= NULL*/, LPCSTR pszPostData /*= NULL*/,int iPostDataLen /*= 0*/)
{
	if (!IsValidString(pszURL))
	{
		return S_FALSE;
	}
	m_strUrl = pszURL;
	return LoadUrl(pszURL);
}

HRESULT DUINativeWeb::GetUrl(LPWSTR pszUrl, int nMaxLen)
{
	HRESULT hr = S_FALSE;
	do 
	{
		if (GetBrowser())
		{
			CefRefPtr<CefFrame> frame = GetBrowser()->GetMainFrame();
			if (frame)
			{
				CefString Url = frame->GetURL();
				std::wstring strUrl = Url.ToWString();
				if (nMaxLen < (int)strUrl.length())
				{
					break;
				}
				ZeroMemory(pszUrl, nMaxLen*sizeof(wchar_t));
				if (!strUrl.empty())
				{
					memcpy(pszUrl, strUrl.c_str(), strUrl.length()*sizeof(wchar_t));
				}
				hr = S_OK;
			}
		}
	} while (false);
	return hr;
}

HWND DUINativeWeb::GetOleWindow()
{
	if (GetBrowserHost())
	{
		return GetBrowserHost()->GetWindowHandle();
	}

	return NULL;
}

bool DUINativeWeb::IsBusy()
{
	if (GetBrowser())
	{
		return GetBrowser()->IsLoading();
	}
	return false;
}

HRESULT DUINativeWeb::Stop()
{
	HRESULT hr = S_FALSE;
	do 
	{
		if (GetBrowser())
		{
			GetBrowser()->StopLoad();
			hr = S_OK;
		}
	} while (false);
	return hr;
}

HRESULT DUINativeWeb::Quit()
{
	HRESULT hr = S_FALSE;
	do 
	{
		m_pEventHandler = NULL;
		if (GetBrowserHost())
		{
			::ShowWindow(GetOleWindow(), SW_HIDE);
			::SetParent(GetOleWindow(),NULL);
			GetBrowserHost()->CloseBrowser(true);
			m_pWebHandler->SetHostWindow(NULL);
			m_pWebHandler->SetDelegate(NULL);
			m_pWebHandler = NULL;
			hr = S_OK;
		}
	} while (false);
	return hr;
}

HRESULT DUINativeWeb::Refresh()
{
	HRESULT hr = S_FALSE;
	do 
	{
		if (GetBrowser())
		{
			GetBrowser()->Reload();
			hr = S_OK;
		}
	} while (false);
	return hr;
}

HRESULT DUINativeWeb::Refresh2(UINT32 nLevel)
{
	HRESULT hr = S_FALSE;
	do 
	{
		if (GetBrowser())
		{
			if (REFRESH_NORMAL == nLevel)
			{
				GetBrowser()->Reload();
			}
			else// 页面失效或无论是否失效都强制从服务器取cache
			{
				GetBrowser()->ReloadIgnoreCache();
			}
			hr = S_OK;
		}
	} while (false);
	return hr;
}

HRESULT DUINativeWeb::GoBack()
{
	HRESULT hr = S_FALSE;
	do 
	{
		if (GetBrowser())
		{
			GetBrowser()->GoBack();
			hr = S_OK;
		}
	} while (false);
	return hr;
}

HRESULT DUINativeWeb::GoForward()
{
	HRESULT hr = S_FALSE;
	do 
	{
		if (GetBrowser())
		{
			GetBrowser()->GoForward();
			hr = S_OK;
		}
	} while (false);
	return hr;
}

HRESULT DUINativeWeb::ExecuteScript(LPCWSTR pszScript)
{
	HRESULT hr = S_FALSE;
	do 
	{
		if (!IsValidString(pszScript))
		{
			break;
		}
		if (GetBrowser())
		{
			CefRefPtr<CefFrame> frame = GetBrowser()->GetMainFrame();
			if (frame)
			{
				frame->ExecuteJavaScript(CefString(pszScript),frame->GetURL(),0);
				hr = S_OK;
			}
		}
	} while (false);
	return hr;
}

HRESULT DUINativeWeb::ExecuteScriptFuntion(LPCWSTR pszFun, const DM::CArray<LPCWSTR>& vecParams, LPWSTR strResult /*= NULL*/,int nMaxLen /*= -1*/)
{
	HRESULT hr = S_FALSE;
	do 
	{
		if (!IsValidString(pszFun))
		{
			break;
		}
		CStringW strJS = pszFun;
		strJS += L"(";
		int iCount = (int)vecParams.GetCount();
		for (int i=0; i< iCount; i++)
		{
			CStringW strParam = vecParams[i];
			strParam.Replace(L"\\", L"\\\\"); // 把所有‘\’替换成‘\\’
			strParam.Replace(L"\'", L"\\\'"); // 把所有‘'’替换成‘\'’
			strParam.Replace(L"\r", L"\\r");  // 把所有‘\r’替换成‘\\r’
			strParam.Replace(L"\n", L"\\n");  // 把所有‘\n’替换成‘\\n’
			strJS += L"\'";
			strJS += strParam;
			strJS += L"\'";
			if (i!=iCount-1)
			{
				strJS += L",";
			}
		}
		strJS += L")";
		hr = ExecuteScript(strJS);
	} while (false);
	return hr;
}

HRESULT DUINativeWeb::SetContextMenuShow(bool bShow)
{
	m_bShowContext = bShow;
	return S_OK;
}

HRESULT DUINativeWeb::WebSetAttribute(LPCWSTR pszAttribute,LPCWSTR pszValue,bool bLoadXml)
{
	if (DMSUCCEEDED(SetAttribute(pszAttribute,pszValue,bLoadXml)))
	{
		return S_OK;
	}
	return S_FALSE;
}
#pragma endregion


//---------------------------------------------------
// Function Des: 消息分发函数
//---------------------------------------------------
#pragma region MsgDispatch
int DUINativeWeb::OnCreate(LPVOID)
{
	if (NULL == m_pWebHandler)
	{
		LONG style = GetWindowLong(GetContainer()->OnGetHWnd(), GWL_STYLE);
		SetWindowLong(GetContainer()->OnGetHWnd(), GWL_STYLE, style | WS_CLIPSIBLINGS | WS_CLIPCHILDREN);
		if (DMSUCCEEDED(GetContainer()->OnIsTranslucent()))
		{
			m_pDUIXmlInfo->m_strText = L"无法在分层窗口内加载原生Cef窗口,请使用离屏模式";
			return 0;
		}
		m_pWebHandler = new CefClientHandler;
		m_pWebHandler->SetHostWindow(GetContainer()->OnGetHWnd());
		m_pWebHandler->SetDelegate(this);

		// 使用有窗模式
		CefWindowInfo window_info;
		window_info.SetAsChild(GetContainer()->OnGetHWnd(), m_rcWindow);

		CefBrowserSettings browser_settings;
		CefBrowserHost::CreateBrowser(window_info, m_pWebHandler, L"", browser_settings, NULL);
	}

	return 0;
}

void DUINativeWeb::OnDestroy()
{
	Quit();
	SetMsgHandled(FALSE);
}

void DUINativeWeb::OnSize(UINT nType, CSize size)
{
	DUIWindow::OnSize(nType,size);
	UpdateWebRect();
}

void DUINativeWeb::OnShowWindow(BOOL bShow, UINT nStatus)
{
	DUIWindow::OnShowWindow(bShow, nStatus);
	HWND hwnd = GetOleWindow();
	if (hwnd)
	{
		if (bShow)
		{
			ShowWindow(hwnd, SW_SHOW);
		}
		else
		{
			ShowWindow(hwnd, SW_HIDE);
		}
	}
}
#pragma endregion

#pragma region Attr
DMCode DUINativeWeb::OnAttributeUrl(LPCWSTR pszValue, bool bLoadXml)
{
	m_strUrl = pszValue;
	if (!bLoadXml)
	{
		OpenUrl(m_strUrl);
	}
	return DM_ECODE_OK;
}

DMCode DUINativeWeb::OnAttributeRefreshKey(LPCWSTR pszValue, bool bLoadXml)
{
	CStringW strValue = pszValue;
	m_Refreshkey = DUIAccel::TranslateAccelKey(strValue);
	return DM_ECODE_OK;
}
#pragma endregion

//---------------------------------------------------
// Function Des: Delegate实现
//---------------------------------------------------
#pragma region Delegate
bool DUINativeWeb::OnProcessMessageReceived(CefRefPtr<CefBrowser> browser, CefProcessId source_process, CefRefPtr<CefProcessMessage> message)
{
	if (source_process == PID_RENDERER)
	{
		if (!message->IsValid()||NULL == g_pDMWebApp || NULL == g_pDMWebApp->GetExternalHandler()) 
		{
			return false;
		}

		CefRefPtr<CefListValue> Args = message->GetArgumentList();
		if (message->GetName() == kJsCallbackMessage && Args->GetSize() > 0)
		{
			return JsCallbackMessage(browser,source_process,message);
		}
		if (message->GetName() == kBeforeNavigateMessage)
		{
			return BeforeNavigateMessage(browser,source_process,message);
		}
	}

	return false;
}

void DUINativeWeb::OnBeforeContextMenu(CefRefPtr<CefBrowser> browser, CefRefPtr<CefFrame> frame, CefRefPtr<CefContextMenuParams> params, CefRefPtr<CefMenuModel> model)
{
	if (!m_bShowContext)
	{
		if (model->GetCount() > 0)
		{
			model->Clear();// 禁止右键菜单
		}
	}
}

bool DUINativeWeb::OnPreKeyEvent(CefRefPtr<CefBrowser> browser,const CefKeyEvent& event,CefEventHandle os_event,bool* is_keyboard_shortcut)
{
	bool bRet = false;
	do 
	{
		if (!event.focus_on_editable_field && event.windows_key_code == VK_SPACE) 
		{// 不处于编辑焦点时,这时空格会引起页面滚动，屏蔽
			bRet = true;
			break;
		}

		MSG* pMsg = os_event;
		if (NULL == pMsg
			||(WM_KEYDOWN != pMsg->message && WM_KEYUP != pMsg->message))
		{// 快捷键只考虑键盘消息
			break;
		}
		if(pMsg->message == WM_KEYDOWN) //lzlong add
		{
			DUIAccel acc(m_Refreshkey);
			if(pMsg->wParam == acc.GetKey())
			{
				if ((0 == acc.GetModifier()&&!PUSH_ALT&&!PUSH_CTRL&&!PUSH_SHIFT)// 未按下辅助键
					||(HOTKEYF_SHIFT == acc.GetModifier() &&PUSH_SHIFT)// Shift
					||(HOTKEYF_CONTROL == acc.GetModifier() &&PUSH_CTRL)// Ctrl
					||(HOTKEYF_ALT == acc.GetModifier() &&PUSH_ALT)//Alt
					)
				{
					Refresh();
				}
			}				
		}

	} while (false);
	return bRet;
}

bool DUINativeWeb::OnBeforePopup(CefRefPtr<CefBrowser> browser, CefRefPtr<CefFrame> frame, const CefString& target_url, const CefString& target_frame_name, CefLifeSpanHandler::WindowOpenDisposition target_disposition, bool user_gesture, const CefPopupFeatures& popupFeatures, CefWindowInfo& windowInfo, CefRefPtr<CefClient>& client, CefBrowserSettings& settings, bool* no_javascript_access)
{
	if (m_pEventHandler && g_pMainUIRunner)
	{	
		if (!g_pMainUIRunner->RunTaskOnMainUI())
		{
			WaitableEvent wait(false, false);
			VARIANT_BOOL* pbCancel = new VARIANT_BOOL;*pbCancel = VARIANT_TRUE;
			g_pMainUIRunner->PostTask(NewCefRunnableMethod(this,&DUINativeWeb::NewWindow3,target_url,pbCancel,&wait));
			bool bWait = wait.Wait(2000);
			bool bRet = (VARIANT_TRUE == *pbCancel);
			if (bWait)
			{// 超时允许内存泄露2字节
				DM_DELETE(pbCancel);
			}
			return bRet;
		}
	}
	return true;
}

void /*CEF主线程调用*/ DUINativeWeb::OnAfterCreated(CefRefPtr<CefBrowser> browser)
{
	std::wstring strUrl = m_strUrl;
	LoadUrl(strUrl);
	UpdateWebRect();
}

void DUINativeWeb::OnAddressChange(CefRefPtr<CefBrowser> browser, CefRefPtr<CefFrame> frame,const CefString& url)
{
	if (m_pEventHandler && g_pMainUIRunner)
	{	
		if (!g_pMainUIRunner->RunTaskOnMainUI())
		{
			g_pMainUIRunner->PostTask(NewCefRunnableMethod(this,&DUINativeWeb::OnAddressChange,browser,frame,url));
		}
		else
		{
			m_pEventHandler->NavigateComplete2(m_hDUIWnd,(IDispatch*)frame.get(),(wchar_t*)url.c_str());
		}
	}
}

void DUINativeWeb::OnTitleChange(CefRefPtr<CefBrowser> browser,const CefString& title)
{
	if (m_pEventHandler && g_pMainUIRunner)
	{	
		if (!g_pMainUIRunner->RunTaskOnMainUI())
		{
			g_pMainUIRunner->PostTask(NewCefRunnableMethod(this,&DUINativeWeb::OnTitleChange,browser,title));
		}
		else
		{
			m_pEventHandler->TitleChange(m_hDUIWnd,(wchar_t*)title.c_str());
		}
	}
}

void DUINativeWeb::OnLoadEnd(CefRefPtr<CefBrowser> browser,CefRefPtr<CefFrame> frame,int httpStatusCode)
{
	if (m_pEventHandler && g_pMainUIRunner)
	{	
		if (!g_pMainUIRunner->RunTaskOnMainUI())
		{
			g_pMainUIRunner->PostTask(NewCefRunnableMethod(this,&DUINativeWeb::OnLoadEnd,browser,frame,httpStatusCode));
		}
		else
		{
			m_pEventHandler->DocumentComplete(m_hDUIWnd,(IDispatch*)frame.get(),(wchar_t*)frame->GetURL().c_str());
		}
	}
}

void DUINativeWeb::OnFullscreenModeChange(CefRefPtr<CefBrowser> browser,bool fullscreen)
{
	if (m_pEventHandler && g_pMainUIRunner)
	{	
		if (!g_pMainUIRunner->RunTaskOnMainUI())
		{
			g_pMainUIRunner->PostTask(NewCefRunnableMethod(this,&DUINativeWeb::OnFullscreenModeChange,browser,fullscreen));
		}
		else
		{
			m_pEventHandler->OnFullScreen(m_hDUIWnd,fullscreen);
		}
	}
}
#pragma endregion

//---------------------------------------------------
// Function Des: private
//---------------------------------------------------
#pragma region private
HRESULT DUINativeWeb::LoadUrl(std::wstring strUrl)
{
	HRESULT hr = S_FALSE;
	do 
	{
		if (strUrl.empty())
		{// 空指针传入CefString会崩溃
			break;
		}
		if (GetBrowser())
		{
			if (!CefCurrentlyOn(TID_UI))
			{// 把操作跳转到Cef线程执行
				CefPostTask(TID_UI, NewCefRunnableMethod(this,&DUINativeWeb::LoadUrl,strUrl));
			}
			else
			{
				CefRefPtr<CefFrame> frame = GetBrowser()->GetMainFrame();
				if (frame)
				{
					frame->LoadURL(CefString(strUrl));
				}
			}
			hr = S_OK;
		}
	} while (false);
	return hr;
}

void DUINativeWeb::UpdateWebRect()
{
	if (GetBrowser())
	{
		if (!CurrentlyOnMainUI())
		{// 把操作跳转到主UI线程执行
			g_pMainUIRunner->PostTask(NewCefRunnableMethod(this,&DUINativeWeb::UpdateWebRect));
		}
		else
		{
			HWND hwnd = GetOleWindow();
			if (hwnd) 
			{
				SetWindowPos(hwnd, HWND_TOP, m_rcWindow.left, m_rcWindow.top, m_rcWindow.Width(), m_rcWindow.Height(), SWP_NOZORDER);
			}
		}
	}
}

bool DUINativeWeb::CurrentlyOnMainUI()
{
	if (GetBrowser())
	{
		if (g_pCefManager && g_pMainUIRunner)
		{
			return g_pMainUIRunner->RunTaskOnMainUI();
		}
	}
	return true;
}

CefRefPtr<CefBrowser> DUINativeWeb::GetBrowser()
{
	if (m_pWebHandler)
	{
		return m_pWebHandler->GetBrowser();
	}
	return NULL;
}

CefRefPtr<CefBrowserHost> DUINativeWeb::GetBrowserHost()
{
	if (m_pWebHandler)
	{
		return m_pWebHandler->GetBrowserHost();
	}
	return NULL;
}

void DUINativeWeb::NewWindow3(const CefString& target_url,VARIANT_BOOL *pbCancel,WaitableEvent*Wait /*= NULL*/)
{
	if (m_pEventHandler)
	{
		m_pEventHandler->NewWindow3(m_hDUIWnd,NULL,pbCancel,0,NULL,(wchar_t*)target_url.ToWString().c_str());
	}

	if (Wait)
	{
		Wait->Signal();
	}
}

void DUINativeWeb::BeforeNavigate2(CefRefPtr<CefFrame> frame,const CefString& strUrl,int iNavType,const CefString& strPostData,const CefString& strHeaderData,VARIANT_BOOL *pbCancel,WaitableEvent*Wait /*= NULL*/)
{
	if (m_pEventHandler)
	{
		_variant_t vPostData = (wchar_t*)strPostData.ToWString().c_str();
		wchar_t* pTargetFrameName = NULL;
		if (frame)
		{
			pTargetFrameName = (wchar_t*)frame->GetName().ToWString().c_str();
		}
		m_pEventHandler->BeforeNavigate2(m_hDUIWnd, (IDispatch*)(frame.get()), (wchar_t*)strUrl.ToWString().c_str(), 
										 iNavType, pTargetFrameName, &vPostData, (wchar_t*)strHeaderData.ToWString().c_str(), pbCancel);
	}

	if (Wait)
	{
		Wait->Signal();
	}
}

bool DUINativeWeb::JsCallbackMessage(CefRefPtr<CefBrowser>& browser, CefProcessId& source_process, CefRefPtr<CefProcessMessage>& message)
{
	CefRefPtr<CefListValue> Args = message->GetArgumentList();
	int iCount = (int)Args->GetSize();
	CefRefPtr<CefDictionaryValue> funcDesc = Args->GetDictionary(iCount-1);
	if (!funcDesc->IsValid())
	{
		return false;
	}

	CefString szFunName = funcDesc->GetString(L"func_name");
	CefString szPipeName = funcDesc->GetString(L"pipe_name");	
	bool bSync = funcDesc->GetBool(L"bsync");
	IDMExternalHandler* pExternalHandler = g_pDMWebApp->GetExternalHandler();

	DM::CArray<CStringW> strArgs;
	for (int i=0; i<iCount-1; i++)
	{
		CStringW strInfo;
		CefValueType type = Args->GetType(i);
		if (VTYPE_NULL		  == type)	strInfo = L"";
		else if (VTYPE_BOOL	  == type)	strInfo.Format(L"%d",Args->GetBool(i));
		else if (VTYPE_INT	  == type)	strInfo.Format(L"%d",Args->GetInt(i));
		else if (VTYPE_DOUBLE == type)	strInfo.Format(L"%lf",Args->GetDouble(i));
		else if (VTYPE_STRING == type)	strInfo = Args->GetString(i).c_str();
		else {}
		strArgs.Add(strInfo);
	}
	DM::CStringW strResult;
	pExternalHandler->CallFunction(GetDUIWnd(),szFunName.c_str(),strArgs,strResult);

	if (bSync)
	{
		NamedPipe Pipe(szPipeName.c_str(),NamedPipe::PIPE_CLIN);
		if (Pipe.IsValid())
		{
			DWORD dwWrite = 0;
			int iSize = strResult.GetLength()*2;
			Pipe.Write((void*)&iSize,4,dwWrite);
			if (iSize>0)
			{
				Pipe.Write(strResult.GetBuffer(),iSize,dwWrite);
			}
		}
	}
	return true;
}

bool DUINativeWeb::BeforeNavigateMessage(CefRefPtr<CefBrowser>& browser, CefProcessId& source_process, CefRefPtr<CefProcessMessage>& message)
{
	CefRefPtr<CefListValue> Args = message->GetArgumentList();
	int iCount = (int)Args->GetSize();
	if (iCount<7)
	{
		return false;
	}

	CefRefPtr<CefDictionaryValue> funcDesc = Args->GetDictionary(6);
	if (!funcDesc->IsValid())
	{
		return false;
	}

	int64 iFrameId = Args->GetInt(0);
	CefRefPtr<CefFrame> frame = browser->GetFrame(iFrameId);
	CefString target_url = Args->GetString(1);
	int navigation_type = Args->GetInt(2);
	CefString post_data = Args->GetString(3);
	int ipost_data_len = Args->GetInt(4);
	CefString header_data = Args->GetString(5);

	bool bRet = false;
	if (m_pEventHandler && g_pMainUIRunner)
	{	
		if (!g_pMainUIRunner->RunTaskOnMainUI())
		{
			WaitableEvent wait(false, false);
			VARIANT_BOOL* pbCancel = new VARIANT_BOOL;*pbCancel = VARIANT_FALSE;
			g_pMainUIRunner->PostTask(NewCefRunnableMethod(this,&DUINativeWeb::BeforeNavigate2, frame, target_url, navigation_type, post_data, header_data, pbCancel, &wait));
			bool bWait = wait.Wait(2000);
			bRet = (VARIANT_TRUE == *pbCancel);
			if (bWait)
			{// 超时允许内存泄露2字节
				DM_DELETE(pbCancel);
			}
		}
	}

	CefString szPipeName = funcDesc->GetString(L"pipe_name");
	NamedPipe Pipe(szPipeName.c_str(),NamedPipe::PIPE_CLIN);
	if (Pipe.IsValid())
	{
		DM::CStringW strResult = bRet?L"true":L"false";
		DWORD dwWrite = 0;
		int iSize = strResult.GetLength()*2;
		Pipe.Write((void*)&iSize,4,dwWrite);
		if (iSize>0)
		{
			Pipe.Write(strResult.GetBuffer(),iSize,dwWrite);
		}
	}
	LOG_INFO("BeforeNavigate2 end\n");
	return true;
}

void DUINativeWeb::OnFinalRelease()
{
	DUINativeWeb::Release();// 调用IMPLEMENT_REFCOUNTING中定义的Release
}
#pragma endregion
