#include "PluginWebAfx.h"
#include "DMWebApp.h"
#include "DUIWeb.h"
#include "DUINativeWeb.h"

IDMWebPtr g_pDMWebApp = NULL;// 外部获取,外部释放,此处不使用引用计数
void DMWebCreate(IDMWebPtr* ppObj)
{
	if (ppObj)
	{
		if (!g_pDMWebApp)
		{
			g_pDMWebApp = new DMWebApp;
		}
		*ppObj = g_pDMWebApp;
	}
}

//----------------------------------------------------------------------------------------------
DMWebApp::DMWebApp()
{
	m_pMainUIRunner = NULL;
	m_pExternalHandler = NULL;
	m_bInit = m_bOsrRender = false;
}

DMWebApp::~DMWebApp()
{
	if (m_bInit)
	{
		UnInitializeWeb();
	}
	m_pExternalHandler = NULL;
}

bool DMWebApp::InitializeWeb(bool bOsrRender /*= true*/,LPCWSTR lpszWebDir /*= NULL*/,IDMExternalHandler* pExternalHandler/* = NULL*/)
{
	bool bRet = m_bInit;
	do 
	{
		if (m_bInit)
		{//0.已初始化
			break;
		}
		m_bOsrRender = bOsrRender;
		m_pExternalHandler = pExternalHandler;
		if (!AddDirToEnvirPath(lpszWebDir))
		{//1.把CEF所在文件夹加入环境变量
			break;
		}
		
		m_pMainUIRunner = new MainUIRunner();
		CefSettings	 Settings;
		InitCefSetting(Settings);
		CefRefPtr<CefListValue> ExtraInfo;
		InitExtraInfo(ExtraInfo);
		if (!g_pCefManager->Initialize(Settings,ExtraInfo,bOsrRender,m_pMainUIRunner))
		{//2.初始化CEF
			break;
		}

		//3.注册控件
		if (g_pCefManager->IsOffsetRender())
		{
			g_pDMApp->Register(DMRegHelperT<DUIWeb>(),true);
		}
		else
		{
			g_pDMApp->Register(DMRegHelperT<DUINativeWeb>(),true);
		}
		m_bInit = bRet = true;
	} while (false);
	return bRet;
}

void DMWebApp::UnInitializeWeb()
{
	if (g_pCefManager->IsOffsetRender())
	{
		g_pDMApp->UnRegister(DUIWeb::GetClassName(),DUIWeb::GetClassType());
	}
	else
	{
		g_pDMApp->UnRegister(DUINativeWeb::GetClassName(),DUINativeWeb::GetClassType());
	}
	g_pCefManager->UnInitialize();
	DM_DELETE(m_pMainUIRunner);
	m_bInit = false;
}

void DMWebApp::PostQuitMsg()
{
	g_pCefManager->PostQuitMsg();
}

IDMExternalHandler* DMWebApp::GetExternalHandler()
{
	return m_pExternalHandler;
}

bool isSpace(wchar_t s)
{
	return (s == L' ')?true:false;
}
bool DMWebApp::SetCookieForUrl(LPCWSTR lpszUrl, LPCWSTR lpszCookie,LPCWSTR lpszDoMain,bool bSync/* = false*/)
{
	if (false == m_bInit || false == IsValidString(lpszCookie) || false == IsValidString(lpszUrl))
	{
		return false;
	}

	std::wstring strUrl = SAFE_STR(lpszUrl);std::wstring strCookie = SAFE_STR(lpszCookie);std::wstring strDoMain = SAFE_STR(lpszDoMain);
	if (!CefCurrentlyOn(TID_IO))
	{ // 非IO线程，把任务转移过去
		WaitableEvent wait(false, false);
		CefPostTask(TID_IO,NewCefRunnableMethod(this,&DMWebApp::SetCefCookieForUrl,strUrl,strCookie,strDoMain,bSync,&wait));
		return wait.Wait(2000);
	}
	return SetCefCookieForUrl(strUrl,strCookie,strDoMain,bSync,NULL);
}

// private--------------------------------------------------------------------------------------------
bool DMWebApp::AddDirToEnvirPath(LPCWSTR lpszWebDir)
{
	bool bRet = false;
	do 
	{
		std::wstring strWebDir = SAFE_STR(lpszWebDir);
		if (strWebDir.empty())
		{
			break;
		}
		if (!PathFileExistsW(strWebDir.c_str()))
		{
			CStringW strErr;
			strErr.Format(L"CEF文件夹所在目录:%s 不存在",strWebDir.c_str());
			DMASSERT_EXPR(false,strErr);
			break;
		}

		//加入环境变量
		wchar_t szPathEnvir[4096] = {0};
		GetEnvironmentVariableW(L"path", szPathEnvir, 4096);
		CStringW strPath = szPathEnvir;
		strPath += L";";
		strPath += strWebDir.c_str();
		SetEnvironmentVariableW(L"path", strPath);

		bRet = true;
	} while (false);
	return bRet;
}

void DMWebApp::InitCefSetting(CefSettings& Settings)
{
#ifdef _DEBUG
	Settings.single_process = true;
#else
	Settings.single_process = false;
#endif

	Settings.no_sandbox = true;
	Settings.multi_threaded_message_loop = true;
	Settings.windowless_rendering_enabled = m_bOsrRender;
	CefString(&Settings.locale) = L"zh-CN";// 右键菜单变为中文
	
	wchar_t szAppDir[MAX_PATH] = {0};
	DM::GetRootDirW(szAppDir,MAX_PATH);
	std::wstring strAppDir = szAppDir;
	//CefString(&Settings.cache_path) = strAppDir + L"CefLocalStorage";// 设置localstorage，不要在路径末尾加"\\"，否则运行时会报错

	// 设置debug log文件位置
	CefString(&Settings.log_file) = strAppDir + L"cef.log";

}

void DMWebApp::InitExtraInfo(CefRefPtr<CefListValue>& ExtraInfo)
{
	ExtraInfo = CefListValue::Create();
	if (m_pExternalHandler)
	{
		CefRefPtr<CefListValue> FunNames = CefListValue::Create();
		DM::CArray<CStringW> NameArray;
		m_pExternalHandler->GetAllFunNames(NameArray);
		FunNames->SetSize(NameArray.GetCount());
		for (size_t i=0; i<NameArray.GetCount();i++)
		{
			FunNames->SetString((int)i, CefString(NameArray[i]));
		}
		ExtraInfo->SetList(EXTRAINFO_FUNNAMES,FunNames);
	}
}

bool DMWebApp::SetCefCookieForUrl(std::wstring strUrl,std::wstring strCookie,std::wstring strDoMain,bool bSync /*= false*/,WaitableEvent*Wait)
{
	DMASSERT(CefCurrentlyOn(TID_IO));
	std::wstring _strCookie = strCookie;
	_strCookie.erase(std::remove_if(_strCookie.begin(), _strCookie.end(), isSpace),_strCookie.end());
	int _nStart = 0;
	int nCount = 0;
	do 
	{
		std::wstring::size_type _nEnd = _strCookie.find(L'=', _nStart);
		if (std::wstring::npos != _nEnd)
		{
			std::wstring _strPrev = _strCookie.substr(_nStart, _nEnd - _nStart);
			std::wstring::size_type _nPrevEnd = _strPrev.find(L';', 0); // 出现了project;的现象
			if (_nPrevEnd != std::wstring::npos) 
			{
				++_nPrevEnd;
				_strPrev = _strPrev.substr(_nPrevEnd, _strPrev.length() - _nPrevEnd);
			}

			_nStart = _nEnd + 1;
			_nEnd = _strCookie.find(L';', _nStart);
			std::wstring _strEnd = (_strCookie.substr(_nStart, (-1 == _nEnd) ? (_strCookie.length() - _nStart) : _nEnd - _nStart));
			if(g_pCefManager->SetCookie(strUrl,_strPrev,_strEnd,strDoMain))// 到此处肯定是在TID_IO线程了
			{
				nCount++;
			}

		}
		_nStart = (std::wstring::npos != _nEnd) ? _nEnd + 1 : _nEnd;
	} while (_nStart != std::wstring::npos);

	if (Wait)
		Wait->Signal();
	return nCount != 0;
}
