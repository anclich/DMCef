#include "PluginWebAfx.h"
#include "DUIWeb.h"
#include <exdisp.h>

#define  CEF_SETCURSOR_LPARAM		0x87654321
DUIWeb::DUIWeb()
{
	m_iOrgSize = m_iOrgPopSize = 0;
	m_pEventHandler = NULL;
	m_bShowContext = true;
	m_Refreshkey = DUIAccel::TranslateAccelKey(L"f5");
	DUIWeb::AddRef();
}

DUIWeb::~DUIWeb()
{

}

//---------------------------------------------------
// Function Des: IDUIWeb对外接口
//---------------------------------------------------
#pragma region IDUIWeb
HRESULT DUIWeb::SetEvtHandler(IDMWebEvent* pEventHandler)
{
	m_pEventHandler = pEventHandler;	
	return S_OK;
}

HRESULT DUIWeb::OpenUrl(LPCWSTR pszURL,int iFlags /*= 0*/,LPCWSTR pszTargetFrameName /*= NULL*/,LPCWSTR pszHeaders /*= NULL*/, LPCSTR pszPostData /*= NULL*/,int iPostDataLen /*= 0*/)
{
	if (!IsValidString(pszURL))
	{
		return S_FALSE;
	}
	m_strUrl = pszURL;
	return LoadUrl(pszURL);
}

HRESULT DUIWeb::GetUrl(LPWSTR pszUrl, int nMaxLen)
{
	HRESULT hr = S_FALSE;
	do 
	{
		if (GetBrowser())
		{
			CefRefPtr<CefFrame> frame = GetBrowser()->GetMainFrame();
			if (frame)
			{
				CefString Url = frame->GetURL();
				std::wstring strUrl = Url.ToWString();
				if (nMaxLen < (int)strUrl.length())
				{
					break;
				}
				ZeroMemory(pszUrl, nMaxLen*sizeof(wchar_t));
				if (!strUrl.empty())
				{
					memcpy(pszUrl, strUrl.c_str(), strUrl.length()*sizeof(wchar_t));
				}
				hr = S_OK;
			}
		}
	} while (false);
	return hr;
}

HWND DUIWeb::GetOleWindow()
{
	if (GetBrowserHost())
	{
		return GetBrowserHost()->GetWindowHandle();
	}

	return NULL;
}

bool DUIWeb::IsBusy()
{
	if (GetBrowser())
	{
		return GetBrowser()->IsLoading();
	}
	return false;
}

HRESULT DUIWeb::Stop()
{
	HRESULT hr = S_FALSE;
	do 
	{
		if (GetBrowser())
		{
			GetBrowser()->StopLoad();
			hr = S_OK;
		}
	} while (false);
	return hr;
}

HRESULT DUIWeb::Quit()
{
	HRESULT hr = S_FALSE;
	do 
	{
		GetContainer()->OnUnRegisterDragDrap(m_hDUIWnd);
		m_pEventHandler = NULL;
		if (GetBrowserHost())
		{
			DMAutoLock autlock(&m_OsrLock);
			g_pDMApp->RemoveMessageFilter(this);
			m_pWebHandler->GetBrowserHost()->CloseBrowser(true);
			m_pWebHandler->SetHostWindow(NULL);
			m_pWebHandler->SetDelegate(NULL);
			m_pWebHandler->SetOsrDelegate(NULL);
			m_pWebHandler = NULL;
			hr = S_OK;
		}
	} while (false);
	return hr;
}

HRESULT DUIWeb::Refresh()
{
	HRESULT hr = S_FALSE;
	do 
	{
		if (GetBrowser())
		{
			GetBrowser()->Reload();
			hr = S_OK;
		}
	} while (false);
	return hr;
}

HRESULT DUIWeb::Refresh2(UINT32 nLevel)
{
	HRESULT hr = S_FALSE;
	do 
	{
		if (GetBrowser())
		{
			if (REFRESH_NORMAL == nLevel)
			{
				GetBrowser()->Reload();
			}
			else// 页面失效或无论是否失效都强制从服务器取cache
			{
				GetBrowser()->ReloadIgnoreCache();
			}
			hr = S_OK;
		}
	} while (false);
	return hr;
}

HRESULT DUIWeb::GoBack()
{
	HRESULT hr = S_FALSE;
	do 
	{
		if (GetBrowser())
		{
			GetBrowser()->GoBack();
			hr = S_OK;
		}
	} while (false);
	return hr;
}

HRESULT DUIWeb::GoForward()
{
	HRESULT hr = S_FALSE;
	do 
	{
		if (GetBrowser())
		{
			GetBrowser()->GoForward();
			hr = S_OK;
		}
	} while (false);
	return hr;
}

HRESULT DUIWeb::ExecuteScript(LPCWSTR pszScript)
{
	HRESULT hr = S_FALSE;
	do 
	{
		if (!IsValidString(pszScript))
		{
			break;
		}
		if (GetBrowser())
		{
			CefRefPtr<CefFrame> frame = GetBrowser()->GetMainFrame();
			if (frame)
			{
				frame->ExecuteJavaScript(CefString(pszScript),frame->GetURL(),0);
				hr = S_OK;
			}
		}
	} while (false);
	return hr;
}

HRESULT DUIWeb::ExecuteScriptFuntion(LPCWSTR pszFun, const DM::CArray<LPCWSTR>& vecParams, LPWSTR strResult /*= NULL*/,int nMaxLen /*= -1*/)
{
	HRESULT hr = S_FALSE;
	do 
	{
		if (!IsValidString(pszFun))
		{
			break;
		}
		CStringW strJS = pszFun;
		strJS += L"(";
		int iCount = (int)vecParams.GetCount();
		for (int i=0; i< iCount; i++)
		{
			CStringW strParam = vecParams[i];
			strParam.Replace(L"\\", L"\\\\"); // 把所有‘\’替换成‘\\’
			strParam.Replace(L"\'", L"\\\'"); // 把所有‘'’替换成‘\'’
			strParam.Replace(L"\r", L"\\r");  // 把所有‘\r’替换成‘\\r’
			strParam.Replace(L"\n", L"\\n");  // 把所有‘\n’替换成‘\\n’
			strJS += L"\'";
			strJS += strParam;
			strJS += L"\'";
			if (i!=iCount-1)
			{
				strJS += L",";
			}
		}
		strJS += L")";
		hr =  ExecuteScript(strJS);
	} while (false);
	return hr;
}

HRESULT DUIWeb::SetContextMenuShow(bool bShow)
{
	m_bShowContext = bShow;
	return S_OK;
}

HRESULT DUIWeb::WebSetAttribute(LPCWSTR pszAttribute,LPCWSTR pszValue,bool bLoadXml)
{
	if (DMSUCCEEDED(SetAttribute(pszAttribute,pszValue,bLoadXml)))
	{
		return S_OK;
	}
	return S_FALSE;
}
#pragma endregion

//---------------------------------------------------
// Function Des: Delegate实现
//---------------------------------------------------
#pragma region Delegate
bool DUIWeb::OnProcessMessageReceived(CefRefPtr<CefBrowser> browser, CefProcessId source_process, CefRefPtr<CefProcessMessage> message)
{
	if (source_process == PID_RENDERER)
	{
		if (!message->IsValid()||NULL == g_pDMWebApp || NULL == g_pDMWebApp->GetExternalHandler()) 
		{
			return false;
		}

		CefRefPtr<CefListValue> Args = message->GetArgumentList();
		if (message->GetName() == kJsCallbackMessage && Args->GetSize() > 0)
		{
			return JsCallbackMessage(browser,source_process,message);
		}
		if (message->GetName() == kBeforeNavigateMessage)
		{
			return BeforeNavigateMessage(browser,source_process,message);
		}
	}

	return false;
}

void DUIWeb::OnBeforeContextMenu(CefRefPtr<CefBrowser> browser, CefRefPtr<CefFrame> frame, CefRefPtr<CefContextMenuParams> params, CefRefPtr<CefMenuModel> model)
{
	if (!m_bShowContext)
	{
		if (model->GetCount() > 0)
		{
			model->Clear();// 禁止右键菜单
		}
	}
}

bool DUIWeb::OnPreKeyEvent(CefRefPtr<CefBrowser> browser,const CefKeyEvent& event,CefEventHandle os_event,bool* is_keyboard_shortcut)
{
	bool bRet = false;
	do 
	{
		if (!event.focus_on_editable_field && event.windows_key_code == VK_SPACE) 
		{// 不处于编辑焦点时,这时空格会引起页面滚动，屏蔽
			bRet = true;
			break;
		}

		MSG* pMsg = os_event;
		if (NULL == pMsg
			||(WM_KEYDOWN != pMsg->message && WM_KEYUP != pMsg->message))
		{// 快捷键只考虑键盘消息
			break;
		}
		if(pMsg->message == WM_KEYDOWN) //lzlong add
		{
			DUIAccel acc(m_Refreshkey);
			if(pMsg->wParam == acc.GetKey())
			{
				if ((0 == acc.GetModifier()&&!PUSH_ALT&&!PUSH_CTRL&&!PUSH_SHIFT)// 未按下辅助键
					||(HOTKEYF_SHIFT == acc.GetModifier() &&PUSH_SHIFT)// Shift
					||(HOTKEYF_CONTROL == acc.GetModifier() &&PUSH_CTRL)// Ctrl
					||(HOTKEYF_ALT == acc.GetModifier() &&PUSH_ALT)//Alt
					)
				{
					Refresh();
				}
			}				
		}

	} while (false);
	return bRet;
}

bool DUIWeb::OnBeforePopup(CefRefPtr<CefBrowser> browser, CefRefPtr<CefFrame> frame, const CefString& target_url, const CefString& target_frame_name, CefLifeSpanHandler::WindowOpenDisposition target_disposition, bool user_gesture, const CefPopupFeatures& popupFeatures, CefWindowInfo& windowInfo, CefRefPtr<CefClient>& client, CefBrowserSettings& settings, bool* no_javascript_access)
{
	if (m_pEventHandler && g_pMainUIRunner)
	{	
		if (!g_pMainUIRunner->RunTaskOnMainUI())
		{
			WaitableEvent wait(false, false);
			VARIANT_BOOL* pbCancel = new VARIANT_BOOL;*pbCancel = VARIANT_TRUE;
			g_pMainUIRunner->PostTask(NewCefRunnableMethod(this,&DUIWeb::NewWindow3,target_url,pbCancel,&wait));
			bool bWait = wait.Wait(2000);
			bool bRet = (VARIANT_TRUE == *pbCancel);
			if (bWait)
			{// 超时允许内存泄露2字节
				DM_DELETE(pbCancel);
			}
			return bRet;
		}
	}
	return true;
}

void /*CEF主线程调用*/ DUIWeb::OnAfterCreated(CefRefPtr<CefBrowser> browser)
{
	std::wstring strUrl = m_strUrl;
	LoadUrl(strUrl);
}

void DUIWeb::OnAddressChange(CefRefPtr<CefBrowser> browser, CefRefPtr<CefFrame> frame,const CefString& url)
{
	if (m_pEventHandler && g_pMainUIRunner)
	{	
		if (!g_pMainUIRunner->RunTaskOnMainUI())
		{
			g_pMainUIRunner->PostTask(NewCefRunnableMethod(this,&DUIWeb::OnAddressChange,browser,frame,url));
		}
		else
		{
			m_pEventHandler->NavigateComplete2(m_hDUIWnd,(IDispatch*)frame.get(),(wchar_t*)url.c_str());
		}
	}
}

void DUIWeb::OnTitleChange(CefRefPtr<CefBrowser> browser,const CefString& title)
{
	if (m_pEventHandler && g_pMainUIRunner)
	{	
		if (!g_pMainUIRunner->RunTaskOnMainUI())
		{
			g_pMainUIRunner->PostTask(NewCefRunnableMethod(this,&DUIWeb::OnTitleChange,browser,title));
		}
		else
		{
			m_pEventHandler->TitleChange(m_hDUIWnd,(wchar_t*)title.c_str());
		}
	}
}

void DUIWeb::OnLoadEnd(CefRefPtr<CefBrowser> browser,CefRefPtr<CefFrame> frame,int httpStatusCode)
{
	if (m_pEventHandler && g_pMainUIRunner)
	{	
		if (!g_pMainUIRunner->RunTaskOnMainUI())
		{
			g_pMainUIRunner->PostTask(NewCefRunnableMethod(this,&DUIWeb::OnLoadEnd,browser,frame,httpStatusCode));
		}
		else
		{
			m_pEventHandler->DocumentComplete(m_hDUIWnd,(IDispatch*)frame.get(),(wchar_t*)frame->GetURL().c_str());
		}
	}
}

void DUIWeb::OnFullscreenModeChange(CefRefPtr<CefBrowser> browser,bool fullscreen)
{
	if (m_pEventHandler && g_pMainUIRunner)
	{	
		if (!g_pMainUIRunner->RunTaskOnMainUI())
		{
			g_pMainUIRunner->PostTask(NewCefRunnableMethod(this,&DUIWeb::OnFullscreenModeChange,browser,fullscreen));
		}
		else
		{
			m_pEventHandler->OnFullScreen(m_hDUIWnd,fullscreen);
		}
	}
}
#pragma endregion

//---------------------------------------------------
// Function Des: OsrDelegate实现
//---------------------------------------------------
#pragma region CefRenderHandler
bool DUIWeb::GetViewRect(CefRefPtr<CefBrowser> browser,CefRect& rect)
{
	return false;
}

void /*CEF主线程调用*/DUIWeb::OnPaint(CefRefPtr<CefBrowser> browser,PaintElementType type,const RectList& dirtyRects,const void* buffer,int width,int height)
{
	if (NULL == buffer || width <=0 || height <=0 || type >PET_POPUP)
	{
		return;
	}
	DMAutoLock autolock(&m_OsrLock);
	if (NULL == GetBrowser())
	{// 和OnDestroy互锁
		return;
	}
	if (PET_VIEW == type)
	{
		if (m_iOrgSize != width*height*4)
		{// 重新分配大小
			m_iOrgSize = width*height*4;
			m_pOrgBuf.AllocateBytes(m_iOrgSize);
		}
		memcpy(m_pOrgBuf.get(), (LPBYTE)buffer,m_iOrgSize);
		if (!m_rcOsrPop.IsEmpty())
		{// OsrWindowWin::OnPaint,强刷Pop
			browser->GetHost()->Invalidate(PET_POPUP);
		}
	}
	else if (PET_POPUP == type)
	{
		if (m_iOrgPopSize != width*height*4)
		{// 重新分配大小
			m_iOrgPopSize = width*height*4;
			m_pOrgPopBuf.AllocateBytes(m_iOrgPopSize);
		}
		memcpy(m_pOrgPopBuf.get(), (LPBYTE)buffer,m_iOrgPopSize);
	}
	if (g_pMainUIRunner)
	{
		g_pMainUIRunner->PostTask(NewCefRunnableMethod(this,&DUIWeb::OsrOnPaint,type,dirtyRects,width,height));
	}
}

void DUIWeb::OnPopupShow(CefRefPtr<CefBrowser> browser, bool show)
{
	if (!show)
	{	
		// 当popup窗口隐藏时，刷新popup区域
		CefRect rect_dirty = m_rcOsrPop;
		m_rcOsrPop.Set(0, 0, 0, 0);
		browser->GetHost()->Invalidate(PET_VIEW);	
	}
}

void DUIWeb::OnPopupSize(CefRefPtr<CefBrowser> browser, const CefRect& rect)
{
	if (rect.width >0 && rect.height >0)
	{
		m_rcOsrPop = rect;
	}
}

void DUIWeb::OnCursorChange(CefRefPtr<CefBrowser> browser, CefCursorHandle cursor, CursorType type, const CefCursorInfo& custom_cursor_info)
{
	if (GetBrowser())
	{
		// Change the plugin window's cursor.
		::SetClassLongPtr(GetContainer()->OnGetHWnd(), GCLP_HCURSOR, static_cast<LONG>(reinterpret_cast<LONG_PTR>(cursor)));
		::SetCursor(cursor);
		//::PostMessage(GetContainer()->OnGetHWnd(),WM_SETCURSOR,(WPARAM)cursor,CEF_SETCURSOR_LPARAM);
	}
}

bool DUIWeb::StartDragging(CefRefPtr<CefBrowser> browser,CefRefPtr<CefDragData> drag_data,CefRenderHandler::DragOperationsMask allowed_ops,int x, int y)
{
	if (!m_pDropTarget)
		return false;

	m_pCurDropOp = DRAG_OPERATION_NONE;
	CefBrowserHost::DragOperationsMask result = m_pDropTarget->StartDragging(browser, drag_data, allowed_ops, x, y);
	m_pCurDropOp = DRAG_OPERATION_NONE;
	
	POINT pt = {};
	GetCursorPos(&pt);
	ScreenToClient(GetContainer()->OnGetHWnd(), &pt);

	GetBrowserHost()->DragSourceEndedAt(pt.x, pt.y, result);
	GetBrowserHost()->DragSourceSystemDragEnded();
	return true;
}

void DUIWeb::UpdateDragCursor(CefRefPtr<CefBrowser> browser,CefRenderHandler::DragOperation operation)
{
	m_pCurDropOp = operation;
}
#pragma endregion

//---------------------------------------------------
// Function Des: OsrDragEvents实现
//---------------------------------------------------
#pragma region OsrDragEvents
CefBrowserHost::DragOperationsMask DUIWeb::OnDragEnter(CefRefPtr<CefDragData> drag_data,CefMouseEvent ev,CefBrowserHost::DragOperationsMask effect)
{
	if (GetBrowserHost())
	{
		GetBrowserHost()->DragTargetDragEnter(drag_data, ev, effect);
		GetBrowserHost()->DragTargetDragOver(ev, effect);
	}
	return m_pCurDropOp;
}

CefBrowserHost::DragOperationsMask DUIWeb::OnDragOver(CefMouseEvent ev,CefBrowserHost::DragOperationsMask effect)
{
	if (GetBrowserHost())
	{
		CRect rcClient;
		DV_GetClientRect(rcClient);
		ev.x -= rcClient.left;
		ev.y -= rcClient.top;
		GetBrowserHost()->DragTargetDragOver(ev, effect);
	}
	return m_pCurDropOp;
}

void DUIWeb::OnDragLeave()
{
	if (GetBrowserHost())
	{
		GetBrowserHost()->DragTargetDragLeave();
	}
}

CefBrowserHost::DragOperationsMask DUIWeb::OnDrop(CefMouseEvent ev,CefBrowserHost::DragOperationsMask effect)
{
	if (GetBrowserHost())
	{
		CRect rcClient;
		DV_GetClientRect(rcClient);
		ev.x -= rcClient.left;
		ev.y -= rcClient.top;
		GetBrowserHost()->DragTargetDragOver(ev, effect);
		GetBrowserHost()->DragTargetDrop(ev);
	}
	return m_pCurDropOp;
}
#pragma endregion


//---------------------------------------------------
// Function Des: private
//---------------------------------------------------
#pragma region private
HRESULT DUIWeb::LoadUrl(std::wstring strUrl)
{
	HRESULT hr = S_FALSE;
	do 
	{
		if (strUrl.empty())
		{// 空指针传入CefString会崩溃
			break;
		}
		if (GetBrowser())
		{
			if (!CefCurrentlyOn(TID_UI))
			{// 把操作跳转到Cef线程执行
				CefPostTask(TID_UI, NewCefRunnableMethod(this,&DUIWeb::LoadUrl,strUrl));
			}
			else
			{
				CefRefPtr<CefFrame> frame = GetBrowser()->GetMainFrame();
				if (frame)
				{
					frame->LoadURL(CefString(strUrl));
				}
				if (GetBrowserHost())
				{// 防止初始时出现双焦点
					GetBrowserHost()->SendFocusEvent(DM_IsFocusWnd());
				}
			}
			hr = S_OK;
		}
	} while (false);
	return hr;
}

bool DUIWeb::InitOsrCanvas(PaintElementType type,int iWidth,int iHei)
{
	bool bRet = false;
	do 
	{
		if (iWidth<=0||iHei<=0||type>PET_POPUP)
		{
			break;
		}
		if (PET_VIEW == type && NULL == m_pOsrCanvas)
		{
			DMSmartPtrT<IDMRender> pRender;
			g_pDMApp->GetDefRegObj((void**)&pRender, DMREG_Render);
			if (DMSUCCEEDED(pRender->CreateCanvas(iWidth,iHei,&m_pOsrCanvas)))
			{
				bRet = true;
			}
			break;
		}
		if (PET_POPUP == type && NULL == m_pOsrPopCanvas)
		{
			DMSmartPtrT<IDMRender> pRender;
			g_pDMApp->GetDefRegObj((void**)&pRender, DMREG_Render);
			if (DMSUCCEEDED(pRender->CreateCanvas(iWidth,iHei,&m_pOsrPopCanvas)))
			{
				bRet = true;
			}
			break;
		}

		// 到此处pOsr不可能为NULL
		DMSmartPtrT<IDMCanvas> pOsr = (PET_VIEW == type)?m_pOsrCanvas:m_pOsrPopCanvas;
		CSize szOrg;
		pOsr->GetSize(szOrg);
		if (szOrg.cx != iWidth || szOrg.cy != iHei)
		{
			if (DMSUCCEEDED(pOsr->Resize(iWidth,iHei)))
			{
				bRet = true;
			}
		}
		else
		{
			bRet = true;
		}
	} while (false);
	return bRet;
}

void DUIWeb::OsrOnPaint(PaintElementType type,const RectList& dirtyRects,int width,int height)
{
	do 
	{
		DMAutoLock autolock(&m_OsrLock);
		if (!GetBrowser())
		{
			break;
		}
		int iRealSize = (PET_VIEW == type)?m_iOrgSize:m_iOrgPopSize;
		if (iRealSize != width*height*4)
		{
			break;
		}
		if (false == InitOsrCanvas(type,width,height))
		{
			break;
		}
		DMSmartPtrT<IDMCanvas> pOsr = (PET_VIEW == type)?m_pOsrCanvas:m_pOsrPopCanvas;
		int iSize = 0;
		LPBYTE pDest = (LPBYTE)pOsr->GetPixelBits(&iSize);
		LPBYTE pSrc = (PET_VIEW == type)?m_pOrgBuf.get():m_pOrgPopBuf.get();
		if (NULL!=pDest&& NULL!=pSrc)
		{
			DMASSERT(iSize == iRealSize);
			memcpy(pDest,pSrc,iSize);
		}
		CRect rcClient;
		DV_GetClientRect(rcClient);
		for (std::vector<CefRect>::const_iterator it = dirtyRects.begin();it != dirtyRects.end();++it)
		{
			CRect rcNeed(it->x,it->y,it->x+it->width,it->y+it->height);
			rcNeed.OffsetRect(rcClient.TopLeft());// 换成DUI坐标
			if (type == PET_POPUP)
			{
				rcNeed.OffsetRect(m_rcOsrPop.x,m_rcOsrPop.y);
			}
			DM_InvalidateRect(rcNeed);
		}
	} while (false);
}

bool DUIWeb::IsCaptured()
{
	return DM_GetCapture() == m_hDUIWnd;
}

CefRefPtr<CefBrowser> DUIWeb::GetBrowser()
{
	if (m_pWebHandler)
	{
		return m_pWebHandler->GetBrowser();
	}
	return NULL;
}

CefRefPtr<CefBrowserHost> DUIWeb::GetBrowserHost()
{
	if (m_pWebHandler)
	{
		return m_pWebHandler->GetBrowserHost();
	}
	return NULL;
}

void DUIWeb::NewWindow3(const CefString& target_url,VARIANT_BOOL *pbCancel,WaitableEvent*Wait /*= NULL*/)
{
	if (m_pEventHandler)
	{
		m_pEventHandler->NewWindow3(m_hDUIWnd,NULL,pbCancel,0,NULL,(wchar_t*)target_url.c_str());
	}

	if (Wait)
	{
		Wait->Signal();
	}
}

void DUIWeb::BeforeNavigate2(CefRefPtr<CefFrame> frame,const CefString& strUrl,int iNavType,const CefString& strPostData,const CefString& strHeaderData,VARIANT_BOOL *pbCancel,WaitableEvent*Wait /*= NULL*/)
{
	if (m_pEventHandler)
	{
		_variant_t vPostData = (wchar_t*)strPostData.ToWString().c_str();
		wchar_t* pTargetFrameName = NULL;
		if (frame)
		{
			pTargetFrameName = (wchar_t*)frame->GetName().ToWString().c_str();
		}
		m_pEventHandler->BeforeNavigate2(m_hDUIWnd, (IDispatch*)(frame.get()), (wchar_t*)strUrl.ToWString().c_str(), 
			iNavType, pTargetFrameName, &vPostData, (wchar_t*)strHeaderData.ToWString().c_str(), pbCancel);
	}

	if (Wait)
	{
		Wait->Signal();
	}
}

bool DUIWeb::JsCallbackMessage(CefRefPtr<CefBrowser>& browser, CefProcessId& source_process, CefRefPtr<CefProcessMessage>& message)
{
	CefRefPtr<CefListValue> Args = message->GetArgumentList();
	int iCount = (int)Args->GetSize();
	CefRefPtr<CefDictionaryValue> funcDesc = Args->GetDictionary(iCount-1);
	if (!funcDesc->IsValid())
	{
		return false;
	}

	CefString szFunName = funcDesc->GetString(L"func_name");
	CefString szPipeName = funcDesc->GetString(L"pipe_name");	
	bool bSync = funcDesc->GetBool(L"bsync");
	IDMExternalHandler* pExternalHandler = g_pDMWebApp->GetExternalHandler();

	DM::CArray<CStringW> strArgs;
	for (int i=0; i<iCount-1; i++)
	{
		CStringW strInfo;
		CefValueType type = Args->GetType(i);
		if (VTYPE_NULL		  == type)	strInfo = L"";
		else if (VTYPE_BOOL	  == type)	strInfo.Format(L"%d",Args->GetBool(i));
		else if (VTYPE_INT	  == type)	strInfo.Format(L"%d",Args->GetInt(i));
		else if (VTYPE_DOUBLE == type)	strInfo.Format(L"%lf",Args->GetDouble(i));
		else if (VTYPE_STRING == type)	strInfo = Args->GetString(i).c_str();
		else {}
		strArgs.Add(strInfo);
	}
	DM::CStringW strResult;
	pExternalHandler->CallFunction(GetDUIWnd(),szFunName.c_str(),strArgs,strResult);

	if (bSync)
	{
		NamedPipe Pipe(szPipeName.c_str(),NamedPipe::PIPE_CLIN);
		if (Pipe.IsValid())
		{
			DWORD dwWrite = 0;
			int iSize = strResult.GetLength()*2;
			Pipe.Write((void*)&iSize,4,dwWrite);
			if (iSize>0)
			{
				Pipe.Write(strResult.GetBuffer(),iSize,dwWrite);
			}
		}
	}
	return true;
}

bool DUIWeb::BeforeNavigateMessage(CefRefPtr<CefBrowser>& browser, CefProcessId& source_process, CefRefPtr<CefProcessMessage>& message)
{
	CefRefPtr<CefListValue> Args = message->GetArgumentList();
	int iCount = (int)Args->GetSize();
	if (iCount<7)
	{
		return false;
	}

	CefRefPtr<CefDictionaryValue> funcDesc = Args->GetDictionary(6);
	if (!funcDesc->IsValid())
	{
		return false;
	}

	int64 iFrameId = Args->GetInt(0);
	CefRefPtr<CefFrame> frame = browser->GetFrame(iFrameId);
	CefString target_url = Args->GetString(1);
	int navigation_type = Args->GetInt(2);
	CefString post_data = Args->GetString(3);
	int ipost_data_len = Args->GetInt(4);
	CefString header_data = Args->GetString(5);

	bool bRet = false;
	if (m_pEventHandler && g_pMainUIRunner)
	{	
		if (!g_pMainUIRunner->RunTaskOnMainUI())
		{
			WaitableEvent wait(false, false);
			VARIANT_BOOL* pbCancel = new VARIANT_BOOL;*pbCancel = VARIANT_FALSE;
			g_pMainUIRunner->PostTask(NewCefRunnableMethod(this,&DUIWeb::BeforeNavigate2, frame, target_url, navigation_type, post_data, header_data, pbCancel, &wait));
			bool bWait = wait.Wait(2000);
			bRet = (VARIANT_TRUE == *pbCancel);
			if (bWait)
			{// 超时允许内存泄露2字节
				DM_DELETE(pbCancel);
			}
		}
	}

	CefString szPipeName = funcDesc->GetString(L"pipe_name");
	NamedPipe Pipe(szPipeName.c_str(),NamedPipe::PIPE_CLIN);
	if (Pipe.IsValid())
	{
		DM::CStringW strResult = bRet?L"true":L"false";
		DWORD dwWrite = 0;
		int iSize = strResult.GetLength()*2;
		Pipe.Write((void*)&iSize,4,dwWrite);
		if (iSize>0)
		{
			Pipe.Write(strResult.GetBuffer(),iSize,dwWrite);
		}
	}

	return true;
}
#pragma endregion

//---------------------------------------------------
// Function Des: IDMMessageFilter实现
//---------------------------------------------------
#pragma region IDMMessageFilter
DMCode DUIWeb::DV_OnSetCursor(const CPoint &pt)
{
	// 这里拦截WM_SETCURSOR消息，不让DM处理（DM会改变光标样式），否则会影响Cef中的鼠标光标
	return DM_ECODE_OK;
}

BOOL DUIWeb::PreTranslateMessage(MSG* pMsg)
{//OsrWindowWin::OsrWndProc
	if (!DM_IsVisible(true) || DM_IsDisable(true) || !GetBrowser())
	{
		return FALSE;
	}

	BOOL bHandled = FALSE;
	UINT uMsg = pMsg->message;
	WPARAM wParam = pMsg->wParam;
	LPARAM lParam = pMsg->lParam;
	switch (uMsg)
	{
	case WM_MOUSEMOVE:
		{
			SendMouseMoveEvent(uMsg, wParam, lParam, bHandled);
		}
		break;

	case WM_SETCURSOR:
		{
#if 0
			if (CEF_SETCURSOR_LPARAM == lParam)
			{
				POINT pt;
				::GetCursorPos(&pt);
				::ScreenToClient(GetContainer()->OnGetHWnd(), &pt);
				CRect rcClient;
				DV_GetClientRect(rcClient);
				if (rcClient.PtInRect(pt))
				{// cef cursor是异步的
					::SetCursor((HCURSOR)wParam);
					bHandled = TRUE;
				}
			}
#endif
		}
		break;

	case WM_LBUTTONDOWN:
	case WM_MBUTTONDOWN:
	case WM_RBUTTONDOWN:
		{
			SendButtonDownEvent(uMsg, wParam, lParam, bHandled);
		}
		break;

	case WM_LBUTTONUP:
	case WM_MBUTTONUP:
	case WM_RBUTTONUP:
		{
			SendButtonUpEvent(uMsg, wParam, lParam, bHandled);
		}
		break;

	case WM_SYSCHAR:
	case WM_SYSKEYDOWN:
	case WM_SYSKEYUP:
	case WM_KEYDOWN:
	case WM_KEYUP:
	case WM_CHAR:
		{
			SendKeyEvent(uMsg, wParam, lParam, bHandled);
		}
		break;

	case WM_LBUTTONDBLCLK:
	case WM_MBUTTONDBLCLK:
	case WM_RBUTTONDBLCLK:
		{
			SendButtonDoubleDownEvent(uMsg, wParam, lParam, bHandled);
		}
		break;

	case WM_CAPTURECHANGED:
	case WM_CANCELMODE:
		{
			SendCaptureLostEvent(uMsg, wParam, lParam, bHandled);
		}
		break;

	case WM_MOUSEWHEEL:
		{
			SendMouseWheelEvent(uMsg, wParam, lParam, bHandled);
		}
		break;

	case WM_MOUSELEAVE:
		{
			SendMouseLeaveEvent(uMsg, wParam, lParam, bHandled);
		}
		break;

	default:
		bHandled = FALSE;
	};
	return bHandled;
}

LRESULT DUIWeb::SendMouseMoveEvent(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	do 
	{
		if (!GetBrowserHost())
		{
			break;
		}
		POINT pt = { GET_X_LPARAM(lParam), GET_Y_LPARAM(lParam) };
		CRect rcClient;
		DV_GetClientRect(rcClient);
		if (!rcClient.PtInRect(pt) && !IsCaptured())
		{
			break;
		}

		CefMouseEvent mouse_event;
		mouse_event.x = pt.x - rcClient.left;
		mouse_event.y = pt.y - rcClient.top;
		mouse_event.modifiers = GetCefMouseModifiers(wParam);
		GetBrowserHost()->SendMouseMoveEvent(mouse_event, false);
		bHandled = FALSE;
	} while (false);
	return 0;
}

LRESULT DUIWeb::SendButtonDownEvent(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	do 
	{
		if (!GetBrowserHost())
		{
			break;
		}
		POINT pt = { GET_X_LPARAM(lParam), GET_Y_LPARAM(lParam) };
		CRect rcClient;
		DV_GetClientRect(rcClient);
		if (!rcClient.PtInRect(pt))
		{
			break;
		}
		DV_SetFocusWnd();
		DM_SetCapture();

		CefMouseEvent mouse_event;
		mouse_event.x = pt.x - rcClient.left;
		mouse_event.y = pt.y - rcClient.top;
		mouse_event.modifiers = GetCefMouseModifiers(wParam);
		CefBrowserHost::MouseButtonType btnType = (uMsg == WM_LBUTTONDOWN ? MBT_LEFT : (uMsg == WM_RBUTTONDOWN ? MBT_RIGHT : MBT_MIDDLE));
		GetBrowserHost()->SendMouseClickEvent(mouse_event, btnType, false, 1);

		bHandled = FALSE;
	} while (false);
	return 0;
}

LRESULT DUIWeb::SendButtonUpEvent(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	do 
	{
		if (!GetBrowserHost())
		{
			break;
		}
		POINT pt = { GET_X_LPARAM(lParam), GET_Y_LPARAM(lParam) };
		CRect rcClient;
		DV_GetClientRect(rcClient);
		if (!rcClient.PtInRect(pt) && !IsCaptured())
		{
			break;
		}
		if (IsCaptured())
		{
			DM_ReleaseCapture();
		}
		
		CefMouseEvent mouse_event;
		if (uMsg == WM_RBUTTONUP)
		{
			mouse_event.x = pt.x/* - rcClient.left*/;	// 此处不进行坐标转换，否则右键菜单位置不正确
			mouse_event.y = pt.y/* - rcClient.top*/;
		}
		else
		{
			mouse_event.x = pt.x - rcClient.left;
			mouse_event.y = pt.y - rcClient.top;
		}
		mouse_event.modifiers = GetCefMouseModifiers(wParam);
		CefBrowserHost::MouseButtonType btnType = (uMsg == WM_LBUTTONUP ? MBT_LEFT : (uMsg == WM_RBUTTONUP ? MBT_RIGHT : MBT_MIDDLE));
		GetBrowserHost()->SendMouseClickEvent(mouse_event, btnType, true, 1);

		bHandled = FALSE;
	} while (false);
	return 0;
}

LRESULT DUIWeb::SendButtonDoubleDownEvent(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	do 
	{
		if (!GetBrowserHost())
		{
			break;
		}
		POINT pt = { GET_X_LPARAM(lParam), GET_Y_LPARAM(lParam) };
		CRect rcClient;
		DV_GetClientRect(rcClient);
		if (!rcClient.PtInRect(pt))
		{
			break;
		}
		DV_SetFocusWnd();
		DM_SetCapture();

		CefMouseEvent mouse_event;
		mouse_event.x = pt.x - rcClient.left;
		mouse_event.y = pt.y - rcClient.top;
		mouse_event.modifiers = GetCefMouseModifiers(wParam);
		CefBrowserHost::MouseButtonType btnType = (uMsg == WM_LBUTTONDBLCLK ? MBT_LEFT : (uMsg == WM_RBUTTONDBLCLK ? MBT_RIGHT : MBT_MIDDLE));
		GetBrowserHost()->SendMouseClickEvent(mouse_event, btnType, false, 2);

		bHandled = FALSE;
	} while (false);
	return 0;
}

LRESULT DUIWeb::SendKeyEvent(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	do 
	{
		if (!GetBrowserHost() || !DM_IsFocusWnd())
		{
			break;
		}

		CefKeyEvent event;
		event.windows_key_code = wParam;
		event.native_key_code = lParam;
		event.is_system_key = uMsg == WM_SYSCHAR || 
							  uMsg == WM_SYSKEYDOWN ||
							  uMsg == WM_SYSKEYUP;

		if (uMsg == WM_KEYDOWN || uMsg == WM_SYSKEYDOWN)
			event.type = KEYEVENT_RAWKEYDOWN;
		else if (uMsg == WM_KEYUP || uMsg == WM_SYSKEYUP)
			event.type = KEYEVENT_KEYUP;
		else
			event.type = KEYEVENT_CHAR;
		event.modifiers = GetCefKeyboardModifiers(wParam, lParam);

		GetBrowserHost()->SendKeyEvent(event);
		bHandled = FALSE;
	} while (false);
	return 0;
}

LRESULT DUIWeb::SendCaptureLostEvent(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	do 
	{
		if (!GetBrowserHost())
		{
			break;
		}
		GetBrowserHost()->SendCaptureLostEvent();
		bHandled = FALSE;
	} while (false);
	return 0;
}

LRESULT DUIWeb::SendMouseWheelEvent(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	do 
	{
		if (!GetBrowserHost())
		{
			break;
		}
		POINT pt = {GET_X_LPARAM(lParam), GET_Y_LPARAM(lParam)};
		HWND scrolled_wnd = ::WindowFromPoint(pt);
		if (scrolled_wnd != GetContainer()->OnGetHWnd())
		{
			break;
		}

		::ScreenToClient(GetContainer()->OnGetHWnd(), &pt);
		CRect rcClient;
		DV_GetClientRect(rcClient);
		if (!rcClient.PtInRect(pt))
		{
			break;
		}

		int delta = GET_WHEEL_DELTA_WPARAM(wParam);

		CefMouseEvent mouse_event;
		mouse_event.x = pt.x - rcClient.left;
		mouse_event.y = pt.y - rcClient.top;
		mouse_event.modifiers = GetCefMouseModifiers(wParam);
		GetBrowserHost()->SendMouseWheelEvent(mouse_event,
											  IsKeyDown(VK_SHIFT) ? delta : 0,
										     !IsKeyDown(VK_SHIFT) ? delta : 0);

		bHandled = FALSE;
	} while (false);
	return 0;
}

LRESULT DUIWeb::SendMouseLeaveEvent(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	do 
	{
		if (!GetBrowserHost())
		{
			break;
		}

		POINT pt = { GET_X_LPARAM(lParam), GET_Y_LPARAM(lParam) };
		CRect rcClient;
		DV_GetClientRect(rcClient);// 这里就不用判断光标了，光标离开区域了
		CefMouseEvent mouse_event;
		mouse_event.x = pt.x - rcClient.left;
		mouse_event.y = pt.y - rcClient.top;
		mouse_event.modifiers = GetCefMouseModifiers(wParam);
		GetBrowserHost()->SendMouseMoveEvent(mouse_event, true);

		bHandled = FALSE;
	} while (false);
	return 0;
}

#pragma endregion


//---------------------------------------------------
// Function Des: 消息分发函数
//---------------------------------------------------
#pragma region MsgDispatch
int DUIWeb::OnCreate(LPVOID)
{
	if (NULL == m_pWebHandler)
	{
		m_pDropTarget = DropTargetWin::Create(this, GetContainer()->OnGetHWnd());
		GetContainer()->OnRegisterDragDrop(m_hDUIWnd, m_pDropTarget);

		g_pDMApp->AddMessageFilter(this);
		m_pWebHandler = new CefOsrClientHandler;
		m_pWebHandler->SetHostWindow(GetContainer()->OnGetHWnd());
		m_pWebHandler->SetDelegate(this);
		m_pWebHandler->SetOsrDelegate(this);
		
		// 无窗模式
		CefWindowInfo window_info;
		window_info.SetAsWindowless(GetContainer()->OnGetHWnd(), false);
		CefBrowserSettings browser_settings;
		CefBrowserHost::CreateBrowser(window_info, m_pWebHandler, L"", browser_settings, NULL);
	}
	return 0;
}

void DUIWeb::OnDestroy()
{
	Quit();
	SetMsgHandled(FALSE);
}

void DUIWeb::OnSize(UINT nType, CSize size)
{
	DUIWindow::OnSize(nType,size);
	if (m_pWebHandler)
	{
		m_pWebHandler->SetViewRect(m_rcWindow);
	}
}

void DUIWeb::DM_OnPaint(IDMCanvas* pCanvas)
{
	DUIWindow::DM_OnPaint(pCanvas);
	if (m_pOsrCanvas && GetBrowser())
	{
		CRect rcClient;
		DV_GetClientRect(rcClient);
		CSize szOrg;
		m_pOsrCanvas->GetSize(szOrg);
		szOrg.cx = szOrg.cx>rcClient.Width()?rcClient.Width():szOrg.cx;
		szOrg.cy = szOrg.cy>rcClient.Height()?rcClient.Height():szOrg.cy;
		CRect rcSrc(0,0,szOrg.cx,szOrg.cy);
		if (!rcSrc.IsRectEmpty())
		{
			CRect rcDest = rcSrc;
			rcDest.OffsetRect(rcClient.TopLeft());

			byte alpha = 0xff;
			m_pDUIXmlInfo->m_pStyle->GetAlpha(alpha);
			pCanvas->AlphaBlend(m_pOsrCanvas,rcSrc,rcDest,alpha);
			
			// 绘制弹出框
			if (!m_rcOsrPop.IsEmpty() && m_pOsrPopCanvas)
			{
				CPoint ptDestOffset(m_rcOsrPop.x,m_rcOsrPop.y);
				CPoint ptSrcOffset;
				if (m_rcOsrPop.x < 0)
				{
					ptDestOffset.x = 0;
					ptSrcOffset.x = -m_rcOsrPop.x;
				}
				if (m_rcOsrPop.y < 0)
				{
					ptDestOffset.y = 0;
					ptSrcOffset.y = -m_rcOsrPop.y;
				}
				ptDestOffset.Offset(rcClient.TopLeft());
				rcDest.SetRect(0,0,m_rcOsrPop.width,m_rcOsrPop.height);rcSrc = rcDest;
				rcDest.OffsetRect(ptDestOffset);rcSrc.OffsetRect(ptSrcOffset);
				pCanvas->AlphaBlend(m_pOsrPopCanvas,rcSrc,rcDest,alpha);
			}
		}
	}
}

void DUIWeb::OnShowWindow(BOOL bShow, UINT nStatus)
{
	DUIWindow::OnShowWindow(bShow, nStatus);
	if (GetBrowserHost())
	{
		GetBrowserHost()->WasHidden(!bShow);
	}
}

void DUIWeb::DM_OnSetFocus()
{
	if (GetBrowserHost())
	{
		GetBrowserHost()->SendFocusEvent(true);
	}
}

void DUIWeb::DM_OnKillFocus()
{
	if (GetBrowserHost())
	{
		GetBrowserHost()->SendFocusEvent(false);
	}
}

DMCode DUIWeb::OnAttributeUrl(LPCWSTR pszValue, bool bLoadXml)
{
	m_strUrl = pszValue;
	if (!bLoadXml)
	{
		OpenUrl(m_strUrl);
	}
	return DM_ECODE_OK;
}

DMCode DUIWeb::OnAttributeRefreshKey(LPCWSTR pszValue, bool bLoadXml)
{
	CStringW strValue = pszValue;
	m_Refreshkey = DUIAccel::TranslateAccelKey(strValue);
	return DM_ECODE_OK;
}

void DUIWeb::OnFinalRelease()
{
	DUIWeb::Release();// 调用IMPLEMENT_REFCOUNTING中定义的Release
}
#pragma endregion