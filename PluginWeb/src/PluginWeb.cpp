#include "PluginWebAfx.h"
#include "PluginWeb.h"

const wchar_t* PluginWeb::GetName() const
{
	return L"Plugin_Cef_v1.0";
}

//--------------------------------------------------------------------
#ifdef PluginWeb_EXPORTS
#define WEB_EXPORT __declspec(dllexport)
#else
#define WEB_EXPORT __declspec(dllimport)
#endif // PluginWeb_EXPORTS

static PluginWeb* plugin;
extern "C" void WEB_EXPORT dllStartPlugin(void) throw()
{
	plugin = new PluginWeb();
	if (g_pDMApp)
	{
		g_pDMApp->InstallPlugin(plugin);
	}
}

extern "C" void WEB_EXPORT dllStopPlugin(void)
{
	if (g_pDMApp)
	{
		g_pDMApp->UninstallPlugin(plugin);
	}
	DM_DELETE(plugin);
}