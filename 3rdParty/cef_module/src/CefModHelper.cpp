#include "CefModuleAfx.h"
#include "CefModHelper.h"

int GetCefMouseModifiers(WPARAM wparam)
{
	int modifiers = 0;
	if (wparam & MK_CONTROL)
		modifiers |= EVENTFLAG_CONTROL_DOWN;
	if (wparam & MK_SHIFT)
		modifiers |= EVENTFLAG_SHIFT_DOWN;
	if (IsKeyDown(VK_MENU))
		modifiers |= EVENTFLAG_ALT_DOWN;
	if (wparam & MK_LBUTTON)
		modifiers |= EVENTFLAG_LEFT_MOUSE_BUTTON;
	if (wparam & MK_MBUTTON)
		modifiers |= EVENTFLAG_MIDDLE_MOUSE_BUTTON;
	if (wparam & MK_RBUTTON)
		modifiers |= EVENTFLAG_RIGHT_MOUSE_BUTTON;

	// Low bit set from GetKeyState indicates "toggled".
	if (::GetKeyState(VK_NUMLOCK) & 1)
		modifiers |= EVENTFLAG_NUM_LOCK_ON;
	if (::GetKeyState(VK_CAPITAL) & 1)
		modifiers |= EVENTFLAG_CAPS_LOCK_ON;
	return modifiers;
}

int GetCefKeyboardModifiers(WPARAM wparam, LPARAM lparam)
{
	int modifiers = 0;
	if (IsKeyDown(VK_SHIFT))
		modifiers |= EVENTFLAG_SHIFT_DOWN;
	if (IsKeyDown(VK_CONTROL))
		modifiers |= EVENTFLAG_CONTROL_DOWN;
	if (IsKeyDown(VK_MENU))
		modifiers |= EVENTFLAG_ALT_DOWN;

	// Low bit set from GetKeyState indicates "toggled".
	if (::GetKeyState(VK_NUMLOCK) & 1)
		modifiers |= EVENTFLAG_NUM_LOCK_ON;
	if (::GetKeyState(VK_CAPITAL) & 1)
		modifiers |= EVENTFLAG_CAPS_LOCK_ON;

	switch (wparam) 
	{
	case VK_RETURN:
		if ((lparam >> 16) & KF_EXTENDED)
			modifiers |= EVENTFLAG_IS_KEY_PAD;
		break;
	case VK_INSERT:
	case VK_DELETE:
	case VK_HOME:
	case VK_END:
	case VK_PRIOR:
	case VK_NEXT:
	case VK_UP:
	case VK_DOWN:
	case VK_LEFT:
	case VK_RIGHT:
		if (!((lparam >> 16) & KF_EXTENDED))
			modifiers |= EVENTFLAG_IS_KEY_PAD;
		break;
	case VK_NUMLOCK:
	case VK_NUMPAD0:
	case VK_NUMPAD1:
	case VK_NUMPAD2:
	case VK_NUMPAD3:
	case VK_NUMPAD4:
	case VK_NUMPAD5:
	case VK_NUMPAD6:
	case VK_NUMPAD7:
	case VK_NUMPAD8:
	case VK_NUMPAD9:
	case VK_DIVIDE:
	case VK_MULTIPLY:
	case VK_SUBTRACT:
	case VK_ADD:
	case VK_DECIMAL:
	case VK_CLEAR:
		modifiers |= EVENTFLAG_IS_KEY_PAD;
		break;
	case VK_SHIFT:
		if (IsKeyDown(VK_LSHIFT))
			modifiers |= EVENTFLAG_IS_LEFT;
		else if (IsKeyDown(VK_RSHIFT))
			modifiers |= EVENTFLAG_IS_RIGHT;
		break;
	case VK_CONTROL:
		if (IsKeyDown(VK_LCONTROL))
			modifiers |= EVENTFLAG_IS_LEFT;
		else if (IsKeyDown(VK_RCONTROL))
			modifiers |= EVENTFLAG_IS_RIGHT;
		break;
	case VK_MENU:
		if (IsKeyDown(VK_LMENU))
			modifiers |= EVENTFLAG_IS_LEFT;
		else if (IsKeyDown(VK_RMENU))
			modifiers |= EVENTFLAG_IS_RIGHT;
		break;
	case VK_LWIN:
		modifiers |= EVENTFLAG_IS_LEFT;
		break;
	case VK_RWIN:
		modifiers |= EVENTFLAG_IS_RIGHT;
		break;
	}
	return modifiers;
}

bool IsKeyDown(WPARAM wparam)
{
	return (GetKeyState(wparam) & 0x8000) != 0;
}


//-----------------------------------------------------------------------------
WaitableEvent::WaitableEvent(bool bManReset, bool bSignaled)
: m_Handle(CreateEvent(NULL, bManReset, bSignaled, NULL)) 
{
	CHECK(m_Handle);
}

WaitableEvent::~WaitableEvent()
{
	CloseHandle(m_Handle);
}


void WaitableEvent::Signal()
{
	SetEvent(m_Handle);
}

bool WaitableEvent::Wait(DWORD dwMaxTimeOut)
{
	if (NULL == m_Handle)
	{
		return false;
	}
	DWORD dwError = ERROR_SUCCESS;
	DWORD dwTickStart = ::GetTickCount();
	DWORD dwTimeOut = dwMaxTimeOut;
	do 
	{
		if (false == DealWithMsg())
		{//1.WM_QUIT退出消息循环
			::PostQuitMessage(0);//WM_QUIT消息被这里截取了,防止DM库退不出来，这里重发一次消息
			break;
		}
		if (INFINITE != dwTimeOut)
		{
			DWORD dwTick = ::GetTickCount() - dwTickStart;
			if (dwTick > dwMaxTimeOut)
			{
				dwTimeOut = 0;
			}
			else
			{
				dwTimeOut -= dwTick;
			}
		}
		dwError = ::MsgWaitForMultipleObjects(1, &m_Handle, FALSE, dwTimeOut, QS_ALLINPUT);// QS_ALLEVENTS可接受Post消息，An input, WM_TIMER, WM_PAINT, WM_HOTKEY, or posted message is in the queue.
	} while (WAIT_OBJECT_0+1==dwError);
	return WAIT_OBJECT_0 == dwError;
}

bool WaitableEvent::DealWithMsg()
{
	MSG msg;
	while (::PeekMessage(&msg, NULL, 0, 0, PM_REMOVE)) 
	{ 
		// If it is a quit message, exit.
		if (msg.message == WM_QUIT)  
			return false; 
		::PeekMessage(&msg, NULL, 0, 0, PM_REMOVE);
		::TranslateMessage(&msg);
		::DispatchMessage(&msg);
	} // End of PeekMessage while loop.
	return true;
}


//-----------------------------------------------------------------------------
const DWORD BUFSIZE = 1024*10;  
const DWORD PIPE_TIMEOUT = 3000;  
NamedPipe::NamedPipe()
{
	m_hPipe = INVALID_HANDLE_VALUE;  
	m_nMode = PIPE_UNDF;  
	m_hEvent  = CreateEvent(NULL,TRUE,TRUE,NULL);   // OVERLPPED‘s event
}

NamedPipe::NamedPipe(LPCWSTR lpszPipeName, int nMode)
{
	m_hPipe = INVALID_HANDLE_VALUE;  
	m_nMode = PIPE_UNDF;  
	m_hEvent  = CreateEvent(NULL,TRUE,TRUE,NULL);   // OVERLPPED‘s event
	Create(lpszPipeName,nMode);
}

NamedPipe::~NamedPipe()
{
	if (IsValid())
	{
		::DisconnectNamedPipe(m_hPipe);  
		::CloseHandle(m_hPipe); 
		::CloseHandle(m_hEvent);
		m_hPipe = INVALID_HANDLE_VALUE;  
		m_nMode = PIPE_UNDF;  
	}
}

bool NamedPipe::IsValid() const
{
	return INVALID_HANDLE_VALUE != m_hPipe;
}

bool NamedPipe::Create(LPCWSTR lpszPipeName, int nMode)
{
	bool bRet = false;
	do 
	{
		if (INVALID_HANDLE_VALUE != m_hPipe)
		{
			bRet = true;
			break;
		}
		if (!IsValidString(lpszPipeName)||nMode>=PIPE_UNDF)
		{
			break;
		}
		wchar_t szPipeName[MAX_PATH] = {0};  
		wsprintf(szPipeName, L"\\\\.\\pipe\\%s", lpszPipeName);  
		if (PIPE_SERV == nMode)
		{
			m_hPipe = ::CreateNamedPipeW(szPipeName, PIPE_ACCESS_DUPLEX|FILE_FLAG_OVERLAPPED, PIPE_TYPE_MESSAGE|PIPE_READMODE_MESSAGE|PIPE_WAIT, PIPE_UNLIMITED_INSTANCES, BUFSIZE, BUFSIZE, PIPE_TIMEOUT, NULL);  
		}
		else if(PIPE_CLIN == nMode)
		{
			m_hPipe = ::CreateFileW(szPipeName, GENERIC_READ|GENERIC_WRITE, FILE_SHARE_READ, NULL, OPEN_EXISTING, 0, NULL);
			DWORD dwMode = PIPE_READMODE_MESSAGE;
		}
		m_strPipeName = szPipeName;
		bRet = (INVALID_HANDLE_VALUE != m_hPipe);
	} while (false);
	return bRet;
}

bool NamedPipe::Read(std::wstring& strRet,DWORD& dwRead,DWORD dwTimeOut/* = INFINITE*/)
{
	bool bRet = false;
	do 
	{
		if (!IsValid())
		{
			break;
		}
		OVERLAPPED op;
		memset(&op,0,sizeof(op));
		op.hEvent = m_hEvent;
		if (::ConnectNamedPipe(m_hPipe, &op))
		{
			break;
		}

		DWORD dwRet = 0;
		byte* lpBuffer = NULL;
		DWORD nNumberOfBytesToRead = 0;
		while (true)
		{
			DWORD dwResult = WaitForSingleObject(m_hEvent, dwTimeOut);
			if (WAIT_OBJECT_0 == dwResult)
			{
				if (!::GetOverlappedResult(m_hPipe,&op,&dwRet,FALSE))
				{
					break;
				}
				::ReadFile(m_hPipe, (void*)&nNumberOfBytesToRead, 4, &dwRead,NULL);
				if (nNumberOfBytesToRead)
				{
					lpBuffer = new byte[nNumberOfBytesToRead+2];
					memset(lpBuffer,0,nNumberOfBytesToRead+2);
					::ReadFile(m_hPipe, (void*)lpBuffer, nNumberOfBytesToRead, &dwRead,NULL);
				}
				if (lpBuffer)
				{
					strRet = (wchar_t*)lpBuffer;
					delete[]lpBuffer;
					lpBuffer = NULL;
				}
				bRet = true;
				break;
			}
			else
			{
				break;
			}
		}	
	} while (false);
	return bRet;
}

bool NamedPipe::Write(void* pBuf, int nSize, DWORD& dwWrite)
{
	bool bRet = false;
	do 
	{
		if (!IsValid())
		{
			break;
		}
		bRet = (0!=WriteFile(m_hPipe, pBuf, nSize, (DWORD*)&dwWrite, NULL));
	} while (false);
	return bRet;
}
