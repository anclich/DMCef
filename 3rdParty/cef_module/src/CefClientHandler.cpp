#include "CefModuleAfx.h"
#include "CefClientHandler.h"
CefClientHandler::CefClientHandler():m_hWnd(NULL),m_pDelegate(NULL)
{
}

void CefClientHandler::SetDelegate(Delegate *pDelegate)
{
	m_pDelegate = pDelegate;
}

void CefClientHandler::SetHostWindow(HWND hwnd)
{
	m_hWnd = hwnd;
}

CefRefPtr<CefBrowser> CefClientHandler::GetBrowser()
{
	return m_pBrowser;
}

CefRefPtr<CefBrowserHost> CefClientHandler::GetBrowserHost()
{
	if (m_pBrowser)
	{
		return m_pBrowser->GetHost();
	}
	return NULL;
}

//---------------------------------------------------
// Function Des: CefLifeSpanHandler methods
//---------------------------------------------------
#pragma region CefClient
bool CefClientHandler::OnProcessMessageReceived(CefRefPtr<CefBrowser> browser, CefProcessId source_process, CefRefPtr<CefProcessMessage> message)
{
	if (m_pBrowser && m_pDelegate)
	{
		m_pDelegate->OnProcessMessageReceived(browser,source_process,message);
	}
	return false;
}
#pragma endregion


//---------------------------------------------------
// Function Des: CefLifeSpanHandler methods
//---------------------------------------------------
#pragma region CefLifeSpanHandler
bool CefClientHandler::OnBeforePopup(CefRefPtr<CefBrowser> browser, CefRefPtr<CefFrame> frame, const CefString& target_url, const CefString& target_frame_name, CefLifeSpanHandler::WindowOpenDisposition target_disposition, bool user_gesture, const CefPopupFeatures& popupFeatures, CefWindowInfo& windowInfo, CefRefPtr<CefClient>& client, CefBrowserSettings& settings, bool* no_javascript_access)
{
	if (m_pBrowser && !target_url.empty())
	{
		if (m_pDelegate)
		{
			return m_pDelegate->OnBeforePopup(browser, frame, target_url, target_frame_name, target_disposition, user_gesture, popupFeatures, windowInfo, client, settings, no_javascript_access);
		}
	}
	return true;// Ĭ�Ͻ�ֹ����Popup����
}

void CefClientHandler::OnAfterCreated(CefRefPtr<CefBrowser> browser)
{
	REQUIRE_UI_THREAD();
	m_pBrowser = browser;
	if (m_pBrowser)
	{
		if (g_pMainUIRunner)
		{
			CefManager *pMgr = CefManager::GetInstance();
			g_pMainUIRunner->PostTask(NewCefRunnableMethod(pMgr,&CefManager::AddBrowserCount));
		}
		if (m_pDelegate)
		{
			m_pDelegate->OnAfterCreated(browser);
		}
	}
}

void CefClientHandler::OnBeforeClose(CefRefPtr<CefBrowser> browser)
{
	if (browser.get() && m_pBrowser && m_pBrowser->GetIdentifier() == browser->GetIdentifier())
	{
		m_pBrowser = NULL;
		if (g_pMainUIRunner)
		{
			CefManager *pMgr = CefManager::GetInstance();
			g_pMainUIRunner->PostTask(NewCefRunnableMethod(pMgr,&CefManager::SubBrowserCount));
		}
	}
}
#pragma endregion


//---------------------------------------------------
// Function Des: CefContextMenuHandler methods
//---------------------------------------------------
#pragma region CefContextMenuHandler
void CefClientHandler::OnBeforeContextMenu(CefRefPtr<CefBrowser> browser, CefRefPtr<CefFrame> frame, CefRefPtr<CefContextMenuParams> params, CefRefPtr<CefMenuModel> model)
{
	REQUIRE_UI_THREAD();
	if (m_pBrowser && m_pDelegate)
	{
		m_pDelegate->OnBeforeContextMenu(browser,frame,params,model);
	}
	else
	{
		if ((params->GetTypeFlags() & (CM_TYPEFLAG_PAGE | CM_TYPEFLAG_FRAME)))
		{
			if (model->GetCount() > 0)
			{
				model->Clear();// ��ֹ�Ҽ��˵�
			}
		}
	}
}
#pragma endregion


//---------------------------------------------------
// Function Des: CefDisplayHandler methods
//---------------------------------------------------
#pragma region CefDisplayHandler
void CefClientHandler::OnAddressChange(CefRefPtr<CefBrowser> browser, CefRefPtr<CefFrame> frame,const CefString& url)
{
	if (m_pBrowser && m_pDelegate)
	{
		return m_pDelegate->OnAddressChange(browser,frame,url);
	}
}

void CefClientHandler::OnTitleChange(CefRefPtr<CefBrowser> browser,const CefString& title)
{
	if (m_pBrowser && m_pDelegate)
	{
		return m_pDelegate->OnTitleChange(browser,title);
	}
}


void CefClientHandler::OnFullscreenModeChange(CefRefPtr<CefBrowser> browser,bool fullscreen)
{
	if (m_pBrowser && m_pDelegate)
	{
		m_pDelegate->OnFullscreenModeChange(browser,fullscreen);
	}
}

#pragma endregion


//---------------------------------------------------
// Function Des: CefKeyboardHandler methods
//---------------------------------------------------
#pragma region CefKeyboardHandler
bool CefClientHandler::OnPreKeyEvent(CefRefPtr<CefBrowser> browser,const CefKeyEvent& event,CefEventHandle os_event,bool* is_keyboard_shortcut)
{
	if (m_pBrowser && m_pDelegate)
	{
		return m_pDelegate->OnPreKeyEvent(browser,event,os_event,is_keyboard_shortcut);
	}
	return false;
}
#pragma endregion


//---------------------------------------------------
// Function Des: CefLoadHandler methods
//---------------------------------------------------
#pragma region CefLoadHandler
void CefClientHandler::OnLoadEnd(CefRefPtr<CefBrowser> browser,CefRefPtr<CefFrame> frame,int httpStatusCode)
{
	if (m_pBrowser && m_pDelegate)
	{
		m_pDelegate->OnLoadEnd(browser,frame,httpStatusCode);
	}
}
#pragma endregion


//=======================================================================================
CefOsrClientHandler::CefOsrClientHandler()
{
	ZeroMemory(&m_rcView, sizeof(RECT));
}

void CefOsrClientHandler::SetOsrDelegate(OsrDelegate *pOsrDelegate)
{
	m_pOsrDelegate = pOsrDelegate;
}

void CefOsrClientHandler::SetViewRect(RECT rect)
{
	if (!CefCurrentlyOn(TID_UI)) 
	{//��ת��Cef�߳�ִ��
		CefPostTask(TID_UI,NewCefRunnableMethod(this,&CefOsrClientHandler::SetViewRect, rect));
	}
	else
	{
		m_rcView = rect;
		if (GetBrowserHost())
		{
			GetBrowserHost()->WasResized();
		}
	}
}

//---------------------------------------------------
// Function Des: CefRenderHandler methods
//---------------------------------------------------
#pragma region CefRenderHandler
bool CefOsrClientHandler::GetRootScreenRect(CefRefPtr<CefBrowser> browser, CefRect& rect)
{
	RECT rcRootWnd = {0};
	HWND hRootWnd = ::GetAncestor(m_hWnd, GA_ROOT);
	if (::GetWindowRect(hRootWnd, &rcRootWnd))
	{
		rect = CefRect(rcRootWnd.left, rcRootWnd.top, rcRootWnd.right - rcRootWnd.left, rcRootWnd.bottom - rcRootWnd.top);
		return true;
	}
	return false;
}

bool CefOsrClientHandler::GetViewRect(CefRefPtr<CefBrowser> browser, CefRect& rect)
{
	if (m_pBrowser && m_pOsrDelegate)
	{
		if (false == m_pOsrDelegate->GetViewRect(browser,rect))
		{
			rect.Set(0,0,m_rcView.right - m_rcView.left,m_rcView.bottom - m_rcView.top);
		}
		return true;
	}
	else
	{
		RECT rcClient;
		if (!::GetClientRect(m_hWnd, &rcClient))
		{
			return false;
		}
		rect.Set(0,0,rcClient.right,rcClient.bottom);
		return true;
	}
}

bool CefOsrClientHandler::GetScreenPoint(CefRefPtr<CefBrowser> browser, int viewX, int viewY, int& screenX, int& screenY)
{
	if (!::IsWindow(m_hWnd))
	{
		return false;
	}

	// Convert the point from view coordinates to actual screen coordinates.
	POINT screen_pt = { viewX, viewY };
	::ClientToScreen(m_hWnd, &screen_pt);
	screenX = screen_pt.x;
	screenY = screen_pt.y;
	return true;
}

void CefOsrClientHandler::OnPopupShow(CefRefPtr<CefBrowser> browser, bool show)
{
	if (m_pBrowser && m_pOsrDelegate)
	{
		m_pOsrDelegate->OnPopupShow(browser,show);
	}
}

void CefOsrClientHandler::OnPopupSize(CefRefPtr<CefBrowser> browser, const CefRect& rect)
{
	if (m_pBrowser && m_pOsrDelegate)
	{
		m_pOsrDelegate->OnPopupSize(browser,rect);
	}
}

void CefOsrClientHandler::OnPaint(CefRefPtr<CefBrowser> browser, PaintElementType type, const RectList& dirtyRects, const void* buffer, int width, int height)
{
	if (m_pBrowser && m_pOsrDelegate)
	{
		m_pOsrDelegate->OnPaint(browser,type,dirtyRects,buffer,width,height);
	}
}

void CefOsrClientHandler::OnCursorChange(CefRefPtr<CefBrowser> browser, CefCursorHandle cursor, CursorType type, const CefCursorInfo& custom_cursor_info)
{
	if (m_pBrowser && m_pOsrDelegate)
	{
		m_pOsrDelegate->OnCursorChange(browser,cursor,type,custom_cursor_info);
	}
}
#pragma endregion