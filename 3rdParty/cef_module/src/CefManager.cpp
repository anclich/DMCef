#include "CefModuleAfx.h"
#include "CefManager.h"
#include "CefClientApp.h"
#include "CefModHelper.h"

CefManager* CefManager::GetInstance()
{
	static CefManager s_Instance;
	return &s_Instance;		
}

CefManager::CefManager()
{
	m_iBrowserNum = 0;
	m_bOsrRender = m_bNeedQuit = false;
	m_pMainUIRunner = NULL;
}

void FixContextMenuBug(HWND hwnd)
{// 父窗口是主UI线程, 子窗口是CEF UI线程
	::CreateWindowW(L"Static", L"", WS_CHILD, 0, 0, 0, 0, hwnd, NULL, NULL, NULL);
	::PostMessage(hwnd, WM_CLOSE, 0, 0);
}

void FixFlashBug()
{// 需要在exe同目录上建一个dmcef_cmd.exe（空记事本建立即可）
	WCHAR app_path[MAX_PATH] = {0};
	::GetModuleFileNameW(NULL, app_path, MAX_PATH);
	::PathRemoveFileSpecW(app_path);

	std::wstring cmd_path = app_path;
	cmd_path += L"\\dmcef_cmd.exe";
	::SetEnvironmentVariableW(L"ComSpec", cmd_path.c_str());
};

bool CefManager::Initialize(CefSettings &settings,CefRefPtr<CefListValue>& ExtraInfo,bool bOsrRender /*= true*/,IMainUIRunner* pMainUIRunner/*=NULL*/)
{
	bool bRet = false;
	do 
	{
		m_ExtraInfo = ExtraInfo;
		m_bOsrRender = bOsrRender;
		m_pMainUIRunner = pMainUIRunner;
		CefMainArgs MainArgs(GetModuleHandle(NULL));
		CefRefPtr<CefClientApp> App(new CefClientApp);
		int iExitCode = CefExecuteProcess(MainArgs, App.get(), NULL);
		if (iExitCode >= 0)// 如果是在子进程中调用，会堵塞直到子进程退出，并且iExitCode返回大于等于0, 如果在Browser进程中调用，则立即返回-1
		{
			break;
		}
		bRet = CefInitialize(MainArgs, settings, App.get(), NULL);
		FixFlashBug();
		if (m_bOsrRender)
		{
			HWND hwnd = ::CreateWindowW(L"Static", L"", WS_POPUP, 0, 0, 0, 0, NULL, NULL, NULL, NULL);
			CefPostTask(TID_UI, base::Bind(&FixContextMenuBug, hwnd));
		}
	} while (false);
	return bRet;
}

void CefManager::UnInitialize()
{
	m_pMainUIRunner = NULL;
	CefShutdown();
}

void CefManager::PostQuitMsg()
{
	// 等所有浏览器对象都销毁后再调用::PostQuitMessage
	if (0 == m_iBrowserNum)
	{
		::PostQuitMessage(1);
	}
	else
	{
		m_bNeedQuit = true;
	}
}

bool CefManager::SetCookie(std::wstring strUrl, std::wstring strCookieName, std::wstring strCookieData, std::wstring strDoMain)
{//必须在TID_IO线程中执行
	REQUIRE_IO_THREAD();
	bool bRet = false;
	do 
	{
		if (strUrl.empty() || strCookieName.empty())
		{
			break;
		}

		CefURLParts UrlParts;
		CefString strCefUrl = strUrl;
		if (!CefParseURL(strCefUrl, UrlParts)
			||NULL == UrlParts.host.str)// file协议或者文件的域名为NULL，不能使用cookie
		{
			break;
		}
		if (strDoMain.empty())
		{// 使用URL中的域名
			strDoMain = UrlParts.host.str;
		}
		CefCookie cookie;
		CefString(&cookie.name)   = strCookieName;
		CefString(&cookie.value)  = strCookieData;
		CefString(&cookie.domain) = strDoMain;
		CefString(&cookie.path)   = L"/"; //UrlParts.path.str;
		CefRefPtr<CefCookieManager> Mgr = CefCookieManager::GetGlobalManager(NULL);
		bRet = Mgr->SetCookie(strCefUrl,cookie,NULL);;
	} while (false);
	return bRet;
}

bool CefManager::IsOffsetRender() const
{
	return m_bOsrRender;
}

//--------------------------------------------------------------
IMainUIRunner* CefManager::GetMainUIRunner()
{
	return m_pMainUIRunner;
}

CefRefPtr<CefListValue> CefManager::GetExtraInfo()
{
	return m_ExtraInfo;
}

void CefManager::AddBrowserCount()
{
	::InterlockedIncrement(&m_iBrowserNum);
}

void CefManager::SubBrowserCount()
{
	assert(m_iBrowserNum >= 0);
	if (0 == ::InterlockedDecrement(&m_iBrowserNum) && m_bNeedQuit)
	{
		::PostQuitMessage(1);
	}
}

int CefManager::GetBrowserCount()
{
	return m_iBrowserNum;
}