#include "CefModuleAfx.h"
#include "CefJSHandler.h"

CefJSHandler::CefJSHandler(CefRefPtr<CefListValue>& ExtraInfo)
{
	m_ExtraInfo = ExtraInfo;
}

void CefJSHandler::OnContextCreated(CefRefPtr<CefBrowser> browser, CefRefPtr<CefFrame> frame, CefRefPtr<CefV8Context> context)
{
	if (m_ExtraInfo && m_ExtraInfo->GetSize()>0)
	{
		CefRefPtr<CefV8Value> V8Global = context->GetGlobal();
		CefRefPtr<CefV8Value> V8External = V8Global->GetValue(L"external"); // external对象,和IE保持一致
		if (V8External->IsUndefined())
		{
			V8External = CefV8Value::CreateObject(NULL);
			V8External->SetValue(L"name", CefV8Value::CreateString(L"external"), V8_PROPERTY_ATTRIBUTE_READONLY);
			V8Global->SetValue(L"external", V8External, V8_PROPERTY_ATTRIBUTE_NONE);
		}
		CefRefPtr<CefListValue> ExtraFunNamesInfo = m_ExtraInfo->GetList(EXTRAINFO_FUNNAMES);
		if (ExtraFunNamesInfo)
		{
			for (int i = 0; i < (int)ExtraFunNamesInfo->GetSize(); i++)
			{
				std::wstring strFunName = ExtraFunNamesInfo->GetString(i);
				CefRefPtr<CefV8Value> V8Func = CefV8Value::CreateFunction(CefString(strFunName),this);
				V8External->SetValue(CefString(strFunName), V8Func, V8_PROPERTY_ATTRIBUTE_NONE);
			}
		}
	}
}

bool CefJSHandler::Execute(const CefString& name, CefRefPtr<CefV8Value> object, const CefV8ValueList& arguments, CefRefPtr<CefV8Value>& retval, CefString& exception)
{
	CefRefPtr<CefProcessMessage> Msg = CefProcessMessage::Create(kJsCallbackMessage);
	CefRefPtr<CefListValue> Args = Msg->GetArgumentList();
	CefRefPtr<CefBrowser> browser = CefV8Context::GetCurrentContext()->GetBrowser();// 从V8上下文获取对应的浏览器窗口
	for (int i = 0; i <(int)arguments.size(); i++)
	{
		const CefRefPtr<CefV8Value>& val = arguments[i];
		if (val->IsNull())			Args->SetNull(i);
		else if (val->IsBool())		Args->SetBool(i, val->GetBoolValue());
		else if (val->IsInt())		Args->SetInt(i, val->GetIntValue());
		else if (val->IsUInt())		Args->SetInt(i, (int)val->GetUIntValue());
		else if (val->IsDouble())	Args->SetDouble(i, val->GetDoubleValue());
		else if (val->IsString())	Args->SetString(i, val->GetStringValue());
		else if (val->IsArray())
		{
			CefRefPtr<CefListValue> cefItem = CefListValue::Create();
			for (int j = 0; j <(int)val->GetArrayLength(); j++)
			{
				CefRefPtr<CefV8Value> item = val->GetValue(j);
				if (item->IsNull())			cefItem->SetNull(j);
				else if (item->IsBool())	cefItem->SetBool(j, item->GetBoolValue());
				else if (item->IsInt())		cefItem->SetInt(j, item->GetIntValue());
				else if (item->IsUInt())	cefItem->SetInt(j, (int)item->GetUIntValue());
				else if (item->IsDouble())	cefItem->SetDouble(j, item->GetDoubleValue());
				else if (item->IsString())	cefItem->SetString(j, item->GetStringValue());
			}
			Args->SetList(i, cefItem);
		}
		else if (val->IsObject())
		{
			CefRefPtr<CefDictionaryValue> cefItem = CefDictionaryValue::Create();
			std::vector<CefString> keys;
			val->GetKeys(keys);
			for (int k = 0; k < (int)keys.size(); k++)
			{
				CefRefPtr<CefV8Value> item = val->GetValue(keys[k]);
				if (item->IsNull())			cefItem->SetNull(keys[k]);
				else if (item->IsBool())	cefItem->SetBool(keys[k], item->GetBoolValue());
				else if (item->IsInt())		cefItem->SetInt(keys[k], item->GetIntValue());
				else if (item->IsUInt())	cefItem->SetInt(keys[k], (int)item->GetUIntValue());
				else if (item->IsDouble())	cefItem->SetDouble(keys[k], item->GetDoubleValue());
				else if (item->IsString())	cefItem->SetString(keys[k], item->GetStringValue());
			}
			Args->SetDictionary(i, cefItem);
		}
		else
		{// 不支持的函数类型
			return false;
		}
	}

	bool bSync = (0 == arguments.size()|| !arguments[arguments.size()-1]->IsFunction());// 检测最后一个参数是否回调函数，如果有回调则采用异步返回，否则使用同步返回
	wchar_t szPipeName[50] = {0};
	wsprintf(szPipeName, L"browser_pipe_%d", browser->GetIdentifier());
	// 最后插入dictionary
	CefRefPtr<CefDictionaryValue> desc = CefDictionaryValue::Create();
	desc->SetString(L"func_name",name);
	desc->SetString(L"pipe_name",szPipeName);
	desc->SetBool(L"bsync",bSync);
	Args->SetDictionary((int)arguments.size(), desc);

	NamedPipe Pipe;
	if (bSync)
	{
		Pipe.Create(szPipeName,NamedPipe::PIPE_SERV);
	}
	bool bRet = browser->SendProcessMessage(PID_BROWSER, Msg);
	if (bRet && bSync && Pipe.IsValid()) // 同步模式下等待调用结果
	{
		DWORD dwRead = 0;
		std::wstring strRet;
		if (Pipe.Read(strRet,dwRead,3000))
		{
			retval = CefV8Value::CreateString(strRet);
		}
	}
	return bRet;
}
