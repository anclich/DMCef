#include "CefModuleAfx.h"
#include "CefClientApp.h"

//---------------------------------------------------
// Function Des: CefApp methods
//---------------------------------------------------
#pragma region CefApp
void CefClientApp::OnBeforeCommandLineProcessing(const CefString& process_type, CefRefPtr<CefCommandLine> command_line)
{
	if (process_type.empty())
	{
		// 如果不设置子进程路径，cef在LoadUrl时，会查找子进程的路径，可能导致IDE在debug状态时卡死
		command_line->AppendSwitchWithValue("browser-subprocess-path", "cef_render.exe");

		// 支持PPAPI flash
		command_line->AppendSwitchWithValue("ppapi-flash-version", "25.0.0.171");
		command_line->AppendSwitchWithValue("ppapi-flash-path", "pepflashplayer32_25_0_0_171.dll");

		// 同一个域下的使用同一个渲染进程,禁用gpu加速
		command_line->AppendSwitch("process-per-site");
		command_line->AppendSwitch("disable-gpu");
		command_line->AppendSwitch("disable-gpu-compositing");

		// 开启离屏渲染
		if (g_pCefManager->IsOffsetRender())
		{
			command_line->AppendSwitch("disable-surfaces");
			command_line->AppendSwitch("enable-begin-frame-scheduling");
		}
	}
}
#pragma endregion


//---------------------------------------------------
// Function Des: CefBrowserProcessHandler methods
//---------------------------------------------------
#pragma region CefBrowserProcessHandler
void CefClientApp::OnRenderProcessThreadCreated(CefRefPtr<CefListValue> extra_info)
{
	CefRefPtr<CefListValue> ExtraInfo = g_pCefManager->GetExtraInfo();
	if (ExtraInfo)
	{
		for (int i=0; i<EXTRAINFO_MAX; i++)
		{
			CefRefPtr<CefListValue> Info = ExtraInfo->GetList(i);
			if (Info)
			{
				extra_info->SetList(i,Info);
			}
		}
	}
}
#pragma endregion
