#include "CefModuleAfx.h"
#include "CefRenderApp.h"

CefRenderApp::CefRenderApp()
{
}

//---------------------------------------------------
// Function Des: CefRenderProcessHandler methods
//---------------------------------------------------
#pragma region CefRenderProcessHandler
void CefRenderApp::OnRenderThreadCreated(CefRefPtr<CefListValue> extra_info)
{
	if (extra_info && extra_info->GetSize() > 0)
	{
		if (!m_ExtraInfo)
		{
			m_ExtraInfo = CefListValue::Create();
		}
		for (int i=0; i<EXTRAINFO_MAX; i++)
		{
			CefRefPtr<CefListValue> Info = extra_info->GetList(i);
			if (Info)
			{
				m_ExtraInfo->SetList(i,Info);
			}
		}
	}
}

bool CefRenderApp::OnBeforeNavigation(CefRefPtr<CefBrowser> browser,CefRefPtr<CefFrame> frame,CefRefPtr<CefRequest> request,NavigationType navigation_type,bool is_redirect)
{
	bool bRet = false;
	do 
	{
		//DUIWND hWnd, DMIN IDispatch *pDisp, DMIN wchar_t *pUrl,DMIN int Flags,DMIN wchar_t *pTargetFrameName,DMIN VARIANT *pPostData,DMIN wchar_t *pHeaders,DMINOUT VARIANT_BOOL *bCancel
		CefRefPtr<CefProcessMessage> Msg = CefProcessMessage::Create(kBeforeNavigateMessage);
		CefRefPtr<CefListValue> Args = Msg->GetArgumentList();

		// 此处根据需要可插入PostData,Header	
		Args->SetInt(0,(int)frame->GetIdentifier());							///< pDisp
		Args->SetString(1,request->GetURL());									///< Url
		Args->SetInt(2,navigation_type);										///< Flags
		std::string strPostData;
		int iPostDataLen = 0;
		CefRefPtr<CefPostData> PostData = request->GetPostData();
		if (PostData.get())
		{
			CefPostData::ElementVector elements;
			PostData->GetElements(elements);
			if (elements.size()>0)
			{
				size_t size = elements[0]->GetBytesCount();
				strPostData.resize(size+1);
				elements[0]->GetBytes(size,(void*)strPostData.c_str());
				iPostDataLen = (int)size;
			}
		}
		Args->SetString(3,strPostData);											///< pPostData
		Args->SetInt(4,iPostDataLen);											///< iPostDataLen

		CefRequest::HeaderMap headerMap;
		request->GetHeaderMap(headerMap);
		std::string strHeader;
		CefRequest::HeaderMap::iterator map_it = headerMap.begin();  
		while (map_it != headerMap.end())
		{
			typedef CefRequest::HeaderMap::size_type sz_type;
			sz_type cnt = headerMap.count(map_it->first);
			for (sz_type i=0;i!=cnt;++map_it,++i)
			{
				strHeader += map_it->first.ToString();;
				strHeader += ":";
				strHeader += map_it->second.ToString();
				strHeader += "\r\n";
			}
		}
		Args->SetString(5,strHeader);											///< pszHeaders

		wchar_t szPipeName[50] = {0};
		wsprintf(szPipeName, L"browser_Nav_pipe_%d", browser->GetIdentifier());
		
		// 插入dictionary
		CefRefPtr<CefDictionaryValue> desc = CefDictionaryValue::Create();
		desc->SetString(L"pipe_name",szPipeName);
		Args->SetDictionary(6, desc);
		
		NamedPipe Pipe;
		Pipe.Create(szPipeName,NamedPipe::PIPE_SERV);
		if (browser->SendProcessMessage(PID_BROWSER, Msg) && Pipe.IsValid()) // 同步模式下等待调用结果
		{
			DWORD dwRead = 0;
			std::wstring strRet;
			if (Pipe.Read(strRet,dwRead,3000))
			{
				if (!strRet.empty()&&strRet == L"true")
				{
					bRet = true;
				}
			}
		}
	} while (false);
	return bRet;
}

void CefRenderApp::OnContextCreated(CefRefPtr<CefBrowser> browser, CefRefPtr<CefFrame> frame, CefRefPtr<CefV8Context> context)
{
	 int id = browser->GetIdentifier();
	 CefRefPtr<CefJSHandler> JSHandler;
	 std::map<int,CefRefPtr<CefJSHandler>>::iterator iter = m_BrowerV8Map.find(id);
	 if (m_BrowerV8Map.end() == iter)// 未创建,尝试创建
	 {
		JSHandler = new CefJSHandler(m_ExtraInfo);
		m_BrowerV8Map.insert(std::make_pair(id,JSHandler));
	 }
	 else
	 {
		 JSHandler = iter->second;
	 }
	 if (JSHandler)
	 {
		 JSHandler->OnContextCreated(browser,frame,context);
	 }
}

#pragma endregion
