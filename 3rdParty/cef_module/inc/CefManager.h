// ----------------------------------------------------------------
// Copyright (c)  
// All rights reserved.
// 
// File name:	CefManager.h
// File mark:   
// File summary:Cef管理器
// Author:		guoyouhuang
// Edition:     1.0
// Create date: 2017-9-4
// ----------------------------------------------------------------
#pragma once
#include "include/cef_app.h"
#include "include/cef_runnable.h"
#include "IMainUIRunner.h"

#define  g_pCefManager			  CefManager::GetInstance()
#define  g_pMainUIRunner          CefManager::GetInstance()->GetMainUIRunner() 

class CefManager 
{
public:
	static CefManager* GetInstance();

public:
	CefManager();

public:
	bool Initialize(CefSettings &settings,CefRefPtr<CefListValue>& ExtraInfo, bool bOsrRender = true,IMainUIRunner* pMainUIRunner=NULL);
	void UnInitialize();
	void PostQuitMsg();
	bool SetCookie(std::wstring strUrl, std::wstring strCookieName, std::wstring strCookieData, std::wstring strDoMain);
	bool IsOffsetRender() const;
	IMainUIRunner* GetMainUIRunner();
	CefRefPtr<CefListValue> GetExtraInfo();

	// 记录浏览器对象的数量，总是让主UI线程来调
	void AddBrowserCount();
	void SubBrowserCount();
	int	GetBrowserCount();
	
public:
	volatile long 					m_iBrowserNum;							///< 浏览器次数
	bool							m_bOsrRender;							///< 支持离屏模式
	bool                            m_bNeedQuit;                            ///< 需要退出消息循环了
	IMainUIRunner*					m_pMainUIRunner;						///< 支持多线程向主UI线程转发绑定函数
	CefRefPtr<CefListValue>         m_ExtraInfo;							
};
DISABLE_RUNNABLE_METHOD_REFCOUNT(CefManager);