// ----------------------------------------------------------------
// Copyright (c)  
// All rights reserved.
// 
// File name:	CefModuleAfx.h
// File mark:   
// File summary:cef_module的预编译
// Author:		guoyouhuang
// Edition:     1.0
// Create date: 2017-9-4
// ----------------------------------------------------------------
#pragma once
#define WIN32_LEAN_AND_MEAN             // 从 Windows 头中排除极少使用的资料
#include <windows.h>
#include <tchar.h> 

//c++ header
#include <ctime>
#include <string>
#include <vector>
#include <list>
#include <queue>
#include <set>
#include <map>
#include <memory>
#include <utility>
#include <shlwapi.h>
#pragma comment(lib, "shlwapi.lib")

//cef_warp
#include "include/cef_base.h"
#include "include/cef_app.h"
#if 1
#include "include/base/cef_bind.h"
#include "include/base/cef_bind_helpers.h"
#include "wrapper/cef_closure_task.h"
#endif
#include "include/cef_parser.h"
#include "include/cef_cookie.h"

//
#include "CefModHelper.h"
#include "CefJSHandler.h"
#include "CefManager.h"

//宏
#define REQUIRE_UI_THREAD()   assert(CefCurrentlyOn(TID_UI));
#define REQUIRE_IO_THREAD()   assert(CefCurrentlyOn(TID_IO));
#define REQUIRE_FILE_THREAD() assert(CefCurrentlyOn(TID_FILE));
