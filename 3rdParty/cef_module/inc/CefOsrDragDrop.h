// ----------------------------------------------------------------
// Copyright (c)  
// All rights reserved.
// 
// File name:	CefOsrDragDrop.h
// File mark:   
// File summary:
// Author:		guoyouhuang
// Edition:     1.0
// Create date: 2017-9-19
// ----------------------------------------------------------------
#pragma once
#include <atlcomcli.h>
#include "include/cef_render_handler.h"

/// <summary>
///		�ο�osr_dragdrop_events.h
/// </summary>
class OsrDragEvents
{
public:
	virtual CefBrowserHost::DragOperationsMask OnDragEnter(CefRefPtr<CefDragData> drag_data,CefMouseEvent ev,CefBrowserHost::DragOperationsMask effect) = 0;
	virtual CefBrowserHost::DragOperationsMask OnDragOver(CefMouseEvent ev,CefBrowserHost::DragOperationsMask effect) = 0;
	virtual void OnDragLeave() = 0;
	virtual CefBrowserHost::DragOperationsMask OnDrop(CefMouseEvent ev,CefBrowserHost::DragOperationsMask effect) = 0;
};


#define DEFAULT_QUERY_INTERFACE(__Class) \
HRESULT __stdcall QueryInterface(const IID& iid, void** object) {*object = NULL;\
	if (IsEqualIID(iid, IID_IUnknown)) { IUnknown* obj = this; *object = obj;} \
	else if (IsEqualIID(iid, IID_ ## __Class)) {__Class* obj = this; *object = obj;} \
	else {return E_NOINTERFACE;} \
	AddRef(); return S_OK;}
#define IUNKNOWN_IMPLEMENTATION \
ULONG __stdcall AddRef() { return ++ref_count_;} \
ULONG __stdcall Release() {if (--ref_count_ == 0) {delete this;return 0U;}return ref_count_;} \
  protected: \
		ULONG ref_count_;


//--------------------------------------------------------------------
/// <summary>
///		�ο�bytes_write_handler.h
/// </summary>
class BytesWriteHandler : public CefWriteHandler 
{
public:
	explicit BytesWriteHandler(size_t grow);
	~BytesWriteHandler();

	size_t Write(const void* ptr, size_t size, size_t n) OVERRIDE;
	int Seek(int64 offset, int whence) OVERRIDE;
	int64 Tell() OVERRIDE;
	int	Flush() OVERRIDE;
	bool MayBlock() OVERRIDE {return false;}
	void* GetData() {return data_;}
	int64 GetDataSize() {return offset_;}

private:
	size_t Grow(size_t size);

	size_t								grow_;
	void*								data_;
	int64								datasize_;
	int64								offset_;
	base::Lock							lock_;

	IMPLEMENT_REFCOUNTING(BytesWriteHandler);
	DISALLOW_COPY_AND_ASSIGN(BytesWriteHandler);
};


/// <summary>
///		�ο�osr_dragdrop_win.h
/// </summary>
class DropTargetWin : public IDropTarget 
{
public:
	static CComPtr<DropTargetWin> Create(OsrDragEvents* callback, HWND hWnd);
	CefBrowserHost::DragOperationsMask StartDragging(CefRefPtr<CefBrowser> browser,CefRefPtr<CefDragData> drag_data,CefRenderHandler::DragOperationsMask allowed_ops,int x, int y);

	// IDropTarget implementation:
	HRESULT __stdcall DragEnter(IDataObject* data_object,DWORD key_state,POINTL cursor_position,DWORD* effect);
	HRESULT __stdcall DragOver(DWORD key_state,POINTL cursor_position,DWORD* effect);
	HRESULT __stdcall DragLeave();
	HRESULT __stdcall Drop(IDataObject* data_object,DWORD key_state,POINTL cursor_position,DWORD* effect);

	DEFAULT_QUERY_INTERFACE(IDropTarget)
	IUNKNOWN_IMPLEMENTATION

protected:
	DropTargetWin(OsrDragEvents* callback, HWND hWnd): ref_count_(0),callback_(callback),hWnd_(hWnd) {}
	virtual ~DropTargetWin() {}

private:
	OsrDragEvents*						callback_;
	HWND								hWnd_;
	CefRefPtr<CefDragData>				current_drag_data_;
};


class DropSourceWin : public IDropSource
{
public:
	static CComPtr<DropSourceWin> Create();

	HRESULT __stdcall GiveFeedback(DWORD dwEffect);
	HRESULT __stdcall QueryContinueDrag(BOOL fEscapePressed, DWORD grfKeyState);
	DEFAULT_QUERY_INTERFACE(IDropSource)
	IUNKNOWN_IMPLEMENTATION

protected:
	explicit DropSourceWin() : ref_count_(0) {}
	virtual ~DropSourceWin() {}
};

class DragEnumFormatEtc : public IEnumFORMATETC 
{
public:
	static HRESULT CreateEnumFormatEtc(UINT cfmt,FORMATETC* afmt,IEnumFORMATETC** ppEnumFormatEtc);

	// IEnumFormatEtc members
	HRESULT __stdcall Next(ULONG celt,FORMATETC * pFormatEtc,ULONG * pceltFetched);
	HRESULT __stdcall Skip(ULONG celt);
	HRESULT __stdcall Reset(void);
	HRESULT __stdcall Clone(IEnumFORMATETC ** ppEnumFormatEtc);

	// Construction / Destruction
	DragEnumFormatEtc(FORMATETC *pFormatEtc, int nNumFormats);
	~DragEnumFormatEtc();

	static void DeepCopyFormatEtc(FORMATETC *dest, FORMATETC *source);
	DEFAULT_QUERY_INTERFACE(IEnumFORMATETC)
	IUNKNOWN_IMPLEMENTATION

private:
	ULONG									 m_nIndex; 
	ULONG									 m_nNumFormats; 
	FORMATETC*								 m_pFormatEtc; 
};

class DataObjectWin : public IDataObject 
{
public:
	static CComPtr<DataObjectWin> Create(FORMATETC* fmtetc,STGMEDIUM* stgmed,int count);

	// IDataObject memberS
	HRESULT __stdcall GetDataHere(FORMATETC* pFormatEtc, STGMEDIUM *pmedium);
	HRESULT __stdcall QueryGetData(FORMATETC* pFormatEtc);
	HRESULT __stdcall GetCanonicalFormatEtc(FORMATETC* pFormatEct,FORMATETC* pFormatEtcOut);
	HRESULT __stdcall SetData(FORMATETC* pFormatEtc,STGMEDIUM* pMedium,BOOL fRelease);
	HRESULT __stdcall DAdvise(FORMATETC* pFormatEtc,DWORD advf,IAdviseSink*,DWORD*);
	HRESULT __stdcall DUnadvise(DWORD dwConnection);
	HRESULT __stdcall EnumDAdvise(IEnumSTATDATA **ppEnumAdvise);
	HRESULT __stdcall EnumFormatEtc(DWORD dwDirection,IEnumFORMATETC **ppEnumFormatEtc);
	HRESULT __stdcall GetData(FORMATETC *pFormatEtc, STGMEDIUM *pMedium);
	DEFAULT_QUERY_INTERFACE(IDataObject)
	IUNKNOWN_IMPLEMENTATION

protected:
	int										m_nNumFormats;
	FORMATETC*								m_pFormatEtc;
	STGMEDIUM*								m_pStgMedium;

	static HGLOBAL DupGlobalMem(HGLOBAL hMem);
	int LookupFormatEtc(FORMATETC *pFormatEtc);

	explicit DataObjectWin(FORMATETC *fmtetc, STGMEDIUM *stgmed, int count);
	~DataObjectWin();
};
