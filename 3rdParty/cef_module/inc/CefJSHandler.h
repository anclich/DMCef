// ----------------------------------------------------------------
// Copyright (c)  
// All rights reserved.
// 
// File name:	CefJSHandler.h
// File mark:   
// File summary:每一个browser拥有一个CefJSHandler对象
// Author:		guoyouhuang
// Edition:     1.0
// Create date: 2017-9-5
// ----------------------------------------------------------------
#pragma once

class CefJSHandler : public CefV8Handler
{
public:
	CefJSHandler(CefRefPtr<CefListValue>& ExtraInfo);
	void OnContextCreated(CefRefPtr<CefBrowser> browser, CefRefPtr<CefFrame> frame, CefRefPtr<CefV8Context> context);
	virtual bool Execute(const CefString& name,	CefRefPtr<CefV8Value> object, const CefV8ValueList& arguments, CefRefPtr<CefV8Value>& retval, CefString& exception) OVERRIDE;

public:
	CefRefPtr<CefListValue>								m_ExtraInfo;			///< 不复制,仅增加引用计数
	IMPLEMENT_REFCOUNTING(CefJSHandler);
};