// ----------------------------------------------------------------
// Copyright (c)  
// All rights reserved.
// 
// File name:	IMainUIRunner.h 
// File mark:   
// File summary:任意线程向主UI线程Post任务,跨线程调用
// Author:		guoyouhuang
// Edition:     1.0
// Create date: 2017-9-4
// ----------------------------------------------------------------
#pragma once

class IMainUIRunner
{
public:
	virtual void PostTask(CefRefPtr<CefTask> task) = 0;			///< Post任务给主UI线程调用
	virtual void PostTask(const base::Closure& closure) = 0;    ///< Post任务给主UI线程调用
	virtual bool RunTaskOnMainUI() const = 0;					///< 是否运行在当前线程上
};