// ----------------------------------------------------------------
// Copyright (c)  
// All rights reserved.
// 
// File name:	CefBrowserHandler.h
// File mark:   
// File summary:实现CefClient接口,处理Cef浏览器对象发出的各个事件和消息，并与上层控件进行数据交互
// Author:		guoyouhuang
// Edition:     1.0
// Create date: 2017-9-4
// ----------------------------------------------------------------
#pragma once
#include "include/cef_context_menu_handler.h"
#include "include/cef_client.h"
#include "include/cef_browser.h"

/// <summary>
///		非离屏模式
/// </summary>
class CefClientHandler : public CefClient,public CefLifeSpanHandler,public CefContextMenuHandler,public CefDisplayHandler,public CefDragHandler,
						 public CefGeolocationHandler,public CefJSDialogHandler,public CefKeyboardHandler,public CefLoadHandler,public CefRequestHandler
{
public:
	CefClientHandler();
	class Delegate : public CefClient,public CefLifeSpanHandler,public CefContextMenuHandler,public CefDisplayHandler,public CefDragHandler,public CefKeyboardHandler,public CefLoadHandler
	{IMPLEMENT_REFCOUNTING(Delegate);};

public:
	virtual CefRefPtr<CefContextMenuHandler> GetContextMenuHandler() OVERRIDE {	return this; }
	virtual CefRefPtr<CefDisplayHandler>	 GetDisplayHandler()	 OVERRIDE { return this; }
	virtual CefRefPtr<CefDragHandler>		 GetDragHandler()		 OVERRIDE { return this; }
	virtual CefRefPtr<CefGeolocationHandler> GetGeolocationHandler() OVERRIDE { return this; }
	virtual CefRefPtr<CefJSDialogHandler>	 GetJSDialogHandler()	 OVERRIDE { return this; }
	virtual CefRefPtr<CefKeyboardHandler>	 GetKeyboardHandler()	 OVERRIDE { return this; }
	virtual CefRefPtr<CefLifeSpanHandler>	 GetLifeSpanHandler()	 OVERRIDE { return this; }
	virtual CefRefPtr<CefLoadHandler>		 GetLoadHandler()		 OVERRIDE { return this; }
	virtual CefRefPtr<CefRequestHandler>	 GetRequestHandler()	 OVERRIDE { return this; }

public:// 自定义相关接口
	void SetDelegate(Delegate *pDelegate);									///< 设置委托类指针
	void SetHostWindow(HWND hwnd);											///< 设置Cef浏览器对象所属的窗体的句柄
	CefRefPtr<CefBrowser> GetBrowser();										
	CefRefPtr<CefBrowserHost> GetBrowserHost();

public:
	//---------------------------------------------------
	// Function Des: CefClient methods
	//---------------------------------------------------
	virtual bool OnProcessMessageReceived(CefRefPtr<CefBrowser> browser, CefProcessId source_process, CefRefPtr<CefProcessMessage> message)	OVERRIDE;

	//---------------------------------------------------
	// Function Des: CefLifeSpanHandler methods
	//---------------------------------------------------
	virtual bool OnBeforePopup(CefRefPtr<CefBrowser> browser, CefRefPtr<CefFrame> frame, const CefString& target_url, const CefString& target_frame_name, CefLifeSpanHandler::WindowOpenDisposition target_disposition, bool user_gesture, const CefPopupFeatures& popupFeatures, CefWindowInfo& windowInfo, CefRefPtr<CefClient>& client, CefBrowserSettings& settings, bool* no_javascript_access)OVERRIDE;
	virtual void OnAfterCreated(CefRefPtr<CefBrowser> browser)OVERRIDE;
	virtual void OnBeforeClose(CefRefPtr<CefBrowser> browser) OVERRIDE;

	//---------------------------------------------------
	// Function Des: CefContextMenuHandler methods
	//---------------------------------------------------
	virtual void OnBeforeContextMenu(CefRefPtr<CefBrowser> browser, CefRefPtr<CefFrame> frame, CefRefPtr<CefContextMenuParams> params, CefRefPtr<CefMenuModel> model) OVERRIDE;

	//---------------------------------------------------
	// Function Des: CefDisplayHandler methods
	//---------------------------------------------------
	virtual void OnAddressChange(CefRefPtr<CefBrowser> browser, CefRefPtr<CefFrame> frame,const CefString& url)OVERRIDE;
	virtual void OnTitleChange(CefRefPtr<CefBrowser> browser,const CefString& title)OVERRIDE;
	virtual void OnFullscreenModeChange(CefRefPtr<CefBrowser> browser,bool fullscreen)OVERRIDE;

	//---------------------------------------------------
	// Function Des: CefKeyboardHandler methods
	//---------------------------------------------------
	virtual bool OnPreKeyEvent(CefRefPtr<CefBrowser> browser,const CefKeyEvent& event,CefEventHandle os_event,bool* is_keyboard_shortcut)OVERRIDE;

	//---------------------------------------------------
	// Function Des: CefLoadHandler methods
	//---------------------------------------------------
	virtual void OnLoadEnd(CefRefPtr<CefBrowser> browser,CefRefPtr<CefFrame> frame,int httpStatusCode)OVERRIDE;


public:
	Delegate*											m_pDelegate;
	CefRefPtr<CefBrowser>								m_pBrowser;
	HWND												m_hWnd;
	IMPLEMENT_REFCOUNTING(CefClientHandler)		
};


/// <summary>
///		离屏模式
/// </summary>
class CefOsrClientHandler : public CefClientHandler,public CefRenderHandler
{
public:
	CefOsrClientHandler();
	class OsrDelegate : public CefRenderHandler{IMPLEMENT_REFCOUNTING(OsrDelegate);};

public:
	virtual CefRefPtr<CefRenderHandler>   GetRenderHandler()		OVERRIDE { return this; }

public:// 自定义相关接口
	void SetOsrDelegate(OsrDelegate *pOsrDelegate);								///< 设置委托类指针
	void SetViewRect(RECT rect);												///< 设置Cef渲染内容的大小,用于离屏模式
	
public:
	//---------------------------------------------------
	// Function Des: CefRenderHandler methods
	//---------------------------------------------------
	virtual bool GetRootScreenRect(CefRefPtr<CefBrowser> browser, CefRect& rect) OVERRIDE;
	virtual bool GetViewRect(CefRefPtr<CefBrowser> browser, CefRect& rect) OVERRIDE;
	virtual bool GetScreenPoint(CefRefPtr<CefBrowser> browser, int viewX, int viewY, int& screenX, int& screenY) OVERRIDE;
	virtual void OnPopupShow(CefRefPtr<CefBrowser> browser, bool show) OVERRIDE;
	virtual void OnPopupSize(CefRefPtr<CefBrowser> browser, const CefRect& rect) OVERRIDE;
	virtual void OnPaint(CefRefPtr<CefBrowser> browser,	PaintElementType type, const RectList& dirtyRects, const void* buffer, int width, int height) OVERRIDE;
	virtual void OnCursorChange(CefRefPtr<CefBrowser> browser, CefCursorHandle cursor, CursorType type,	const CefCursorInfo& custom_cursor_info) OVERRIDE;


public:
	OsrDelegate*										m_pOsrDelegate;
	RECT												m_rcView;
	IMPLEMENT_REFCOUNTING(CefOsrClientHandler)
};