// ----------------------------------------------------------------
// Copyright (c)  
// All rights reserved.
// 
// File name:	CefRenderApp.h 
// File mark:   
// File summary:定义Render进程的CefRenderApp类，管理Cef模块的生命周期
// Author:		guoyouhuang
// Edition:     1.0
// Create date: 2017-9-7
// ----------------------------------------------------------------
#pragma once
#include "CefJSHandler.h"

class CefRenderApp : public CefApp,
					 public CefRenderProcessHandler
{
public:
	CefRenderApp();
	virtual CefRefPtr<CefRenderProcessHandler> GetRenderProcessHandler() OVERRIDE{return this;}


	//---------------------------------------------------
	// Function Des: CefRenderProcessHandler methods
	//---------------------------------------------------
	virtual void OnRenderThreadCreated(CefRefPtr<CefListValue> extra_info) OVERRIDE;
	virtual bool OnBeforeNavigation(CefRefPtr<CefBrowser> browser,CefRefPtr<CefFrame> frame,CefRefPtr<CefRequest> request,NavigationType navigation_type,bool is_redirect) OVERRIDE;
	virtual void OnContextCreated(CefRefPtr<CefBrowser> browser, CefRefPtr<CefFrame> frame, CefRefPtr<CefV8Context> context) OVERRIDE;

public:
	CefRefPtr<CefListValue>								m_ExtraInfo;
	std::map<int/*浏览器ID*/,CefRefPtr<CefJSHandler>>	m_BrowerV8Map;
	IMPLEMENT_REFCOUNTING(CefRenderApp);
};