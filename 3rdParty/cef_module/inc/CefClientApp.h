// ----------------------------------------------------------------
// Copyright (c)  
// All rights reserved.
// 
// File name:	CefClientApp.h
// File mark:   
// File summary:定义Browser进程的CefClientApp类，管理Cef模块的生命周期
// Author:		guoyouhuang
// Edition:     1.0
// Create date: 2017-9-4
// ----------------------------------------------------------------
#pragma once
#include "CefRenderApp.h"

class CefClientApp :public CefBrowserProcessHandler,
					public CefRenderApp
{
public:
	virtual CefRefPtr<CefBrowserProcessHandler> GetBrowserProcessHandler() OVERRIDE{return this;}
	virtual CefRefPtr<CefRenderProcessHandler> GetRenderProcessHandler() OVERRIDE{return this;}


	//---------------------------------------------------
	// Function Des: CefApp methods
	//---------------------------------------------------
	virtual void OnBeforeCommandLineProcessing(const CefString& process_type, CefRefPtr<CefCommandLine> command_line) OVERRIDE;


	//---------------------------------------------------
	// Function Des: CefBrowserProcessHandler methods
	//---------------------------------------------------
	virtual void OnRenderProcessThreadCreated(CefRefPtr<CefListValue> extra_info) OVERRIDE;

public:
	IMPLEMENT_REFCOUNTING(CefClientApp);
};
