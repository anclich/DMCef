// ----------------------------------------------------------------
// Copyright (c)  
// All rights reserved.
// 
// File name:	CefModHelper.h
// File mark:   
// File summary:一些辅助函数
// Author:		guoyouhuang
// Edition:     1.0
// Create date: 2017-9-4
// ----------------------------------------------------------------
#pragma once
#include <string>

/// <summary>
///	    基础宏定义
/// </summary>
static const char kJsCallbackMessage[] = "JsCallback";					// web调用C++接口接口的通知
static const char kBeforeNavigateMessage[] = "BeforeNavigate";			// BeforeNavigate接口通知

#ifndef    IsValidString
#define    IsValidString(x)							((x) && (x)[0])
#endif
#ifndef    SAFE_STR
#define    SAFE_STR(a) IsValidString(a) ? a :L""
#endif


/// <summary>
///		额外数据的index
/// </summary>
enum enumExtraInfo
{
	EXTRAINFO_FUNNAMES = 0,
	EXTRAINFO_MAX,
};


/// <summary>
///		窗口消息转换函数
/// </summary>
int GetCefMouseModifiers(WPARAM wparam);
int GetCefKeyboardModifiers(WPARAM wparam, LPARAM lparam);
bool IsKeyDown(WPARAM wparam);


/// <summary>
///		参考chromium_git\chromium\src\base\synchronization\waitable_event.h
/// </summary>
class WaitableEvent
{
public:
	WaitableEvent(bool bManReset, bool bSignaled);
	~WaitableEvent();
	void Reset();
	void Signal();
	bool Wait(DWORD dwMaxTimeOut = INFINITE);
	bool DealWithMsg();
public:
	HANDLE							 m_Handle;
};


/// <summary>
///		命名管道通讯工具,参考http://blog.csdn.net/dengxu11/article/details/7174288
/// </summary>
class NamedPipe
{
public:
	enum{PIPE_SERV,PIPE_CLIN,PIPE_UNDF};
	NamedPipe();
	NamedPipe(LPCWSTR lpszPipeName, int nMode);
	~NamedPipe();
	bool IsValid() const;
	bool Create(LPCWSTR lpszPipeName, int nMode);  
	bool Read(std::wstring& strRet,DWORD& dwRead,DWORD dwTimeOut = INFINITE);
	bool Write(void* pBuf, int nSize, DWORD& dwWrite); 

public:
	HANDLE							 m_hPipe;
	int                              m_nMode;
	std::wstring                     m_strPipeName;
	HANDLE                           m_hEvent;
};

