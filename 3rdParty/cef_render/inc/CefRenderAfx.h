// ----------------------------------------------------------------
// Copyright (c)  
// All rights reserved.
// 
// File name:	CefModuleAfx.h
// File mark:   
// File summary:cef_module的预编译
// Author:		guoyouhuang
// Edition:     1.0
// Create date: 2017-9-4
// ----------------------------------------------------------------
#pragma once
#define WIN32_LEAN_AND_MEAN             // 从 Windows 头中排除极少使用的资料
#include <windows.h>
#include <tchar.h> 

////cef_warp
#include "include/cef_base.h"
#include "include/cef_app.h"

// lib
#ifdef _DEBUG
#pragma comment(lib,"libcef.lib")
#pragma comment(lib,"libcef_dll_wrapper_d.lib")
#pragma comment(lib,"cef_module_d.lib")
#else
#pragma comment(lib,"libcef.lib")
#pragma comment(lib,"libcef_dll_wrapper.lib")
#pragma comment(lib,"cef_module.lib")
#endif

#include "CefModHelper.h"
#include "CefRenderApp.h"


