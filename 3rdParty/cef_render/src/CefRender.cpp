// ----------------------------------------------------------------
// Copyright (c)  
// All rights reserved.
// 
// File name:	CefRender.h
// File mark:   
// File summary:
// Author:		guoyouhuang
// Edition:     1.0
// Create date: 2017-9-7
// ----------------------------------------------------------------
#pragma once
#include "CefRenderAfx.h"

int APIENTRY _tWinMain(HINSTANCE hInstance,
					   HINSTANCE hPrevInstance,
					   LPTSTR    lpCmdLine,
					   int       nCmdShow)
{
	_wsetlocale(LC_ALL, L"chs");

	CefMainArgs MainArgs(GetModuleHandle(NULL));
	CefRefPtr<CefRenderApp> App(new CefRenderApp);

	// 执行子进程逻辑，此时会堵塞直到子进程退出。
	return CefExecuteProcess(MainArgs, App.get(), NULL);
}