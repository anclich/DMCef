#include "StdAfx.h"
#include "MainWnd.h"
#include "DUICheckBox.h"

BEGIN_MSG_MAP(CMainWnd)
	MSG_WM_INITDIALOG(OnInitDialog)
	MSG_WM_DESTROY(OnDestroy)
	MSG_WM_SIZE(OnSize)
	CHAIN_MSG_MAP(DMHWnd)// 将未处理的消息交由DMHWnd处理
END_MSG_MAP()
BEGIN_EVENT_MAP(CMainWnd)
	EVENT_NAME_COMMAND(L"closebutton",OnClose)
	EVENT_NAME_COMMAND(L"maxbutton",OnMaximize)
	EVENT_NAME_COMMAND(L"restorebutton",OnRestore)
	EVENT_NAME_COMMAND(L"minbutton", OnMinimize)
	EVENT_NAME_COMMAND(L"btn_back", OnBack)
	EVENT_NAME_COMMAND(L"btn_forward", OnForward)
	EVENT_NAME_COMMAND(L"btn_url", OnUrlBtn)
	EVENT_NAME_COMMAND(L"btn_refresh", OnRefresh)
	EVENT_NAME_COMMAND(L"btn_send_js", OnSendJS)
	EVENT_NAME_COMMAND(L"btn_initcef", OnInitCef)
	EVENT_NAME_COMMAND(L"btn_initjs", OnInitJS)
END_EVENT_MAP()
CMainWnd::CMainWnd()
{
	m_pUrlEdit = m_pJSEdit = NULL;
	m_pCurWeb = NULL;
	m_pCurPage = NULL;
	m_pExternalObject = NULL;
	m_pTabMain = NULL;
	m_bInit = false;
}

CMainWnd::~CMainWnd()
{
	DM_DELETE(m_pExternalObject);
}

BOOL CMainWnd::OnInitDialog(HWND wndFocus, LPARAM lInitParam)
{
	m_pUrlEdit = FindChildByNameT<DUIEdit>(L"edit_url");DMASSERT(m_pUrlEdit);
	m_pJSEdit  = FindChildByNameT<DUIEdit>(L"edit_send_js");DMASSERT(m_pJSEdit);
	m_pTabMain = FindChildByNameT<TabCtrl>(L"tab_main");DMASSERT(m_pTabMain);
	m_pUrlEdit->m_EventMgr.SubscribeEvent(DM::DMEventREWantReturnArgs::EventID, Subscriber(&CMainWnd::OnUrlReturn, this));
	m_pTabMain->m_EventMgr.SubscribeEvent(TabCtrlSelChangedArgs::EventID,Subscriber(&CMainWnd::OnTabMainSel,this));
	m_pUrlEdit->DV_SetFocusWnd();
	return TRUE;
}

void CMainWnd::OnDestroy()
{
	m_pUrlEdit->m_EventMgr.UnSubscribeEvent(DM::DMEventREWantReturnArgs::EventID, Subscriber(&CMainWnd::OnUrlReturn, this));
	m_pTabMain->m_EventMgr.UnSubscribeEvent(TabCtrlSelChangedArgs::EventID,Subscriber(&CMainWnd::OnTabMainSel,this));
	m_bInit = false;
	SetMsgHandled(FALSE);
}

void CMainWnd::OnSize(UINT nType, CSize size)
{
	DUIWindow* pMaxBtn = FindChildByName(L"maxbutton");
	DUIWindow* pRestoreBtn = FindChildByName(L"restorebutton");
	if (0 != size.cx&&0 != size.cy&&pMaxBtn&&pRestoreBtn)
	{
		if (SIZE_MAXIMIZED == nType)
		{
			pMaxBtn->DM_SetVisible(false);
			pRestoreBtn->DM_SetVisible(true);
		}
		else if (SIZE_RESTORED == nType)
		{
			pMaxBtn->DM_SetVisible(true);
			pRestoreBtn->DM_SetVisible(false);
		}
	}
	SetMsgHandled(FALSE);  // 由DMHWnd继续处理OnSize消息
}

void CMainWnd::OnAfterClosed()
{
	if (m_pDMWebApp.isNull())
	{
		__super::OnAfterClosed();
	}
	else
	{
		if (DMSUCCEEDED(g_pDMApp->IsRun(m_hWnd)))
		{
			m_pDMWebApp->PostQuitMsg();
		}
	}
}

DMCode CMainWnd::OnTabMainSel(DMEventArgs *pEvt)
{
	TabCtrlSelChangedArgs *pEvent = (TabCtrlSelChangedArgs*)pEvt;
	if (pEvent)
	{
		TabPage* pPage = m_pTabMain->GetObj(pEvent->m_iNewSel);
		if (pPage)
		{
			m_pCurWeb = pPage->FindChildByNameT<IDUIWeb>(L"cef");
		}
	}
	return DM_ECODE_OK;
}

DMCode CMainWnd::OnClose()
{
	DestroyWindow(); 
	return DM_ECODE_OK;
}

DMCode CMainWnd::OnMaximize()
{
	SendMessage(WM_SYSCOMMAND, SC_MAXIMIZE);
	return DM_ECODE_OK;
}

DMCode CMainWnd::OnRestore()
{
	SendMessage(WM_SYSCOMMAND,SC_RESTORE);
	return DM_ECODE_OK;
}

DMCode CMainWnd::OnMinimize()
{
	SendMessage(WM_SYSCOMMAND,SC_MINIMIZE);
	return DM_ECODE_OK;
}

//-----------------------------------------------------
DMCode CMainWnd::OnBack()
{
	if (m_pCurWeb)
	{
		m_pCurWeb->GoBack();
	}
	return DM_ECODE_OK;
}

DMCode CMainWnd::OnForward()
{
	if (m_pCurWeb)
	{
		m_pCurWeb->GoForward();
	}
	return DM_ECODE_OK;
}

DMCode CMainWnd::OnUrlBtn()
{
	CStringW strUrl = m_pUrlEdit->GetWindowText();
	if (!strUrl.IsEmpty())
	{
		if (m_pCurWeb)
		{
			m_pCurWeb->OpenUrl(strUrl);
		}
	}

	return DM_ECODE_OK;
}

DMCode CMainWnd::OnUrlReturn(DMEventArgs* pEvent)
{
	return OnUrlBtn();
}

DMCode CMainWnd::OnRefresh()
{
	if (m_pCurWeb)
	{
		m_pCurWeb->Refresh();
	}
	return DM_ECODE_OK;
}

DMCode CMainWnd::OnInitCef()
{
	DUICheckBox* pCheckBox = FindChildByNameT<DUICheckBox>(L"cbx_osr");DMASSERT(pCheckBox);
	if (InitDMWeb(pCheckBox->DM_IsChecked()))
	{
		pCheckBox->DM_EnableWindow(false,true);
		FindChildByName(L"sta_cef")->DV_SetWindowText(L"CEF初始化成功");
	}
	InitDMWebView();
	OnUrlBtn();
	return DM_ECODE_OK;
}	

DMCode CMainWnd::OnInitJS()
{
	if (m_pCurWeb)
	{
		m_pCurWeb->OpenUrl(L"http://f2e.yy.com/1709/m_368818052777.html?3");
	}
	return DM_ECODE_OK;
}

DMCode CMainWnd::OnSendJS()
{
	if (m_pCurWeb)
	{
		DM::CArray<LPCWSTR> vecParams;
		CStringW strParam = m_pJSEdit->GetWindowText();
		vecParams.Add(strParam);
		m_pCurWeb->ExecuteScriptFuntion(L"CppCallJs",vecParams);
	}
	return DM_ECODE_OK;
}

//-----------------------------------------------------------------------
bool CMainWnd::InitDMWeb(bool bOsr)
{
	if (m_bInit)
	{// 已初始化
		return true;
	}
	HMODULE hMod = NULL;
#ifdef _DEBUG
	hMod = ::GetModuleHandleW(L"PluginWeb_d.dll");
#else
	hMod = ::GetModuleHandleW(L"PluginWeb.dll");
#endif
	if (hMod)
	{
		LPVOID fun_DMWebCreate = ::GetProcAddress(hMod,"DMWebCreate");
		if (fun_DMWebCreate)
		{
			((void (*)(IDMWebPtr*))fun_DMWebCreate)((IDMWebPtr*)&m_pDMWebApp);
		}
	}

	wchar_t szPath[MAX_PATH] = {0};
	DM::GetRootFullPath(L".\\Cef\\libcef.dll",szPath,MAX_PATH);
	if (PathFileExists(szPath))// CEF存在才加载DMCEF插件,可以此处检测更多条件
	{
		if (m_pDMWebApp)
		{
			DM::GetRootFullPath(L".\\Cef",szPath,MAX_PATH);
			m_pExternalObject = new DMExternalHandler;
			m_bInit = m_pDMWebApp->InitializeWeb(bOsr,szPath,m_pExternalObject);
		}
	}
	return m_bInit;
}

bool CMainWnd::InitDMWebView()
{
	do 
	{
		if (!m_bInit)
		{
			break;
		}

		CStringW strWXml = L"<root><cef name=\"cef\" pos=\"0,0,-0,-0\" refreshkey=\"f5\" ncmargin=\"1,1,1,1\" clrnc=\"pbgra(00,00,00,ff)\" /></root>";
		CStringA strXml = DMW2A(strWXml,CP_UTF8);
		DMXmlDocument Doc;
		Doc.LoadFromBuffer((const PVOID)(LPCSTR)strXml, strXml.GetLength());
		DMXmlNode XmlNode = Doc.Root();
		m_pCurPage = m_pTabMain->GetObj(0);DMASSERT(m_pCurPage);
		m_pCurPage->DV_CreateChildWnds(XmlNode);
		m_pCurWeb = m_pCurPage->FindChildByNameT<IDUIWeb>(L"cef");
		if (m_pWebEvent.isNull())
		{
			m_pWebEvent.Attach(new WebEvent());
		}
		m_pCurWeb->SetEvtHandler(m_pWebEvent);
		DV_UpdateChildLayout();
	} while (false);
	return NULL != m_pCurWeb;
}


