#include "StdAfx.h"
#include "TabCtrl.h"

#define  TABITEM_LEN      80
TabButton::TabButton()
{
	m_pParent = NULL;
	m_pPage   = NULL;
}

void TabButton::SetData(TabCtrl* pParent,TabPage* pPage)
{
	m_pParent = pParent;
	m_pPage = pPage;
}

void TabButton::OnMouseMove(UINT nFlags,CPoint pt)
{
	SetMsgHandled(FALSE);
	if (m_pParent)
	{// 让父TabCtrl的标签也收到move消息
		m_pParent->OnMouseMove(nFlags,pt);
	}
} 

void TabButton::OnLButtonUp(UINT nFlags,CPoint pt)
{
	SetMsgHandled(FALSE);
	if (m_pParent)
	{
		int iNewSel = m_pParent->GetCurSel()-1;///< 如果删除选中页，就把选中项置前一页,因为首页不能删除，iSel!=0
		m_pParent->DeleteItem(m_pPage,iNewSel);
	}
}

///TabPage --------------------------------------------------
TabPage::TabPage()
{
	m_pDUIXmlInfo->m_bVisible = false;// 默认Page页是隐藏的
	m_ItemSize.SetSize(-1,-1);
	m_pParent = NULL;
	m_pCloseBtn = NULL;
	m_iIconOffset = 20;
}

void TabPage::SetParant(TabCtrl* pParent)
{
	m_pParent = pParent;
	if (NULL == m_pCloseBtn && !m_strCloseSkin.IsEmpty())
	{
		g_pDMApp->CreateRegObj((void**)&m_pCloseBtn, TabButton::GetClassName(),DMREG_Window);
		if (m_pCloseBtn)
		{
			m_pCloseBtn->SetAttribute(L"skin", m_strCloseSkin,true);// 初始化时不要使用false
			m_pParent->DM_InsertChild(m_pCloseBtn);
			m_pCloseBtn->SetData(m_pParent,this);// 互相包含
		}  
	}  
}  

void TabPage::Layout()
{  
	if (m_pParent && m_pCloseBtn)
	{
		CRect rcItem = m_pParent->GetItemRect(this);// 得到标签区
		if (!rcItem.IsRectEmpty()&& m_pCloseSkin)
		{
			CSize size; 
			m_pCloseSkin->GetStateSize(size);

			CRect rcClose = rcItem;
			MeetRect(rcClose,size);
			if (rcItem.right - rcClose.right>30
				&& rcItem.Width()>TABITEM_LEN)// 如果不大于TABITEM_LEN，就直接居中了
			{
				rcClose.OffsetRect(rcItem.right-rcClose.right-30,0);
			}
			m_pCloseBtn->DM_FloatLayout(rcClose);// 设置close按钮
			ShowOrHideBtn();
		}
	}
}

void TabPage::ShowOrHideBtn()
{// 在tab页小于TABITEM_LEN时,非选中状态不显示rcClose
	if (m_pParent && m_pCloseBtn)
	{
		CRect rcItem = m_pParent->GetItemRect(this);// 得到标签区
		if (rcItem.Width()<TABITEM_LEN)
		{
			TabPage* pSelPage = NULL;
			m_pParent->GetObj(m_pParent->GetCurSel(),pSelPage);
			if (pSelPage != this)
			{
				m_pCloseBtn->DM_SetVisible(false,true);
			}
			else
			{
				m_pCloseBtn->DM_SetVisible(true,true);
			}
		}
		else
		{
			m_pCloseBtn->DM_SetVisible(true,true);
		}
	}
}

void TabPage::OnDestroy()
{
	SetMsgHandled(FALSE);
	if (m_pParent && m_pCloseBtn)
	{
		m_pCloseBtn->SetData(NULL,NULL);
		m_pParent->DM_DestroyChildWnd(m_pCloseBtn);
	}
}

DMCode TabPage::OnItemTitle(LPCWSTR pszValue, bool bLoadXml)
{
	m_strTitle = pszValue;
	m_pDUIXmlInfo->m_strTooltipText = m_strTitle;
	if (!bLoadXml && m_pParent)
	{
		m_pParent->DM_Invalidate();
	}
	return DM_ECODE_OK;
}

DMCode TabPage::OnItemSkin(LPCWSTR pszValue, bool bLoadXml)
{
	m_pItemSkin = g_pDMApp->GetSkin(pszValue);
	if (!bLoadXml && m_pParent)
	{
		m_pParent->DM_Invalidate();
	}
	return DM_ECODE_OK;
}

DMCode TabPage::OnItemIconSkin(LPCWSTR pszValue, bool bLoadXml)
{
	m_pIconSkin = g_pDMApp->GetSkin(pszValue);
	if (!bLoadXml && m_pParent)
	{
		m_pParent->DM_Invalidate();
	}
	return DM_ECODE_OK;
}

DMCode TabPage::OnItemCloseSkin(LPCWSTR pszValue, bool bLoadXml)
{
	m_pCloseSkin = g_pDMApp->GetSkin(pszValue);
	m_strCloseSkin = pszValue;
	if (!bLoadXml && m_pParent && m_pCloseBtn)
	{
		m_pCloseBtn->SetAttribute(L"skin", m_strCloseSkin);
		m_pParent->DM_Invalidate();
	}
	return DM_ECODE_OK;
}

DMCode TabPage::OnItemSize(LPCWSTR pszValue, bool bLoadXml)
{
	dm_parsesize(pszValue,m_ItemSize);
	if (!bLoadXml && m_pParent)
	{
		m_pParent->DM_Invalidate();
	}
	return DM_ECODE_OK;
}

///TabCtrl -----------------------------------------
TabCtrl::TabCtrl()
{
	m_TextPt.SetPoint(-1,-1);           // 默认为-1，-1
	m_iHoverItem         = -1;
	m_iCurSelItem        = 0;
	m_iFirstItemOffset   = m_iLastItemOffset = 0;
	m_nTabAlign          = AlignTop;
	m_pDUIXmlInfo->m_bFocusable = true;
	m_iItemSpace         = 0;
	m_bDrag				 = false;

	// tabctrl
	DMADDEVENT(TabCtrlDelingArgs::EventID);
	DMADDEVENT(TabCtrlDelArgs::EventID);
	DMADDEVENT(TabCtrlSelChangingArgs::EventID);
	DMADDEVENT(TabCtrlSelChangedArgs::EventID);
}

int TabCtrl::InsertItem(DMXmlNode &XmlNode, int iInsert, bool bLoadXml)
{
	int iRealInsert = iInsert;
	do 
	{
		if (!XmlNode.IsValid())
		{
			iRealInsert = -1;
			break;
		}

		if (_wcsicmp(XmlNode.GetName(), TabPage::GetClassName()))
		{
			iRealInsert = -1;
			break;
		}

		TabPage* pChild = NULL;
		g_pDMApp->CreateRegObj((void**)&pChild, XmlNode.GetName(),DMREG_Window);
		if (!pChild)
		{
			iRealInsert = -1;
			break;
		}

		if (-1 == iInsert||iInsert>(int)GetCount())
		{
			iRealInsert = (int)GetCount();
		}
		DM_InsertChild(pChild);
		m_DMArray.InsertAt(iRealInsert, pChild);
		pChild->InitDMData(XmlNode);

		pChild->SetAttribute(L"pos",L"0,0,-0,-0",bLoadXml);
		pChild->DM_SetVisible(false,true);
		pChild->SetParant(this);
		if (!bLoadXml && m_iCurSelItem>=iRealInsert)
		{// bLoadXml表示在xml解析时初始化所有tab，这时不需要改变初始选中项
			m_iCurSelItem++;
		}

		if (!bLoadXml)
		{
			Layout();
			pChild->Layout();
			DV_UpdateChildLayout();
			DM_Invalidate();
		}
	} while (false);
	return iRealInsert;
}

int TabCtrl::GetCurSel()
{
	return m_iCurSelItem;
}

bool TabCtrl::SetCurSel(DUIWindow* pPage)
{
	bool bRet = false;
	do 
	{
		int iCount = (int)GetCount();
		int nIndex = -1;
		for (int i=0; i<iCount; i++)
		{
			TabPage* pMyPage = NULL;
			GetObj(i,pMyPage);
			if (pMyPage == pPage)
			{
				nIndex = i;
				break;
			}
		}
		if (-1 == nIndex)
		{
			break;
		}

		bRet = SetCurSel(nIndex);
	} while (false);
	return bRet;
}

bool TabCtrl::SetCurSel(int nIndex)
{
	bool bRet = false;
	do 
	{
		int iCount = (int)GetCount();
		if (nIndex<0
			||nIndex>=iCount
			||(m_iCurSelItem == nIndex))
		{
			break;
		}

		int iOldSelItem = m_iCurSelItem;

		// 1.
		TabCtrlSelChangingArgs Evt(this);
		Evt.m_iOldSel = iOldSelItem;
		Evt.m_iNewSel = nIndex;
		DV_FireEvent(Evt);
		if (Evt.m_bCancel)
		{
			break;
		}

		// 2.
		TabPage *pPage = NULL;
		TabPage *pOldPage = NULL;
		GetObj(nIndex,pPage);
		GetObj(iOldSelItem,pOldPage);
		m_iCurSelItem = nIndex;
		if (pOldPage)
		{
			pOldPage->DM_SetVisible(false,true);
		}
		if (pPage)
		{
			pPage->DM_SetVisible(true, true);
		}

		CRect rcOldItem = GetItemRect(iOldSelItem);
		CRect rcSelItem = GetItemRect(m_iCurSelItem);
		DM_InvalidateRect(rcOldItem);
		DM_InvalidateRect(rcSelItem);

		// 3.
		TabCtrlSelChangedArgs EndEvt(this);
		EndEvt.m_iNewSel = nIndex;
		EndEvt.m_iOldSel = iOldSelItem;
		DV_FireEvent(EndEvt);

		// 控制close显示隐藏
		for (int i=0; i<iCount; i++)
		{
			TabPage *pPage = NULL;
			GetObj(i,pPage);
			if (pPage)
			{
				pPage->ShowOrHideBtn();
			}
		}	
		bRet = true;
	} while (false);
	return bRet;
}

bool TabCtrl::SetCurSel(LPCWSTR lpszTitle)
{
	bool bRet = false;
	do 
	{
		if (NULL == lpszTitle)
		{
			break;
		}

		int iFind = -1;
		int iCount = (int)GetCount();
		for (int i=0; i<iCount; i++)
		{
			TabPage *pPage = NULL;
			GetObj(i,pPage);
			if (pPage && 0 == _wcsicmp(pPage->m_strTitle,lpszTitle))
			{
				iFind = i;
				break;
			}
		}
		if (-1 != iFind)
		{
			bRet = SetCurSel(iFind);
		}	
	} while (false);
	return bRet;
}

bool TabCtrl::SetItemTitle(int nIndex, LPCWSTR lpszTitle)
{
	TabPage *pPage = NULL;
	GetObj(nIndex,pPage);
	if (pPage)
	{
		pPage->m_strTitle = lpszTitle;
		CRect rcTitle = GetTitleRect();
		DM_InvalidateRect(rcTitle);
		return true;
	}

	return false;
}

CRect TabCtrl::GetItemRect(DUIWindow* pPage)
{
	CRect rcItem;
	int iCount = (int)GetCount();
	for (int i=0; i<iCount; i++)
	{
		TabPage *pMyPage = NULL;
		GetObj(i,pMyPage);
		if (pMyPage && pMyPage == pPage)
		{
			return GetItemRect(i);
		}
	}
	return rcItem;
}

CRect TabCtrl::GetItemRect(int nIndex)
{
	CRect rcItem;
	do 
	{
		if (nIndex<0
			||nIndex>=(int)GetCount())
		{
			break;
		}
		CRect rcTitle = GetTitleRect();
		switch (m_nTabAlign)
		{
		case AlignTop:
		case AlignBottom:
			{
				int iLeft = m_iFirstItemOffset;
				for (int i=0; i<nIndex; i++)
				{
					TabPage *pMyPage = NULL;
					GetObj(i,pMyPage);
					if (pMyPage)
					{
						iLeft += (-1 == pMyPage->m_ItemSize.cx)?m_ItemSize.cx:pMyPage->m_ItemSize.cx;
						iLeft += m_iItemSpace;
					}
				}
				TabPage *pPage = NULL;
				GetObj(nIndex,pPage);
				if (pPage)
				{
					rcItem.left = rcTitle.left + iLeft;
					rcItem.right = rcItem.left + ((-1 == pPage->m_ItemSize.cx)?m_ItemSize.cx:pPage->m_ItemSize.cx);
					if (AlignTop == m_nTabAlign)
					{
						rcItem.bottom = rcTitle.bottom;
						rcItem.top = rcItem.bottom - ((-1 == pPage->m_ItemSize.cy)?m_ItemSize.cy:pPage->m_ItemSize.cy);
					}
					else
					{
						rcItem.top = rcTitle.top;
						rcItem.bottom = rcItem.top +((-1 == pPage->m_ItemSize.cy)?m_ItemSize.cy:pPage->m_ItemSize.cy);
					}
				}
			}
			break;

		case AlignLeft:
		case AlignRight:
			{
				int iTop = m_iFirstItemOffset;
				for (int i=0; i<nIndex; i++)
				{
					TabPage *pMyPage = NULL;
					GetObj(i,pMyPage);
					if (pMyPage)
					{
						iTop += ((-1 == pMyPage->m_ItemSize.cy)?m_ItemSize.cy:pMyPage->m_ItemSize.cy);
						iTop += m_iItemSpace;
					}
				}
				TabPage *pPage = NULL;
				GetObj(nIndex,pPage);
				if (pPage)
				{
					rcItem.top  = rcTitle.top + iTop;
					rcItem.bottom = rcItem.top +((-1 == pPage->m_ItemSize.cy)?m_ItemSize.cy:pPage->m_ItemSize.cy);
					if (AlignLeft == m_nTabAlign)
					{
						rcItem.right = rcTitle.right;
						rcItem.left = rcItem.right - ((-1 == pPage->m_ItemSize.cx)?m_ItemSize.cx:pPage->m_ItemSize.cx);
					}
					else
					{
						rcItem.left = rcTitle.left;
						rcItem.right = rcItem.left + ((-1 == pPage->m_ItemSize.cx)?m_ItemSize.cx:pPage->m_ItemSize.cx);
					}
				}
			}
			break;
		}
		rcItem.IntersectRect(rcItem,rcTitle);
	} while (false);

	return rcItem;
}

CRect TabCtrl::GetTitleRect()
{
	CRect rcTitle;
	DV_GetClientRect(rcTitle);
	switch (m_nTabAlign)
	{
	case AlignTop:	 {rcTitle.bottom = rcTitle.top+m_ItemSize.cy;}break;
	case AlignBottom:{rcTitle.top = rcTitle.bottom-m_ItemSize.cy;}break;
	case AlignLeft:  {rcTitle.right = rcTitle.left+m_ItemSize.cx;}break;
	case AlignRight: {rcTitle.left = rcTitle.right-m_ItemSize.cx;}break;
	}
	return rcTitle;    
}

bool TabCtrl::DeleteItem(int nIndex, int iSelPage/*=0*/)
{
	bool bRet = false;
	do 
	{
		int iCount = (int)GetCount();
		if (nIndex<0||nIndex>=iCount)
		{
			break;
		}
		TabPage* pPage = NULL;
		GetObj(nIndex,pPage);
		if (NULL == pPage)
		{
			break;
		}

		TabCtrlDelingArgs Evt(this);
		Evt.m_pPage = pPage;
		DV_FireEvent(Evt);
		if (Evt.m_bCancel)
		{
			break;
		}

		RemoveObj(nIndex);

		TabCtrlDelArgs EndEvt(this);
		EndEvt.m_pPage = pPage;
		DV_FireEvent(EndEvt);// 此时还没有真正删除，只是移除了

		// 开始真实的删除
		DM_DestroyChildWnd(pPage);
		if (nIndex == m_iCurSelItem)// 删除的是当前选中页
		{
			if (iSelPage<0)
			{
				iSelPage = 0;
			}

			if (iSelPage>=(int)GetCount())
			{
				iSelPage = (int)GetCount()-1;
			}
			m_iCurSelItem = -1;
			SetCurSel(iSelPage);
		}
		else
		{
			if (m_iCurSelItem>nIndex) 
			{
				m_iCurSelItem--;
			}
		}

		CRect rcTitle = GetTitleRect();
		DM_InvalidateRect(rcTitle);
		Layout();

		bRet = true;
	} while (false);
	return bRet;
}

bool TabCtrl::DeleteItem(DUIWindow* pPage,int iSelPage/*=0*/)
{
	bool bRet = false;
	do 
	{
		if (NULL == pPage)
		{
			break;
		}
		int iCount = (int)GetCount();
		int nIndex = -1;
		for (int i=0; i<iCount; i++)
		{
			TabPage* pMyPage = NULL;
			GetObj(i,pMyPage);
			if (pMyPage == pPage)
			{
				nIndex = i;
				break;
			}
		}
		if (-1 == nIndex)
		{
			break;
		}
		bRet = DeleteItem(nIndex,iSelPage);
	} while (false);
	return bRet;
}

void TabCtrl::DeleteAllItems()
{
	int iCount = (int)GetCount();
	for (int i=0; i<iCount; i++)
	{
		TabPage* pPage = NULL;
		GetObj(i,pPage);
		if (pPage)
		{
			TabCtrlDelArgs EndEvt(this);
			EndEvt.m_pPage = pPage;
			EndEvt.m_bDelAll = true;
			DV_FireEvent(EndEvt);// 此时还没有真正删除，只是移除了

			DM_DestroyChildWnd(pPage);
		}
	}
	RemoveAll();
}

void TabCtrl::DrawItem(IDMCanvas* pCanvas,const CRect &rcItem,int iItem,DWORD dwState)
{
	do 
	{
		if (rcItem.IsRectEmpty())
		{
			break;
		}

		TabPage* pPage = NULL;
		GetObj(iItem,pPage);
		if (!pPage)
		{
			break;
		} 

		//0.先叠一次 
		if (m_pItemBgSkin)// 不同于pPage->m_pItemSkin
		{
			int iDraw = 0;
			m_pItemBgSkin->GetStates(iDraw);
			iDraw = iDraw>(int)dwState?(int)dwState:0;
			m_pItemBgSkin->Draw(pCanvas,rcItem,iDraw);
		}

		//1.绘制背景图
		if (pPage->m_pItemSkin)
		{
			int iDraw = 0;
			pPage->m_pItemSkin->GetStates(iDraw);
			iDraw = iDraw>(int)dwState?(int)dwState:0;
			CRect rcSkin = rcItem;
			CSize sz;
			pPage->m_pItemSkin->GetStateSize(sz);
			MeetRect(rcSkin,sz);
			pPage->m_pItemSkin->Draw(pCanvas,rcSkin,iDraw);
		}

		if (pPage->m_pIconSkin)
		{
			if (rcItem.Width()>TABITEM_LEN || (iItem != m_iCurSelItem))
			{
				int iDraw = 0;
				pPage->m_pIconSkin->GetStates(iDraw);
				iDraw = iDraw>(int)dwState?(int)dwState:0;
				CRect rcSkin = rcItem;
				CSize sz;
				pPage->m_pIconSkin->GetStateSize(sz);
				MeetRect(rcSkin,sz);
				if (rcSkin.left-rcItem.left>pPage->m_iIconOffset)
				{
					rcSkin.OffsetRect(rcItem.left-rcSkin.left+pPage->m_iIconOffset,0);
				}

				pPage->m_pIconSkin->Draw(pCanvas,rcSkin,iDraw);
			}
		}

		// 2.绘制文字
		DUIDrawEnviron paint;
		DWORD dwOld = pPage->DM_ModifyState(dwState,DUIWNDSTATE_FULL,false);
		pPage->DV_PushDrawEnviron(pCanvas,paint);
		if (-1!=m_TextPt.x&&-1!=m_TextPt.y)
		{
			pCanvas->TextOut(DMTR(pPage->m_strTitle),-1,rcItem.left+m_TextPt.x,rcItem.top+m_TextPt.y);
		}
		else// x,y至少有一个为-1
		{
			CRect rcText = rcItem;
			if (pPage->m_pCloseBtn)
			{
				rcText.right = pPage->m_pCloseBtn->m_rcWindow.left;
			}

			UINT uTextAlign = 0;
			m_pDUIXmlInfo->m_pStyle->GetTextAlign(uTextAlign);
			UINT uAlign = uTextAlign;
			if (m_TextPt.y!=-1)// 指定了Y偏移，X居中
			{
				rcText.top += m_TextPt.y;
				uAlign = uTextAlign&(DT_CENTER|DT_RIGHT|DT_SINGLELINE|DT_END_ELLIPSIS);
			}
			else if(m_TextPt.x!=-1)
			{
				rcText.left += m_TextPt.x;
				uAlign = uTextAlign&(DT_VCENTER|DT_BOTTOM|DT_SINGLELINE|DT_END_ELLIPSIS);
			}
			pCanvas->DrawText(DMTR(pPage->m_strTitle),-1,&rcText,uAlign);
		}

		pPage->DV_PopDrawEnviron(pCanvas,paint);
		pPage->DM_ModifyState(dwOld,0,false);

	} while (false);
}

void TabCtrl::Layout()
{
	do 
	{
		int iCount = (int)GetCount();
		if (iCount<=0)
		{
			break;
		}
		CRect rcTitle = GetTitleRect();
		if (rcTitle.IsRectEmpty())
		{
			break;
		}

		int nDeltaX = 0;	
		int nDeltaY = 0;
		int iDeltaCount = 0;// 在Page中设置了itemsize的个数,这部分不计入自动减小的范围
		for (int i=0;i<iCount;i++)
		{
			TabPage *pPage = NULL;
			GetObj(i,pPage);
			if (pPage)
			{
				if (-1 != pPage->m_ItemSize.cx)
				{
					nDeltaX += pPage->m_ItemSize.cx;
					nDeltaY += pPage->m_ItemSize.cy;
					iDeltaCount ++;
				}
			} 
		}
		if (AlignTop == m_nTabAlign || AlignBottom == m_nTabAlign)
		{
			int nAllWid = (rcTitle.Width()-m_iItemSpace*(iCount-1)-m_iFirstItemOffset-m_iLastItemOffset - nDeltaX);
			if (0 == m_OrgItemSize.cx||0>=nAllWid||iDeltaCount>=iCount)
			{
				break;
			}
			int nWid = nAllWid/(iCount-iDeltaCount);
			if (nWid > m_OrgItemSize.cx)
			{
				m_ItemSize = m_OrgItemSize;
			}
			else
			{
				m_ItemSize = m_OrgItemSize;
				m_ItemSize.cx = nWid;
			}
		}
		else if (AlignLeft == m_nTabAlign || AlignRight == m_nTabAlign)
		{
			int nAllHei = (rcTitle.Height()-m_iItemSpace*(iCount-1)-m_iFirstItemOffset-m_iLastItemOffset - nDeltaY);
			if (0 == m_OrgItemSize.cy||0>=nAllHei||iDeltaCount>=iCount)
			{
				break;
			}
			int nHei = nAllHei/(iCount-iDeltaCount);
			if (nHei > m_OrgItemSize.cy)
			{
				m_ItemSize = m_OrgItemSize;
			}
			else
			{
				m_ItemSize = m_OrgItemSize;
				m_ItemSize.cy = nHei;
			}
		}

		for (int i=0; i<iCount; i++)
		{
			TabPage *pPage = NULL;
			GetObj(i,pPage);
			if (pPage)
			{
				pPage->Layout();
			}
		}	
		DM_Invalidate();
	} while (false);
}

int TabCtrl::HitTest(CPoint pt)
{
	int iCount = (int)GetCount();
	for (int i=0; i<iCount; i++)
	{
		CRect rcItem = GetItemRect(i);
		if (rcItem.PtInRect(pt))
		{
			return i;
		}
	}
	return -1;
}

void TabCtrl::DM_OnPaint(IDMCanvas* pCanvas)
{
	do 
	{
		DUIDrawEnviron painter;
		DV_PushDrawEnviron(pCanvas,painter);

		CRect rcTitle = GetTitleRect();
		pCanvas->PushClip(&rcTitle,RGN_AND);

		if (m_pMainBgSkin)
		{
			m_pMainBgSkin->Draw(pCanvas, rcTitle, DUIWNDSTATE_Normal);
		}

		DWORD dwState = DUIWNDSTATE_Normal;
		int iCount = (int)GetCount();
		for (int i=0; i<iCount; i++)
		{
			dwState = DUIWNDSTATE_Normal;
			if (i== m_iCurSelItem)
			{
				dwState = DUIWNDSTATE_PushDown;
			}
			else if (i == m_iHoverItem)
			{
				dwState = DUIWNDSTATE_Hover;
			}
			CRect rcItem = GetItemRect((int)i); 
			DrawItem(pCanvas,rcItem,i,dwState);
			if (i!=0 && i<iCount-1 && m_iItemSpace>0 && m_pSpaceSkin)// 不是第一个不是最后一个，根据大厅特殊要求
			{   
				CRect rcSpace = rcItem;
				rcSpace.left = rcItem.right;
				rcSpace.right = rcSpace.left + m_iItemSpace;
				m_pSpaceSkin->Draw(pCanvas,rcSpace,0); 
			}
		}
		pCanvas->PopClip();
		DV_PopDrawEnviron(pCanvas,painter);
	} while (false);
}

void TabCtrl::OnDestroy()
{
	DeleteAllItems();
}

void TabCtrl::OnLButtonDown(UINT nFlags, CPoint point)
{
	m_bDrag = false;
	if (m_pDUIXmlInfo->m_bDrag)// 只允许在大标签列内并且不在标签上，才能拖动
	{
		CRect rcTitle = GetTitleRect();
		if (rcTitle.PtInRect(point)// 在大标签列范围内
			&&-1 == HitTest(point))// 不在任意一个标签上
		{
			m_bDrag = true;
		}
	} 
	DMAutoResetT<bool> bAutoDrag(&m_pDUIXmlInfo->m_bDrag,m_bDrag);
	DUIWindow::OnLButtonDown(nFlags,point);
	int iClickItem = HitTest(point);
	if (iClickItem != m_iCurSelItem)
	{
		SetCurSel(iClickItem);
	}
}

void TabCtrl::OnLButtonUp(UINT nFlags,CPoint pt)
{
	DMAutoResetT<bool> bAutoDrag(&m_pDUIXmlInfo->m_bDrag,m_bDrag);
	DUIWindow::OnLButtonUp(nFlags,pt);;
	m_bDrag = false;
}

void TabCtrl::OnMouseMove(UINT nFlags, CPoint point)
{
	int iOldHoverItem = m_iHoverItem;
	m_iHoverItem = HitTest(point);
	if (m_iHoverItem != iOldHoverItem) // 更新状态
	{
		CRect rcItem = GetItemRect(iOldHoverItem);
		DM_InvalidateRect(rcItem);

		if (-1 != m_iHoverItem)
		{
			rcItem = GetItemRect(m_iHoverItem);
			DM_InvalidateRect(rcItem);
		}
	}
}

void TabCtrl::OnMouseLeave()
{
	OnMouseMove(0,CPoint(-1,-1));
}

void TabCtrl::OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags)
{
#if 0
	switch (nChar)
	{
	case VK_LEFT:
	case VK_UP:
		{
			if (!SetCurSel(m_iCurSelItem-1))// 轮询到最右边了
			{
				SetCurSel((int)GetCount()-1);
			}
		}
		break;

	case VK_RIGHT:
	case VK_DOWN:
		{
			if (!SetCurSel(m_iCurSelItem+1))// 轮询到最左边了
			{
				SetCurSel(0);
			}
		}
		break;
	case VK_HOME:
		SetCurSel(0);
		break;
	case VK_END:
		SetCurSel((int)GetCount()-1);
	default:
		break;
	}
#endif
}

void TabCtrl::OnSize(UINT nType, CSize size)
{
	SetMsgHandled(FALSE);
	Layout();
}

DMCode TabCtrl::DV_CreateChildWnds(DMXmlNode &XmlNode)
{
	DMCode iErr = DM_ECODE_FAIL;
	do 
	{
		// 加入子Page
		for (DMXmlNode XmlChildNode = XmlNode.FirstChild(); XmlChildNode.IsValid(); XmlChildNode=XmlChildNode.NextSibling())
		{
			InsertItem(XmlChildNode,-1, true);
		}

		if (-1 == m_iCurSelItem
			||m_iCurSelItem>=(int)GetCount())
		{
			m_iCurSelItem = 0;
		}
		if (0 == (int)GetCount())
		{
			m_iCurSelItem = -1;
		}

		if (-1 != m_iCurSelItem)
		{
			TabPage* pPage = NULL;
			GetObj(m_iCurSelItem,pPage);
			if (pPage)
			{
				pPage->DM_SendMessage(WM_SHOWWINDOW,TRUE,NormalShow);
			}
		}

		iErr = DM_ECODE_OK;
	} while (false);
	return iErr;
} 

DMCode TabCtrl::DV_GetChildMeasureLayout(LPRECT lpRect)
{
	DV_GetClientRect(lpRect);
	switch (m_nTabAlign)
	{
	case AlignLeft:  lpRect->left+= m_ItemSize.cx;	break;
	case AlignRight: lpRect->right-=m_ItemSize.cx;	break;
	case AlignTop:   lpRect->top += m_ItemSize.cy;	break;
	case AlignBottom:lpRect->bottom -= m_ItemSize.cy;break;
	}

	return DM_ECODE_OK;
}

DMCode TabCtrl::DV_OnUpdateToolTip(CPoint pt, DMToolTipInfo &tipInfo)
{
	DMCode iErr = DM_ECODE_FAIL;
	do 
	{
		int iHoverItem = HitTest(pt);
		if (-1==iHoverItem)
		{
			break;
		}

		TabPage* pPage = NULL;
		GetObj(iHoverItem,pPage);
		if (NULL == pPage)
		{
			break;
		}
		if (pPage->m_pDUIXmlInfo->m_strTooltipText.IsEmpty())
		{
			break;
		}
		CRect rcItem = GetItemRect(iHoverItem);

		tipInfo.hDUIWnd  = m_hDUIWnd;
		tipInfo.rcTarget = rcItem;
		tipInfo.strTip    = DMTR(pPage->m_pDUIXmlInfo->m_strTooltipText);
		tipInfo.iDelayTime =  pPage->m_pDUIXmlInfo->m_iTooltipDelayTime;
		tipInfo.iSpanTime  =  pPage->m_pDUIXmlInfo->m_iTooltipSpanTime;
		iErr = DM_ECODE_OK;
	} while (false);
	return iErr;
}

DMCode TabCtrl::OnItemSize(LPCWSTR pszValue, bool bLoadXml)
{
	dm_parsesize(pszValue,m_OrgItemSize);
	m_ItemSize = m_OrgItemSize;
	return DM_ECODE_OK;
}

DMCode TabCtrl::OnCurSel(LPCWSTR pszValue, bool bLoadXml)
{
	int nCurSel = m_iCurSelItem;
	dm_parseint(pszValue,nCurSel);
	if (!bLoadXml)
	{
		if (nCurSel!=m_iCurSelItem)
		{
			SetCurSel(nCurSel);
		}
	}
	else
	{
		m_iCurSelItem = nCurSel;
	}
	return DM_ECODE_OK;
}