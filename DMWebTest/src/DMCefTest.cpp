#include "StdAfx.h"
#include "MainWnd.h"

CMainWnd* g_pMainWnd = NULL;
int APIENTRY _tWinMain(HINSTANCE hInstance,
					   HINSTANCE hPrevInstance,
					   LPTSTR    lpCmdLine,
					   int       nCmdShow)
{   
	DMApp theApp(hInstance);
#ifdef _DEBUG
	theApp.LoadPlugin(L"PluginWeb_d.dll");								    // 也可使用LoadPlugins来批量加载，如果不在同目录，请使用绝对路径
#else
	theApp.LoadPlugin(L"PluginWeb.dll");
#endif
	theApp.Register(DMRegHelperT<TabButton>(),true);
	theApp.Register(DMRegHelperT<TabPage>(),true);
	theApp.Register(DMRegHelperT<TabCtrl>(),true);
	theApp.LoadResPack((WPARAM)(L"MyRes"),NULL,NULL);						// 路径总是相对于生成目录
	theApp.InitGlobal();													// 初始化指定的全局skin、style、默认字体

	DMSmartPtrT<CMainWnd> pMainWnd;
	pMainWnd.Attach(new CMainWnd());
	g_pMainWnd = pMainWnd;
	pMainWnd->DM_CreateWindow(L"main",0,0,0,0,NULL,false);					// 创建主窗口

	pMainWnd->SendMessage(WM_INITDIALOG);
	pMainWnd->CenterWindow();
	pMainWnd->ShowWindow(SW_SHOW);
	theApp.Run(pMainWnd->GetSafeHwnd());								    // 运行当前线程的消息循环，并加入消息队列管理中

	return (int) 0;
};