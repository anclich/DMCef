#include "StdAfx.h"
#include "WebEvent.h"

HRESULT WebEvent::BeforeNavigate2(DUIWND hWnd, DMIN IDispatch *pDisp, DMIN wchar_t *pUrl,DMIN int Flags,DMIN wchar_t *pTargetFrameName,DMIN VARIANT *pPostData,DMIN wchar_t *pHeaders,DMINOUT VARIANT_BOOL *bCancel)
{
	*bCancel = VARIANT_FALSE;
	return S_OK;
}

HRESULT WebEvent::NavigateComplete2(DUIWND hWnd,DMIN IDispatch *pDisp,DMIN wchar_t *pUrl)
{// CefDisplayHandler::OnAddressChange
	if (g_pMainWnd && g_pMainWnd->IsWindow())
	{
		g_pMainWnd->m_pUrlEdit->SetWindowText(SAFE_STR(pUrl));
	}
	return S_OK;
}

HRESULT WebEvent::TitleChange(DUIWND hWnd, DMIN wchar_t *pText)
{
	if (g_pMainWnd && g_pMainWnd->IsWindow() && hWnd)
	{
		DUIWindow* pWeb = g_pDMApp->FindDUIWnd(hWnd);
		if (pWeb)
		{
			TabPagePtr pPage = dynamic_cast<TabPage*>(pWeb->DM_GetWindow(GDW_PARENT));
			if (pPage)
			{
				pPage->SetAttribute(DMAttr::TabPageAttr::STRING_title,SAFE_STR(pText));
			}
		}
	}
	return S_OK;
}

#define TAB_LOADING L"加载中,马上就好..."
HRESULT WebEvent::NewWindow3(DUIWND hWnd, DMINOUT IDispatch **pDisp,DMINOUT VARIANT_BOOL *bCancel,DMIN DWORD dwFlags,DMIN wchar_t *pUrlContext,DMIN wchar_t *pUrl)
{
	if (g_pMainWnd && g_pMainWnd->IsWindow())
	{
		CStringW strWXml;
		strWXml.Format(L"<TabPage title=\"%s\" font=\"face:宋体,size:12,weight:400\" clrtext=\"pbgra(7b,7b,7b,ff)\" clrtexthover=\"pbgra(00,5e,ff,ff)\" clrtextpush=\"pbgra(ff,ff,ff,ff)\" iconoffset=\"25\" iconskin=\"subtab_pageicon\" closeskin=\"tab_close\">"\
			L"<cef name=\"cef\" pos=\"0,0,-0,-0\" refreshkey=\"f5\" url=\"%s\" ncmargin=\"1,1,1,1\" clrnc=\"pbgra(00,00,00,ff)\"/>"\
			L"</TabPage>",L"新标签页",SAFE_STR(pUrl));
		CStringA strXml = DMW2A(strWXml,CP_UTF8);
		DMXmlDocument Doc;
		Doc.LoadFromBuffer((const PVOID)(LPCSTR)strXml,strXml.GetLength());
		DMXmlNode XmlNode = Doc.Root();
		int iInsert = g_pMainWnd->m_pTabMain->InsertItem(XmlNode,-1);
		if (-1 != iInsert)
		{
			TabPagePtr pPage = g_pMainWnd->m_pTabMain->GetObj(iInsert);
			IDUIWeb* pWeb = pPage->FindChildByNameT<IDUIWeb>(L"cef");
			if (pWeb)
			{
				pWeb->SetEvtHandler(g_pMainWnd->m_pWebEvent);
			}
			g_pMainWnd->m_pTabMain->SetCurSel(iInsert);
			*bCancel = VARIANT_TRUE;
		}
	}
	return S_OK;
}
