#include "StdAfx.h"
#include "JSFun.h"
struct JSFunction
{
	CStringW                        rgszName;																		///< JS通过名字得到dispIdMember
	HRESULT (JSFun::*FUN)(DUIWND, DISPID, const std::vector<std::wstring>&, std::wstring*);							///< JS调用的函数
};

/// <summary>
///		JS交互函数列表
/// </summary>
#define DISPID_RENDER_WEB_EXTERN_START				0x300
#define DIPSID_RENDER_WEB_EXTERN_END				0x5FF
static JSFunction g_JsWebExternFun[] = 
{
	{L"JsCallCpp",					&JSFun::JsCallCpp},								
};

static JSFunction* GetRenderJSFunction(DISPID dispIdMember)
{
	if (dispIdMember >= DISPID_RENDER_WEB_EXTERN_START && dispIdMember <= DIPSID_RENDER_WEB_EXTERN_END)
	{
		return &g_JsWebExternFun[dispIdMember-DISPID_RENDER_WEB_EXTERN_START];
	}
	return NULL;
}


//--------------------------------------------------------------------------------------------------
DMExternalHandler::DMExternalHandler()
{
	m_spJSFun.Attach(new JSFun);
}

void DMExternalHandler::GetAllFunNames(DM::CArray<CStringW>& NameArray)
{
	NameArray.RemoveAll();
	for(int i = 0; i <sizeof(g_JsWebExternFun)/sizeof(JSFunction); ++i)   //枚举所有函数名
	{
		NameArray.Add(g_JsWebExternFun[i].rgszName);
	}
}

void DMExternalHandler::CallFunction(DUIWND hDUIWnd, CStringW strName, DM::CArray<CStringW>&strArgs, CStringW& strResult)
{
	DISPID DispID = 0;
	std::wstring cName = strName;
	if (SUCCEEDED(JSFun::GetIDsOfJSNames(hDUIWnd,cName,&DispID)))
	{
		JSFunction* pFun = GetRenderJSFunction(DispID);
		if (pFun)
		{
			std::vector<std::wstring> args;
			std::wstring stringResult;
			for (int i=0; i<(int)strArgs.GetCount(); i++)
			{
				args.push_back(std::wstring(strArgs[i]));
			}
			(m_spJSFun->*(pFun->FUN))(hDUIWnd, DispID, args, &stringResult);  
			strResult = stringResult.c_str();
		}
	}
}

//--------------------------------------------------------------------------------------
HRESULT JSFun::GetIDsOfJSNames(DUIWND hDUIWnd, std::wstring& strName, DISPID* rgDispId)
{
	for(int i = 0; i <sizeof(g_JsWebExternFun)/sizeof(JSFunction) ; ++i)   ///<  枚举所有函数  
	{  
		CStringW strMyName(strName.c_str());
		if (0 == strMyName.CompareNoCase(g_JsWebExternFun[i].rgszName))
		{
			*rgDispId = i+DISPID_RENDER_WEB_EXTERN_START;
			return S_OK;  
		}
	}  
	return ResultFromScode(DISP_E_UNKNOWNNAME);  
}		

HRESULT JSFun::JsCallCpp(DUIWND hDUIWnd, DISPID dispIdMember, const std::vector<std::wstring>&strArgs, std::wstring* pResult)
{
	IDUIWeb* pWeb = dynamic_cast<IDUIWeb*>(g_pDMApp->FindDUIWnd(hDUIWnd));
	if (pWeb)
	{
		CStringW strCookie = L"4366Ver=3.0.0.4;4366_account=hgy413_4366_account;4366_info=hgy413_4366_info;account_token=hgy413_account_token;accountinfo=accountinfox1tx1sejEFBRpEPaJBUptL-cIS_byaNGXAAAAAADAAAAAAAAAA4ANTguMjE1LjEzOC4xMTUEADU2Mjk=;accounttype=0;game_info=5A5BA4D81FE9CA2124DA6C9F88244F8B0BC36460976DC3529D6209B91D457BC4CC77192743D03C9CBE898F1A04C46A48;game_nickname=hgy413;mckey=81edbef3d2591373c004b97e776d6fc7ee654bc5a940ac8a99c00c6491c37a3b1e68b27e0e1ae95eb8f3ddb8343fbe78;nickname_4366=hgy413;oauthCookie=F950D7F7F01D4587D77D46FA72867B91A7F14C24C2CD1692995255D6F088A822F4A82DA3401E107B9D9A5683176A0B7064D0BEFEF70B4CF35638FEAB0450499DF581722CC3F0DF98DE0B34851390C3DBBFCBE34DEDFCD676916C9989F74647EDDEA6CE900228E790C534548EB41C610A69C63FD041B7D8D726E76FA73C4ED6BDC7FA23B2CBEFDC1EEBFB6D96AE65FF6F8033527A69B10169C5EDB2BCC007DF423EC1DCB314AFF3C3222E975EDE215B187B15197504A435FC65F327A838B43DAF6D07633A4D4D1E71FDBA724D3733926C8E1AD549763413595442B6915763EB35FAC4F23F21E9BDD9B5C2FB68698F99D1AD1B946C27C340FEACC0EEC34C1D9D5337C7A3385172164F0855B6DC2A802313;osinfo=69dabd496adeadeca8abd843c814bc0dad3d1812;parnerInfo=E692F2FE8B4B83351F9A2A2664E892E7A9CFCFAED10072E8B1F8444F49EE8FF3;partner_uid=24272981;password=bf789bb38506581594f3f5f1d4da0c2b14b3517a;signature=A9HmiWsJRuz2yLxqhW9htFKdQqo=;stoken=-1;thirdCookie=00BE0A2818071F2CAE4BAB582A6DDC9AF625C194CD0378D10E6AE21186FC03AA4F43702929C61F39964A36E14E162BE4887A638F9EF489ABA4D135890B8849BB;uc_states=1;username=game_vgijs8q__6;yyuid=1703418630;";
		if (g_pMainWnd && g_pMainWnd->m_pDMWebApp)
		{
			g_pMainWnd->m_pDMWebApp->SetCookieForUrl(L"http://f2e.yy.com/1709/m_368818052777.html?3",strCookie,NULL,true);
		}
	}

	if (pResult)
	{
		pResult->assign(L"设置cookie成功,请点击显示cookie按钮");
	}
	return S_OK;
}
