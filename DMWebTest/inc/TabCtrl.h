// ----------------------------------------------------------------
// Copyright (c)  
// All rights reserved.
// 
// File name:	TabCtrl.h
// File mark:   
// File summary:copy from DUITabCtrl.h
// Author:		guoyouhuang
// Edition:     1.0
// Create date: 2017-9-15
// ----------------------------------------------------------------
#pragma once
#include "DUIButton.h"
#pragma region TabData
enum UIEVT_ID
{
	DMEVT_TABCTRL_DELING    =   30000,
	DMEVT_TABCTRL_DEL,
	DMEVT_TABCTRL_SELCHANGING,
	DMEVT_TABCTRL_SELCHANGED,
};

class TabPage;
/// <summary>
///		Tab页准备删除事件
/// </summary>
class TabCtrlDelingArgs : public DMEventArgs
{
public:
	TabCtrlDelingArgs(DUIWindow* pWnd):DMEventArgs(pWnd),m_bCancel(false),m_pPage(NULL){}
	enum{EventID=DMEVT_TABCTRL_DELING};
	virtual UINT GetEventID(){return EventID;}
	LPCSTR GetEventName(){return EVEIDNAME(DMEVT_TABCTRL_DELING);}
	TabPage*			  m_pPage;			  ///< 当前准备删除的项
	bool                  m_bCancel;          ///< 是否取消删除
};

/// <summary>
///		Tab页删除后事件
/// </summary>
class TabCtrlDelArgs : public DMEventArgs
{
public:
	TabCtrlDelArgs(DUIWindow* pWnd):DMEventArgs(pWnd),m_pPage(NULL),m_bDelAll(false){}
	enum{EventID=DMEVT_TABCTRL_DEL};
	virtual UINT GetEventID(){return EventID;}
	LPCSTR GetEventName(){return EVEIDNAME(DMEVT_TABCTRL_DEL);}
	TabPage*            m_pPage;            ///< 当前已删除的项
	bool			    m_bDelAll;          ///< 在调用DeleteAllItems时设置为true
};

/// <summary>
///		Tab页切换中事件
/// </summary>
class TabCtrlSelChangingArgs:public DMEventArgs
{
public:
	TabCtrlSelChangingArgs(DUIWindow *pWnd):DMEventArgs(pWnd),m_bCancel(false){}
	enum{EventID=DMEVT_TABCTRL_SELCHANGING};
	virtual UINT GetEventID(){return EventID;}
	LPCSTR GetEventName(){return EVEIDNAME(DMEVT_TABCTRL_SELCHANGING);}
	int					m_iOldSel;			///<先前选中页
	int				    m_iNewSel;			///<当前选中页
	bool				m_bCancel;          ///<是否取消当前切换页的操作
};

/// <summary>
///		Tab页切换后事件
/// </summary>
class TabCtrlSelChangedArgs:public DMEventArgs
{
public:
	TabCtrlSelChangedArgs(DUIWindow *pWnd):DMEventArgs(pWnd){}
	enum{EventID=DMEVT_TABCTRL_SELCHANGED};
	virtual UINT GetEventID(){return EventID;}
	LPCSTR GetEventName(){return EVEIDNAME(DMEVT_TABCTRL_SELCHANGED);}
	int					m_iOldSel;			///<先前选中页
	int					m_iNewSel;			///<当前选中页
};
#pragma endregion

//-----------------------------------------------------------------------------------------------
namespace DMAttr
{
	/// <summary>
	///		<see cref="DM::TabPage"/>的xml属性定义
	/// </summary>
	class TabPageAttr:public DUIWindowAttr
	{
	public:
		static wchar_t* STRING_title;											   ///< tab页的标题,示例：title="标签1"
		static wchar_t* SKIN_itemskin;                                             ///< 标签项的皮肤，示例: itemskin="skin"
		static wchar_t* SKIN_iconskin;                                             ///< 标签项的icon，靠左绘制,示例:iconskin="skin"
		static wchar_t* INT_iconoffset;											   ///< icon相对于边框的偏移，示例:iconoffset="10"
		static wchar_t* SKIN_closeskin;                                            ///< 标签的关闭按钮图标,如果存在,则创建GPTabButton,示例:closeskin="skin"
		static wchar_t* SIZE_itemsize;                                             ///< 标签项的大小，示例:itemsize="78,30"
	};
	DMAttrValueInit(TabPageAttr,STRING_title)DMAttrValueInit(TabPageAttr,SKIN_itemskin)DMAttrValueInit(TabPageAttr,SKIN_iconskin)DMAttrValueInit(TabPageAttr,INT_iconoffset)DMAttrValueInit(TabPageAttr,SKIN_closeskin)DMAttrValueInit(TabPageAttr,SIZE_itemsize)

	/// <summary>
	///		<see cref="DM::TabCtrl"/>的xml属性定义
	/// </summary>
	class TabCtrlAttr:public DUIWindowAttr
	{  
	public:
		static wchar_t* POINT_textpoint;                                           ///< 相对于主标签区域的左上角偏移，默认为-1，-1，其中-1表示此方向居中，示例textpoint="-1,-1"
		static wchar_t* OPTION_tabalign;										   ///< tab页的排列方式,分为top、left、right、buttom,示例:tabalign="top"
		static wchar_t* SKIN_mainbgskin;							               ///< 整个item(标签列表)所在区的背景,不包括page区,示例:mainbgskin="TabCtrl_Item"
		static wchar_t* SKIN_itembgskin;										   ///< 每个item(标签)的skin，即在GTabPageAttr::SKIN_itemskin之下先绘一次,用于所有item的hover-sel为同一状态时简化截图,示例:itembgskin="skin"
		static wchar_t* SKIN_spaceskin;											   ///< 两个item之间的skin，绘制在间隔之间，示例:spaceskin="skin"
		static wchar_t* INT_firstitemoffset;                                       ///< 第一个Item(标签)的相对于边框的偏移，示例:firstitemoffset="10"
		static wchar_t* INT_lastitemoffset;                                        ///< 最后一个Item(标签)的相对于右边框的偏移，示例:lastitemoffset="10"
		static wchar_t* INT_itemspace;                                             ///< Item之间的间距，示例:itemspace="10"
		static wchar_t* INT_cursel;                                                ///< 当前选中项, 示例:cursel="1"
		static wchar_t* SIZE_itemsize;                                             ///< Item(标签)项的大小，示例:itemsize="78,30"
	};
	DMAttrValueInit(TabCtrlAttr,POINT_textpoint)DMAttrValueInit(TabCtrlAttr,OPTION_tabalign)DMAttrValueInit(TabCtrlAttr,SKIN_mainbgskin)DMAttrValueInit(TabCtrlAttr,SKIN_itembgskin)DMAttrValueInit(TabCtrlAttr,SKIN_spaceskin)DMAttrValueInit(TabCtrlAttr,INT_firstitemoffset)
	DMAttrValueInit(TabCtrlAttr,INT_lastitemoffset)DMAttrValueInit(TabCtrlAttr,INT_itemspace)DMAttrValueInit(TabCtrlAttr,INT_cursel)DMAttrValueInit(TabCtrlAttr,SIZE_itemsize)
}//namespace DMAttr


//-----------------------------------------------------------------------------------------------
class TabCtrl;
class TabPage;

/// <summary>
///		tab标签上关闭按钮的实现
/// </summary>
class TabButton : public DUIButton
{
	DMDECLARE_CLASS_NAME(TabButton,L"TabButton",DMREG_Window);
public:
	TabButton();
	void SetData(TabCtrl* pParent,TabPage* pPage);

public:
	DM_BEGIN_MSG_MAP()
		MSG_WM_MOUSEMOVE(OnMouseMove)
		MSG_WM_LBUTTONUP(OnLButtonUp)
	DM_END_MSG_MAP()
public:
	void OnMouseMove(UINT nFlags,CPoint pt);
	void OnLButtonUp(UINT nFlags,CPoint pt);

public:
	TabCtrl*                                           m_pParent;					 ///< 父TabCtrl
	TabPage*										   m_pPage;						 ///< 对应TabPage页
};		


/// <summary>
///		TabPage的特定实现，属性：<see cref="DMAttr::TabPageAttr"/>
/// </summary>
class TabPage : public DUIWindow
{
	DMDECLARE_CLASS_NAME(TabPage,L"TabPage",DMREG_Window);
public:
	TabPage();
	void SetParant(TabCtrl* pParent);
	void Layout();
	void ShowOrHideBtn();

public:
	DM_BEGIN_MSG_MAP()
		MSG_WM_DESTROY(OnDestroy)
		DM_END_MSG_MAP()
public:
	void OnDestroy();

public:
	DM_BEGIN_ATTRIBUTES()
		DM_CUSTOM_ATTRIBUTE(DMAttr::TabPageAttr::STRING_title, OnItemTitle)
		DM_CUSTOM_ATTRIBUTE(DMAttr::TabPageAttr::SKIN_itemskin, OnItemSkin)
		DM_CUSTOM_ATTRIBUTE(DMAttr::TabPageAttr::SKIN_iconskin, OnItemIconSkin)
		DM_CUSTOM_ATTRIBUTE(DMAttr::TabPageAttr::SKIN_closeskin, OnItemCloseSkin)
		DM_CUSTOM_ATTRIBUTE(DMAttr::TabPageAttr::SIZE_itemsize,OnItemSize)
		DM_INT_ATTRIBUTE(DMAttr::TabPageAttr::INT_iconoffset, m_iIconOffset,DM_ECODE_NOXMLLOADREFRESH)
		DM_END_ATTRIBUTES()
public:
	DMCode OnItemTitle(LPCWSTR pszValue, bool bLoadXml);
	DMCode OnItemSkin(LPCWSTR pszValue, bool bLoadXml);
	DMCode OnItemIconSkin(LPCWSTR pszValue, bool bLoadXml);
	DMCode OnItemCloseSkin(LPCWSTR pszValue, bool bLoadXml);
	DMCode OnItemSize(LPCWSTR pszValue, bool bLoadXml);

public:
	CStringW											m_strTitle;				 ///< ITEM标题
	DMSmartPtrT<IDMSkin>                                m_pItemSkin;			 ///< 标签皮肤
	DMSmartPtrT<IDMSkin>                                m_pIconSkin;			 ///< icon皮肤
	DMSmartPtrT<IDMSkin>                                m_pCloseSkin;			 ///< 关闭皮肤
	CStringW                                            m_strCloseSkin;			 ///< 关闭皮肤名称
	CSize												m_ItemSize;				 ///< ITEM大小
	TabCtrl*											m_pParent;				 ///< 父窗口
	TabButton*											m_pCloseBtn;			 ///< 父窗口的子按钮,设置在标签上,互相包含
	int                                                 m_iIconOffset;			 ///< Icon相对左边的偏移
};
typedef TabPage* TabPagePtr;


/// <summary>
///		 TabCtrl的实现，属性：<see cref="DMAttr::TabCtrlAttr"/>
/// </summary>
class TabCtrl : public DUIWindow, public DMArrayT<TabPagePtr>
{
	DMDECLARE_CLASS_NAME(TabCtrl,L"TabCtrl",DMREG_Window);
public:
	TabCtrl();

	//---------------------------------------------------
	// Function Des: 对外接口
	//---------------------------------------------------
	int InsertItem(DMXmlNode &XmlNode, int iInsert=-1, bool bLoadXml=false);
	int GetCurSel();
	bool SetCurSel(DUIWindow* pPage);
	bool SetCurSel(int nIndex);
	bool SetCurSel(LPCWSTR lpszTitle);
	bool SetItemTitle(int nIndex, LPCWSTR lpszTitle);
	CRect GetItemRect(DUIWindow* pPage);
	CRect GetItemRect(int nIndex);											///< 取得某个标签所在的矩形，不包括page部分	
	CRect GetTitleRect();													///< 所有标签所在的大矩形，另一部分就是所有page所在的大矩形		
	bool DeleteItem(int nIndex, int iSelPage=0);							///< 如果删除的是当前选中页，通过iSelPage指定新的选中页
	bool DeleteItem(DUIWindow* pPage,int iSelPage=0);
	void DeleteAllItems();

public:
	void DrawItem(IDMCanvas* pCanvas,const CRect &rcItem,int iItem,DWORD dwState);
	void Layout();
	int HitTest(CPoint pt);

public:
	DM_BEGIN_MSG_MAP()
		DM_MSG_WM_PAINT(DM_OnPaint)
		MSG_WM_DESTROY(OnDestroy)
		MSG_WM_LBUTTONDOWN(OnLButtonDown)
		MSG_WM_LBUTTONUP(OnLButtonUp)
		MSG_WM_MOUSEMOVE(OnMouseMove)
		MSG_WM_MOUSELEAVE(OnMouseLeave)
		MSG_WM_KEYDOWN(OnKeyDown)
		MSG_WM_SIZE(OnSize)
	DM_END_MSG_MAP()
public:
	//---------------------------------------------------
	// Function Des: DUI的消息分发系列函数
	//---------------------------------------------------
	void DM_OnPaint(IDMCanvas* pCanvas);
	void OnDestroy();
	void OnLButtonDown(UINT nFlags, CPoint point);
	void OnLButtonUp(UINT nFlags,CPoint pt);
	void OnMouseMove(UINT nFlags, CPoint point);
	void OnMouseLeave();
	void OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags);
	void OnSize(UINT nType, CSize size);

public:   
	//--------- ------------------------------------------
	// Function Des: 重载
	//---------------------------------------------------
	DMCode DV_CreateChildWnds(DMXmlNode &XmlNode);
	UINT DV_OnGetDlgCode(){return DMDLGC_WANTARROWS;};	
	DMCode DV_GetChildMeasureLayout(LPRECT lpRect);
	DMCode DV_OnUpdateToolTip(CPoint pt, DMToolTipInfo &tipInfo);

public:
	enum{AlignTop,AlignLeft,AlignBottom,AlignRight};
	DM_BEGIN_ATTRIBUTES()
		DM_POINT_ATTRIBUTE(DMAttr::TabCtrlAttr::POINT_textpoint,m_TextPt, DM_ECODE_NOXMLLOADREFRESH)
		DM_SKINPTR_ATTRIBUTE(DMAttr::TabCtrlAttr::SKIN_mainbgskin, m_pMainBgSkin, DM_ECODE_NOXMLLOADREFRESH)
		DM_SKINPTR_ATTRIBUTE(DMAttr::TabCtrlAttr::SKIN_itembgskin, m_pItemBgSkin, DM_ECODE_NOXMLLOADREFRESH)
		DM_SKINPTR_ATTRIBUTE(DMAttr::TabCtrlAttr::SKIN_spaceskin, m_pSpaceSkin, DM_ECODE_NOXMLLOADREFRESH)
		DM_INT_ATTRIBUTE(DMAttr::TabCtrlAttr::INT_firstitemoffset, m_iFirstItemOffset, DM_ECODE_NOXMLLOADREFRESH)
		DM_INT_ATTRIBUTE(DMAttr::TabCtrlAttr::INT_lastitemoffset, m_iLastItemOffset, DM_ECODE_NOXMLLOADREFRESH)
		DM_INT_ATTRIBUTE(DMAttr::TabCtrlAttr::INT_itemspace, m_iItemSpace,DM_ECODE_NOXMLLOADREFRESH)
		DM_CUSTOM_ATTRIBUTE(DMAttr::TabCtrlAttr::INT_cursel, OnCurSel)
		DM_CUSTOM_ATTRIBUTE(DMAttr::TabCtrlAttr::SIZE_itemsize,OnItemSize)
		DM_ENUM_BEGIN(DMAttr::TabCtrlAttr::OPTION_tabalign, int, DM_ECODE_NOXMLRELAYOUT)
		DM_ENUM_VALUE(L"top", AlignTop)
		DM_ENUM_VALUE(L"left", AlignLeft)
		DM_ENUM_VALUE(L"right", AlignRight)
		DM_ENUM_VALUE(L"bottom", AlignBottom)
		DM_ENUM_END(m_nTabAlign)
		DM_END_ATTRIBUTES()
public:
	DMCode OnItemSize(LPCWSTR pszValue, bool bLoadXml);
	DMCode OnCurSel(LPCWSTR pszValue, bool bLoadXml);

public:
	DMSmartPtrT<IDMSkin>					m_pMainBgSkin;			 ///< 整个item所在区的背景,不包括page区
	DMSmartPtrT<IDMSkin>					m_pItemBgSkin;			 ///< 在GTabPage绘完后再叠一次,比如tab的hover-sel变化全是相同的,减少截图量
	DMSmartPtrT<IDMSkin>                    m_pSpaceSkin;            ///< Item页面间距时绘制
	int										m_iHoverItem;		     ///< 当前hover的item 
	int										m_iCurSelItem;		     ///< 当前选中的item
	int										m_iItemSpace;		     ///< Item页面间距  
	int										m_iFirstItemOffset;		 ///< 第一个Item的相对于边框的偏移 
	int										m_iLastItemOffset;		 ///< 最后一个Item的相对于右边框的偏移 
	int										m_nTabAlign;			 ///< 排列方式 
	CSize									m_ItemSize;				 ///< 一个标签的默认大小，同时它也起了标识所有标签的高度(top/bottom)或宽度(left/right)
	CSize                                   m_OrgItemSize;           ///< 原始的Size大小
	CPoint									m_TextPt;				 ///< 相对于主标签区域的左上角偏移，默认为-1，-1，其中-1表示此方向居中 
	bool                                    m_bDrag;				 ///< 辅助,如果设置了可拖动属性,只有title列上,且不在title上的部分可以拖动
};