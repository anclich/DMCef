// ----------------------------------------------------------------
// Copyright (c)  
// All rights reserved.
// 
// File name:	StdAfx.h
// File mark:   
// File summary:
// Author:		guoyouhuang
// Edition:     1.0
// Create date: 2017-9-4
// ----------------------------------------------------------------
#pragma once
#define WIN32_LEAN_AND_MEAN             // 从 Windows 头中排除极少使用的资料
#include <vector>
#include <winerror.h>


// DM
#include "DmMainOutput.h"
#include "DUIEdit.h"
#include "IDUIWeb.h"
#include "DUICheckBox.h"

// lib
#ifdef _DEBUG
#pragma comment(lib,"DmMain_d.lib")
#else
#pragma comment(lib,"DmMain.lib")
#endif

using namespace DM;

#include "IDMWebApp.h"
#include "WebEvent.h"
#include "TabCtrl.h"
#include "MainWnd.h"
#if 0
#include "vld.h"
#endif

extern CMainWnd* g_pMainWnd;