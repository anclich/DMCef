// ----------------------------------------------------------------
// Copyright (c)  
// All rights reserved.
// 
// File name:	WebEvent.h
// File mark:   
// File summary:仿IE的接口实现
// Author:		guoyouhuang
// Edition:     1.0
// Create date: 2017-9-15
// ----------------------------------------------------------------
#pragma once

class WebEvent : public IDMWebEvent, public DMRefNum
{
public:
	virtual HRESULT BeforeNavigate2(DUIWND hWnd, DMIN IDispatch *pDisp, DMIN wchar_t *pUrl,DMIN int Flags,DMIN wchar_t *pTargetFrameName,DMIN VARIANT *pPostData,DMIN wchar_t *pHeaders,DMINOUT VARIANT_BOOL *bCancel);
	virtual HRESULT NavigateComplete2(DUIWND hWnd,DMIN IDispatch *pDisp,DMIN wchar_t *pUrl);
	virtual HRESULT TitleChange(DUIWND hWnd, DMIN wchar_t *pText);
	virtual HRESULT NewWindow3(DUIWND hWnd, DMINOUT IDispatch **pDisp,DMINOUT VARIANT_BOOL *bCancel,DMIN DWORD dwFlags,DMIN wchar_t *pUrlContext,DMIN wchar_t *pUrl);
};
