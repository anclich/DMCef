// ----------------------------------------------------------------
// Copyright (c)  
// All rights reserved.
// 
// File name:	JSFun.h
// File mark:   
// File summary:JS调用相关函数
// Author:		guoyouhuang
// Edition:     1.0
// Create date: 2017-9-12
// ----------------------------------------------------------------
#pragma once
typedef long DISPID;

class JSFun;
class DMExternalHandler : public IDMExternalHandler
{
public:
	DMExternalHandler();
	void GetAllFunNames(DM::CArray<CStringW>& NameArray);
	void CallFunction(DUIWND hDUIWnd, CStringW strName, DM::CArray<CStringW>&strArgs, CStringW& strResult);

public:
	DMSmartPtrT<JSFun>             m_spJSFun;      
};

//--------------------------------------------------------------------------------------------------------------------------
/// <summary>
///		和IE尝试保持一样
/// </summary>
class JSFun : public DMRefNum
{
public:
	static HRESULT GetIDsOfJSNames(DUIWND hDUIWnd, std::wstring& strName, DISPID* rgDispId);

public:
	HRESULT JsCallCpp(DUIWND hDUIWnd, DISPID dispIdMember, const std::vector<std::wstring>&strArgs, std::wstring* pResult);	
};
