@echo off

set batDir=%~dp0
shift & set OutDir=%1

cd /d %batDir%
echo hgy delete MyRes folder begin------------------------------
rmdir  /s /q %OutDir%\MyRes\
echo hgy delete MyRes folder end--------------------------------
echo hgy copy MyRes folder begin--------------------------------
xcopy /y /s .\MyRes %OutDir%\MyRes\
echo hgy copy MyRes folder end----------------------------------

