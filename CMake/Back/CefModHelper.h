// ----------------------------------------------------------------
// Copyright (c)  
// All rights reserved.
// 
// File name:	CefModHelper.h
// File mark:   
// File summary:一些辅助函数
// Author:		guoyouhuang
// Edition:     1.0
// Create date: 2017-9-4
// ----------------------------------------------------------------
#pragma once
#include <string>

static const char kJsCallbackMessage[] = "JsCallback";					// web调用C++接口接口的通知

#ifndef    IsValidString
#define    IsValidString(x)							((x) && (x)[0])
#endif
#ifndef    SAFE_STR
#define    SAFE_STR(a) IsValidString(a) ? a :L""
#endif

namespace CefMod
{
	/// <summary>
	///		简单控制台LOG辅助
	/// </summary>
	enum CEFLOGLEVEL///LOG类型
	{
		LOG_TRACE = 0,		///<跟踪类型
		LOG_DEBUG = 1,		///<Debug类型
		LOG_INFO  = 2,		///<正常类型
		LOG_WARN  = 3,		///<警告类型
		LOG_ERR   = 4,		///<错误类型
		LOG_FATAL = 5,		///<极其严重错误
	};

	class CefLogHelper
	{
	public:
		static void VLog(CEFLOGLEVEL iLevel, LPCWSTR lpszFuncName, LPCWSTR lpszFileName, int iLine, LPCWSTR szFmt,...);
	};
#define CEFLOG_DEBUG(fmt, ...) CefLogHelper::VLog(LOG_DEBUG, __FUNCTIONW__,__FILEW__, __LINE__,__STR2WSTR(fmt),__VA_ARGS__)
#define CEFLOG_INFO(fmt, ...)  CefLogHelper::VLog(LOG_INFO, __FUNCTIONW__,__FILEW__, __LINE__,__STR2WSTR(fmt),__VA_ARGS__)
#define CEFLOG_ERR(fmt, ...)   CefLogHelper::VLog(LOG_ERR, __FUNCTIONW__,__FILEW__, __LINE__,__STR2WSTR(fmt),__VA_ARGS__)

	/// <summary>
	///		简单辅助函数
	/// </summary>
	bool CreateMyDirectory(const std::wstring& strFullDir);// 创建多层目录
	std::wstring GetAppPath();


	/// <summary>
	///		窗口消息转换函数
	/// </summary>
	int GetCefMouseModifiers(WPARAM wparam);
	int GetCefKeyboardModifiers(WPARAM wparam, LPARAM lparam);
	bool IsKeyDown(WPARAM wparam);
}

/// <summary>
///		取自chromium_git\chromium\src\base\synchronization\waitable_event.h
/// </summary>
class WaitableEvent
{
public:
	WaitableEvent(bool bManReset, bool bSignaled);
	~WaitableEvent();
	void Reset();
	void Signal();
	bool Wait(DWORD dwMaxTimeOut = INFINITE);
	bool DealWithMsg();
public:
	HANDLE							 m_Handle;
};


/// <summary>
///		命名管道通讯工具,取自http://blog.csdn.net/dengxu11/article/details/7174288
/// </summary>
class NamedPipe
{
public:
	enum{PIPE_SERV,PIPE_CLIN,PIPE_UNDF};
	NamedPipe();
	NamedPipe(LPCWSTR lpszPipeName, int nMode);
	~NamedPipe();
	bool IsValid() const;
	bool Create(LPCWSTR lpszPipeName, int nMode);  
	bool Read(std::wstring& strRet,DWORD& dwRead,DWORD dwTimeOut = INFINITE);
	bool Write(void* pBuf, int nSize, DWORD& dwWrite); 

public:
	HANDLE							 m_hPipe;
	int                              m_nMode;
	std::wstring                     m_strPipeName;
	HANDLE                           m_hEvent;
};


/// <summary>
///		额外数据的index
/// </summary>
enum enumExtraInfo
{
	EXTRAINFO_FUNNAMES = 0,
	EXTRAINFO_MAX,
};
