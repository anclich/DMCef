# DMCef
---

##1.DMCef
* `DMCef`是[REDM](https://gitee.com/hgy413/REDM)的CEF插件,实现CEF的一些基础功能
* 1.考虑到CEF体积较大,用户可动态静默下载，再延迟加载CEF
* 2.实现CEF原生、离屏两种模式，均使用独立线程模式
* 3.实现设置Cookie等基础功能
* 4.实现JS和C++互调(同步方式)
* 5.实现IDUIWeb部分接口，可以和IE混用同一套对外接口



* DMCef库SVN路径:[svn://gitee.com/hgy413/DMCef](svn://gitee.com/hgy413/DMCef)

##2.编译
* CMAKE简单使用:[http://hgy413.com/3426.html](http://hgy413.com/3426.html)
* libcef_dll_wrapper仅支持debug和release模式，所以如果在RelWithDebInfo模式下，用户需自行把libcef_dll_wrapper的Release生成文件拷贝到RelWithDebInfo目录
* 优先编译libcef_dll_wrapper，会自动打包[Release+Resources]到生成目录的Cef文件夹
* debug默认使用单进程模式，方便调用

`持续完善中...`